var 	express = require('express');                                 						//Load the Express module for hosting static files, and http API routes
var 	bcrypt = require('bcrypt-nodejs');										//Encrypt, hash, and compare passwords
var 	fs = require('fs');													//Allows access to the file system of the node server, for storing config, logs, etc..
var 	exp = express();													//Instantiate an Express App
		exp.use(express.static('build'));										//Declare directorie(s) for serviing static assests
		exp.use(require('serve-favicon')('build/favicon.ico'))							//Add/serve the DiRAD favicon in a variety of formats, without processing additional logic
var 	port = require('normalize-port')(process.env.PORT || 8765)				//This will use a cloud host provided port number, or specified local port
var 	Connection = require('tedious').Connection;							//Load the driver(s) to connect with MS SQL
var 	Request = require('tedious').Request;										//...
var 	TYPES = require('tedious').TYPES;												//...
var     http = require('request')                                           //Provides simple syntax for sending HTTP requests
var 	server = exp.listen(port)											//Create  app server
var 	util = require('util'); 												//Allows logging of embedded objects/arrays

//Instantiate the main application from the config file, which will hold in memory all of the working data, for super fast changes and access.
//routes make up the web-socket API. Each route is a specific function to execute.
var	app = JSON.parse(fs.readFileSync('./config.json').toString())

//Output to console, and write logs to the server, in the logs directory, formatted like 2018-04-27.txt
function log(data){
	fs.appendFile('logs/' + new Date().toISOString().split('T')[0] + '.txt', '\n' + util.inspect(data, {depth: null}).substr(0,500), (err) => {
		if (err) {console.log(err)}
	});
	console.log(util.inspect(data, {depth: null}))
}

log(app)

//		***Websocket Configuration and Event Handling***
var 	websocket = require('ws');																	//Load the websockets module
var 	wss = new websocket.Server({server: server});													//Attach web socket server to existing app server

wss.on('connection', function connection(ws) {
	ws.on('message', function incoming(message) {
		var data = JSON.parse(message);
		if(ws.info){
			log(new Date().toJSON() + ' -- Websocket -- '  + ws.info.domain + ' -- ' + ws.info.user + ' Received data over a websocket')
		}else {
			log(new Date().toJSON() + ' -- Websocket -- Received data over a websocket')
		}
		if(data.route != 'login'){log(data)}																	//Log the data, as long as it does not include a login password

		if(app.routes[data.route]){																				//If the route exists within our app...
			app.routes[data.route](data, ws);																	//Launch the corresponding function, passing the data and particular web socket.
		} else {
			log(data.route + ' -- Route does not exist')														//If the route doesn't exist, we go nowhere. Sad..
		}
	});

	ws.on('close', function incoming() {																		//If the user closes their connection, clean up their user object and log it.
		if(ws.info){
			log(new Date().toJSON() + ' -- Websocket -- '  + ws.info.domain + ' -- ' + ws.info.user + ' Websocket disconnected')
			userDisconnect(ws.info.user, ws.info.domain)
		}else {
			log(new Date().toJSON() + ' -- Websocket -- Anonymous websocket disconnected')
		}
	});

	ws.on('error', function incoming() {																			//If the user's connection errors out, clean up their user object and log it.
		if(ws.info){
			log(new Date().toJSON() + ' -- Websocket -- '+ws.info.user+' Websocket disconnected')
			userDisconnect(ws.info.user, ws.info.domain)
		}else {
			log(new Date().toJSON() + ' -- Websocket -- Anonymous websocket disconnected')
		}
	});
});

function userDisconnect(user, domain){																		//User is disconnected.
	app.routes.logDisconnect(domain, user)																	//Log to database.
	app.orgs[domain].Users[user].loggedIn = false														//Adjust their user object to match current state.
	delete app.orgs[domain].Users[user].ActivityDateTime												//...
	delete app.orgs[domain].Users[user].status															//...

	for (var type in app.orgs[domain].listeners){																				//Remove this user's listeners.
		!app.orgs[domain].listeners[type][user] || delete app.orgs[domain].listeners[type][user]
	}

	for (var listener in app.orgs[domain].listeners.Users) {																	//...And send it out to anyone listening
		send({route: 'alterState', state: "Users", path: user, value: app.orgs[domain].Users[user]}, app.orgs[domain].listeners.Users[listener]);
	}
}

function send(data, ws){																							//Centralized function for sending data to clients through websockets
	if(!data || !ws){																										//Make sure we have data and a socket to send it on
		log(new Date().toJSON() + ' -- Websocket -- Tried to send, but missing socket or data.')
		log(data)
		return false
	}
	if(ws.info){
		log(new Date().toJSON() + ' -- Websocket -- Sending Message over a websocket -- ' + ws.info.domain + ' -- ' + ws.info.user) 	//Log the data
	}else {
		log(new Date().toJSON() + ' -- Websocket -- Sending Message over a websocket') 	//Log the data
	}


	if(data.state && data.state.hasOwnProperty('reportData') ){
		log('<Report>')
	} else if (data.state &&  data.state.hasOwnProperty('RealTimeStats')){
		log('<Stats>')
	} else if (data.state &&  data.state.hasOwnProperty('download')){
		log('<File>')
	} else if (data.state &&  data.state.hasOwnProperty('Faxing')){
		log('<Faxlist>')
	} else {
		log(data)
	}
	ws.send(JSON.stringify(data), function ack(err) {
		if(err){
			log(new Date().toJSON() + ' -- Websocket -- Problem sending message over a websocket')
			log(err)
		} else {
			return
		}
	});
}


function authenticate(domain, user, module, right){
	for (var role in app.orgs[domain].Users[user].roles ) {				//look at every role assigned to the user
		if(app.orgs[domain].Roles[role].security[module][right]){			//Check the module and specific permission we're looking for.
			return true											//Return true if they have permission
		}
	}
	return false													//Return false if none of their roles have the right permission
}

function update(){
	var config = Object.assign({}, app)
		config.routes = {}
		config.timers = {}
		config.orgs = Object.assign({}, app.orgs)

	for (domain in config.orgs) {
		config.orgs[domain] = Object.assign({}, app.orgs[domain])
		config.orgs[domain].connections = {}
		config.orgs[domain].listeners = {}
		//app.orgs[domain].hasOwnProperty('SpecialMessages') ? config.orgs[domain].SpecialMessages = [] : ''
		app.orgs[domain].hasOwnProperty('PromoJackpots') ? config.orgs[domain].PromoJackpots = [] : ''
		app.orgs[domain].hasOwnProperty('CallingHours') ? config.orgs[domain].CallingHours = [] : ''
		//app.orgs[domain].hasOwnProperty('BusinessHours') ? config.orgs[domain].BusinessHours = [] : ''
		//app.orgs[domain].hasOwnProperty('Holidays') ? config.orgs[domain].Holidays = [] : ''
		//app.orgs[domain].hasOwnProperty('TransferNumbers') ? config.orgs[domain].TransferNumbers = [] : ''
		app.orgs[domain].hasOwnProperty('DelayMessages') ? config.orgs[domain].DelayMessages = [] : ''
		app.orgs[domain].hasOwnProperty('Settings') ? config.orgs[domain].Settings = {} : ''
		app.orgs[domain].hasOwnProperty('HolidayRaffle') ? config.orgs[domain].HolidayRaffle = {} : ''


		!config.orgs[domain].rts || delete config.orgs[domain].rts
	}

	fs.writeFile('config.json', JSON.stringify(config, null, 4), function (err) {
		if (err) return console.log(err);
		log('writing to config file ');
	});
}

setInterval(function(){
	update()
}, 300000)










app.routes.prepareDownload = function (data, ws) {
	log('preparing download. Looking for : ' + data.call)

	blobSvc.getBlobToStream('ocfs', decodeURIComponent(data.call), fs.createWriteStream("recordings/" + decodeURIComponent(data.call)), function(error, result, response) {
		log(result)
		log(response)
		if (!error) {
			ws.send(JSON.stringify({route: 'setState', state: {downloadReady: true}}));
		}else {
			log(error)
			ws.send(JSON.stringify({route: 'setState', state: {downloadReady: false}}));
		}
	})
};






app.routes.removeDownload = function (data) {
	fs.unlink("recordings/" +  decodeURIComponent(data.call))
};



fs.watch('./serverCode/', (eventType, file) => {
	eval(fs.readFileSync('./serverCode/' + file)+'');
});

fs.readdir('./serverCode/', (err, files) => {
	for (file of files ){
		eval(fs.readFileSync('./serverCode/' + file)+'');
	}
})

process.on('uncaughtException', function(err) {log('Caught exception: ' + err)});
log('app started on ' + port)
