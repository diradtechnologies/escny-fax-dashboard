var chatScript = document.createElement('script')

var id = document.createAttribute("id"); 
id.value = "purecloud-webchat-js";
chatScript.setAttributeNode(id); 

var type = document.createAttribute("type"); 
type.value = "text/javascript";
chatScript.setAttributeNode(type); 

var src = document.createAttribute("src"); 
src.value = "https://apps.mypurecloud.com/webchat/jsapi-v1.js";
chatScript.setAttributeNode(src); 

var region = document.createAttribute("region"); 
region.value = "us-east-1";
chatScript.setAttributeNode(region); 

var org = document.createAttribute("org-guid"); 
org.value = "a14769c0-66a8-4198-b062-3a0afe0a7b35";
chatScript.setAttributeNode(org); 

var deployment = document.createAttribute("deployment-key"); 
deployment.value = "9c6956d6-6b8b-4927-9a98-2f82d248fa4d";
chatScript.setAttributeNode(deployment); 

document.getElementsByTagName('body')[0].appendChild(chatScript);

var chatButton = document.createElement('div')

chatButton.appendChild(document.createTextNode("Chat Now!"))
var id = document.createAttribute("id"); 
id.value = "start-purecloud-chat"
chatButton.setAttributeNode(id)
chatButton.style.backgroundColor = '#397'
chatButton.style.position = 'fixed'
chatButton.style.border = '1px solid #000'
chatButton.style.padding = '25px'
chatButton.style.borderRadius = '5px'
chatButton.style.bottom = '-5px'
chatButton.style.right = '150px'
document.getElementsByTagName('body')[0].appendChild(chatButton);

var chatBox = document.createElement('div')
var id = document.createAttribute("id"); 
id.value = "chat-container"
chatBox.setAttributeNode(id)

chatBox.style.position = 'fixed'
chatBox.style.height = '500px'
chatBox.style.bottom = '0px'
chatBox.style.right = '150px'


document.getElementsByTagName('body')[0].appendChild(chatBox);


window.PURECLOUD_WEBCHAT_FRAME_CONFIG = { containerEl: 'chat-container' };
	
	var chatConfig = {
		webchatAppUrl: 'https://apps.mypurecloud.com/webchat',
		webchatServiceUrl: 'https://realtime.mypurecloud.com:443',
		orgGuid: 'a14769c0-66a8-4198-b062-3a0afe0a7b35',
		orgId: '21300',
		orgName: 'illinoistreasurer',
		queueName : 'Claims Heir',
		chatNowElement: 'start-purecloud-chat',
		logLevel: 'INFO',
		reconnectEnabled: true,
		reconnectOrigins: [ 'https://example.com' ]
	};
	

	chatButton.onclick = function onStartChatClick () {
		window.ININ.webchat.create(chatConfig).then(function(webchat) {
			return webchat.renderFrame({
				containerEl: 'chat-container'
			}).catch(function(error) {
				console.error('Error starting chat');
				console.error(error);
			});
		}).catch(function(error) {
			console.error('Error initializing chat');
			console.error(error);
		});
	}
