exp.use(require('body-parser').json());

app.routes.initiate = function(data, ws){
	send({route: 'hello'}, ws);
};

app.routes.login = function(data, ws){
	//Check to verify both login fields were submitted properly.
	if(!data.loginCreds.user) {
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "No username provided."}}, ws);
		return false;
	} else if (!data.loginCreds.user.split('@')[1]) {
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "No domain provided. (user@domain)"}}, ws);
		return false;
	} else if (!data.loginCreds.password) {
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "No password provided."}}, ws);
		return false;
	}

	//log the websocket message without exposing password.
	var 	logdata = Object.assign({}, data);
			logdata.loginCreds = Object.assign({}, data.loginCreds);
			logdata.loginCreds.password = logdata.loginCreds.password.replace(/./g, '*');
			log(logdata);

	var 	user = data.loginCreds.user.split('@')[0];
			domain = data.loginCreds.user.split('@')[1];
			password = data.loginCreds.password;

	if (!app || !app.orgs){
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "There was an error logging in."}}, ws);
	} else if (!app.orgs[domain]) {
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "Domain is not recognized."}}, ws);
	} else if (!app.orgs[domain].Users[user]) {
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "Username is not recognized."}}, ws);
	} else if (app.orgs[domain].Users[user].failedAttempts > app.orgs[domain].settings.passwordPolicy.maxFailedAttempts) {
		send({route: 'setState', state: {loggedIn: false, loading: false, loginError: "Account is locked out."}}, ws);
	} else {

		bcrypt.compare(password, app.orgs[domain].Users[user].password, function(err, valid) {
			if(!valid){
				app.orgs[domain].Users[user].failedAttempts++
				ws.send(JSON.stringify({route: 'setState', state: {loggedIn: false, loading: false, loginError: "Credentials are not recognized."}}));
			}else {

				logAccess(domain,user)
				app.orgs[domain].Users[user].failedAttempts = 0
				app.orgs[domain].Users[user].loggedIn = true
				app.orgs[domain].Users[user].ActivityDateTime = new Date()
				app.orgs[domain].Users[user].status = "Online"

				var secObject = {};
				var availableViews = [];

				for(var view of app.orgs[domain].settings.views){						//Loop through the views within the app...
					for (var role in app.orgs[domain].Users[user].roles ) {				//look at every role assigned to the user
						for (var sec in app.orgs[domain].Roles[role].security ) {		//check each security right for the role
							if(view.view === sec){												//When we find the one we're looking for...
								if(app.orgs[domain].Roles[role].security[sec].View){	//if the user can view...
									if(availableViews.indexOf(view) < 0){					//And if we haven't already added it...
										availableViews.push(view)								//Add the view to the user's application and navigation
									}
									if(!app.orgs[domain].listeners[sec]){						//Check to see if we have a listener set up already for that data
										app.orgs[domain].listeners[sec] = {}					//Add it if we don't
									}

									app.orgs[domain].listeners[sec][user] = ws			//add the user's web socket to a listener for that data
									secObject[sec] = true											//add the permission to an object so we know what data to send to get them started
								}
							}
						}
					}
				}


				var 	sendData = {
					route: 'setState',
					state: {
						loggedIn: true,
						loading: false,
						domain: domain,
						org: app.orgs[domain].organization,
						loginError: "",
						user: user,
						firstName: app.orgs[domain].Users[user].firstName,
						lastName: app.orgs[domain].Users[user].lastName,
						views: availableViews,
						availableViews: availableViews,
						features: app.orgs[domain].features
					}
				}

                if(app.orgs[domain].settings.passwordPolicy.maxAge && (new Date() - new Date(app.orgs[domain].Users[user].passwordUpdated)) / 86400000 >  app.orgs[domain].settings.passwordPolicy.maxAge ){
                    sendData.state.passwordExpired = true;
                    sendData.state.adjustPINError = 'Your password has expired. Please set a new password.'
                }

				for (var sec in secObject ){
					sendData.state[sec] = app.orgs[domain][sec]
				}

				ws.info = {
					domain: domain,
					user: user
				}
				send(sendData, ws)


				app.orgs[domain].connections[user] = ws

				for (var listener in app.orgs[domain].listeners.Users) {
					send({route: 'alterState', state: "Users", path: user, value: app.orgs[domain].Users[user]}, app.orgs[domain].listeners.Users[listener]);
				}
			}
		});
	}
}

app.routes.logOut = function(data, ws) {log(new Date().toJSON() + ' -- Application -- User Logged Out -- ' + ws.info.user )
	var user =  ws.info.user
	var domain = ws.info.domain

	app.routes.logDisconnect(domain, user)

	app.orgs[domain].Users[user].loggedIn = false
	app.orgs[domain].Users[user].ActivityDateTime = new Date()
	app.orgs[domain].Users[user].status = "Logged Out"

	send({route: 'setState', state: {loggedIn: false, loginConfigLoaded: false, loginConfig: [{data: "user", type: "text",label: "Username@Domain", placeholder: "Username@Domain", error: '',validation: {}}], loading: false, loginError: "You have been successfully logged out.", firstName: '', lastName: '', views: [], availableViews: [], features: {}}}, ws);


	for (var listener in app.orgs[domain].listeners.Users) {
		send({route: 'alterState', state: "Users", path: user, value: app.orgs[domain].Users[user]}, app.orgs[domain].listeners.Users[listener]);
	}
}

app.routes.logOutUser = function(data, ws) {
	var domain = ws.info.domain
	var user = data.user
	if(!authenticate(ws.info.domain, ws.info.user, "Users", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Users permission.'}, ws);return false;}

	app.routes.logDisconnect(domain, data.user)

	app.orgs[domain].Users[user].loggedIn = false
	app.orgs[domain].Users[user].ActivityDateTime = new Date()
	app.orgs[domain].Users[user].status = "Logged Out"

	if(app.orgs[domain].connections[user]){
		send({route: 'setState', state: {loggedIn: false, loginConfigLoaded: false, loginConfig: [{data: "user", type: "text",label: "Username@Domain", placeholder: "Username@Domain", error: '',validation: {}}], loading: false, loginError: "You have been logged out by an administrator.", firstName: '', lastName: '', views: [], availableViews: [], features: {}}}, app.orgs[domain].connections[user]);
	}

	for (var listener in app.orgs[domain].listeners.Users) {
		send({route: 'alterState', state: "Users", path: data.user, value: app.orgs[domain].Users[data.user]}, app.orgs[domain].listeners.Users[listener]);
	}
}

app.routes.resetUser = function(data, ws) {
	var domain = ws.info.domain
	var user = data.resetUser

	if(!authenticate(ws.info.domain,  ws.info.user, "Users", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Users permission.'}, ws);return false;}

    if(app.orgs[domain].settings.passwordPolicy.minLength && data.resetUserPIN.length < app.orgs[domain].settings.passwordPolicy.minLength){
        send({route: 'setState', state:{resetUserPINError: 'Password must be at least '+ app.orgs[domain].settings.passwordPolicy.minLength +' characters.'}}, ws);
        return false;
    }

    if(app.orgs[domain].settings.passwordPolicy.maxLength && data.resetUserPIN.length > app.orgs[domain].settings.passwordPolicy.maxLength){
        send({route: 'setState', state:{resetUserPINError: 'Password must be less than '+ app.orgs[domain].settings.passwordPolicy.maxLength +' characters.'}}, ws);
        return false;
    }

    for(i=0; app.orgs[domain].Users[user].oldPasswords && i<app.orgs[domain].settings.passwordPolicy.history && i< app.orgs[domain].Users[user].oldPasswords.length; i++){
        if(bcrypt.compareSync(data.resetUserPIN, app.orgs[domain].Users[user].oldPasswords[i])){
            send({route: 'setState', state:{resetUserPINError: 'Password can not be one of the previous '+ app.orgs[domain].settings.passwordPolicy.history +' passwords.'}}, ws);
            return false;
        }
    }

    var complexCount = 0
    for(pattern of app.orgs[domain].settings.passwordPolicy.complexity ){
        var test = new RegExp(pattern)
        if(test.test(data.resetUserPIN)){
            complexCount++
        }
    }
    if(app.orgs[domain].settings.passwordPolicy.minComplex && complexCount < app.orgs[domain].settings.passwordPolicy.minComplex){
        send({route: 'setState', state:{resetUserPINError: 'Password does not meet complexity requirements.'}}, ws);
        return false;
    }

	bcrypt.hash(data.resetUserPIN, null, null, function(err, hash) { if (err) {log(err)}
		log(new Date().toJSON() + ' -- Application -- Password Changed -- ' + domain + ' -- ' + user )
        if(!app.orgs[domain].Users[user].oldPasswords){
            app.orgs[domain].Users[user].oldPasswords = []
        }
        app.orgs[domain].Users[user].oldPasswords.unshift(app.orgs[domain].Users[user].password)

        if(app.orgs[domain].Users[user].oldPasswords.length > app.orgs[domain].settings.passwordPolicy.history ) {
            app.orgs[domain].Users[user].oldPasswords.splice(app.orgs[domain].settings.passwordPolicy.history, app.orgs[domain].Users[user].oldPasswords.length )
        }

		app.orgs[domain].Users[user].password = hash
        app.orgs[domain].Users[user].passwordUpdated = new Date().toJSON()

        send({route: 'setState', state:{resetUserPINError: null}}, ws);
		send({route:  'alterState', state: "Users", path: user, value: app.orgs[domain].Users[user]}, ws);
	});
}

app.routes.adjustPIN = function(data, ws) {
	var domain = ws.info.domain
	var user = ws.info.user

    if(app.orgs[domain].settings.passwordPolicy.minAge && (new Date() - new Date(app.orgs[domain].Users[user].passwordUpdated)) / 86400000 < app.orgs[domain].settings.passwordPolicy.minAge ) {
        send({route: 'setState', state: {adjustingPIN: true, adjustPINError: 'You must wait '+ app.orgs[domain].settings.passwordPolicy.minAge +' days between password changes.', loading: false}}, ws)
        return false;
    }

    if(app.orgs[domain].settings.passwordPolicy.minLength && data.adjustPIN.length < app.orgs[domain].settings.passwordPolicy.minLength){
        send({route: 'setState', state: {adjustingPIN: true, adjustPINError: 'Password must be at least '+ app.orgs[domain].settings.passwordPolicy.minLength +' characters.', loading: false}}, ws)
        return false;
    }

    if(app.orgs[domain].settings.passwordPolicy.maxLength && data.adjustPIN.length > app.orgs[domain].settings.passwordPolicy.maxLength){
        send({route: 'setState', state: {adjustingPIN: true, adjustPINError: 'Password must be less than '+ app.orgs[domain].settings.passwordPolicy.maxLength +' characters.', loading: false}}, ws)
        return false;
    }

    for(i=0; app.orgs[domain].Users[user].oldPasswords && i<app.orgs[domain].settings.passwordPolicy.history && i< app.orgs[domain].Users[user].oldPasswords.length; i++){
        if(bcrypt.compareSync(data.adjustPIN, app.orgs[domain].Users[user].oldPasswords[i])){
            send({route: 'setState', state: {adjustingPIN: true, adjustPINError: 'Password can not be one of the previous '+ app.orgs[domain].settings.passwordPolicy.history +' passwords.', loading: false}}, ws)
            return false;
        }
    }

    var complexCount = 0
    for(pattern of app.orgs[domain].settings.passwordPolicy.complexity ){
        var test = new RegExp(pattern)
        if(test.test(data.adjustPIN)){
            complexCount++
        }
    }
    if(app.orgs[domain].settings.passwordPolicy.minComplex && complexCount < app.orgs[domain].settings.passwordPolicy.minComplex){
        send({route: 'setState', state: {adjustingPIN: true, adjustPINError: 'Password does not meet complexity requirements.', loading: false}}, ws)
        return false;
    }

	bcrypt.hash(data.adjustPIN, null, null, function(err, hash) { if (err) {log(err)}
		log(new Date().toJSON() + ' -- Application -- Password Changed -- ' + user  )

        if(!app.orgs[domain].Users[user].oldPasswords){
            app.orgs[domain].Users[user].oldPasswords = []
        }
        app.orgs[domain].Users[user].oldPasswords.unshift(app.orgs[domain].Users[user].password)

        if(app.orgs[domain].Users[user].oldPasswords.length > app.orgs[domain].settings.passwordPolicy.history ) {
            app.orgs[domain].Users[user].oldPasswords.splice(app.orgs[domain].settings.passwordPolicy.history, app.orgs[domain].Users[user].oldPasswords.length )
        }

		app.orgs[domain].Users[user].password = hash
        app.orgs[domain].Users[user].passwordUpdated = new Date().toJSON()

		send({route: 'setState', state: {passwordExpired: false, adjustingPIN: '', adjustPIN: '', loading: false}}, ws);
	});
}

app.routes.changeName = function(data, ws) {
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain, ws.info.user, "Users", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Users permission.'}, ws);return false;}

	log(new Date().toJSON() + ' -- Database -- Name Changed -- ' + data.first + ' ' + data.last )

	app.orgs[domain].Users[data.user].firstName = data.first
	app.orgs[domain].Users[data.user].lastName = data.last
	app.orgs[domain].Users[data.user].wholeName = data.first + ' ' + data.last

    for(user in app.orgs[domain].Users){
        if(data.user !== user && data.recUserID === app.orgs[domain].Users[user].userID){
            send({route: 'memo', color: '#903', content: 'Can not duplicate another user\'s id.'}, ws);
            return false;
        }
    }
    if(data.recUserID){ app.orgs[domain].Users[data.user].userID = data.recUserID}
    if(data.recPassword){app.orgs[domain].Users[data.user].accessCode = data.recPassword}


	for (var listener in app.orgs[domain].listeners.Users) {
		send({route: 'alterState', state: "Users", path: data.user, value: app.orgs[domain].Users[data.user]}, app.orgs[domain].listeners.Users[listener]);
	}
}

//Special Message Routes
function getSpecialMessages(domain) {
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("Select * from Special_Messages", function (err, rowCount) {
			if (err) {log(err) }
			connection.close();
		});

			request.on('row', function (columns) {
				var rowObject = {};
				columns.forEach(function (column) {
					if (column.value === null) { } else {
						rowObject[column.metadata.colName] = column.value;
					}
				});
				result.push(rowObject)
			});

			request.on('doneProc', async function () {
				app.orgs[domain].SpecialMessages = result
				for (var listener in app.orgs[domain].listeners.SpecialMessages) {
					send({route: 'setState', state: {SpecialMessages: app.orgs[domain].SpecialMessages}}, app.orgs[domain].listeners.SpecialMessages[listener]);
				}
			})
		connection.execSql(request);
	});
}

function updateSpecialMessages(){
    for(domain in app.orgs){
    	if(app.orgs[domain].hasOwnProperty('SpecialMessages')){
    		getSpecialMessages(domain)
    	}
    }
}
updateSpecialMessages()

exp.get('/updateSpecialMessages', (req, res)=>{
    updateSpecialMessages()
    res.status(200).end("Updated. Thanks!")
})

exp.post('/tuiAuthentication', (req, res)=>{
    if(req.body){
        console.log(req.body)
        for (user in app.orgs[req.body.domain].Users){
            if(req.body.userID === app.orgs[req.body.domain].Users[user].userID){
                if(req.body.accessCode === app.orgs[req.body.domain].Users[user].accessCode){
                    var send = {firstName: app.orgs[req.body.domain].Users[user].firstName, lastName: app.orgs[req.body.domain].Users[user].lastName}
                    return res.status(200).end(JSON.stringify(send))
                }
            }
        }
    }
    res.status(401).end("Unauthorized")
})

app.routes.uploadSpecialMessage = (data, ws) => {
    if(!authenticate(ws.info.domain,  ws.info.user, "SpecialMessages", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Special Messages permission.'}, ws);return false;}

    http.post({url: 'http://192.168.77.79:5678/uploadSpecialMessage', json: data}, (err, res, body) => {
        console.log(res.statusCode)
        if (err || res.statusCode == 500) {
            log(res.statusCode + ' - Upload failed:' + err);
            send({route: 'memo', color: '#903', content: 'An error occurred while uploading. Please contact DiRAD.'}, ws);
        } else {
            log('Upload Succeeded!')
            getSpecialMessages('hrt')
        }
    });
}

app.routes.editSpecialMessage = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain,  ws.info.user, "SpecialMessages", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Special Messages permission.'}, ws);return false;}
	app.routes.logActivity(ws.info.domain,  ws.info.user, 'Edited Special Message -- '+ data.editPrompt )

	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

   var reset = ""
	if(data.active === true){
		reset = "Update Special_Messages SET is_active = 0; "
	}

    var extraFields = ''
    if(data.hasOwnProperty('Barge_Allowed')){
        extraFields += ', Barge_Allowed = @Barge_Allowed '
    }
    if(data.hasOwnProperty('Action')){
        extraFields += ', Action = @Action '
    }

	connection.on('connect', function (err) {
        var request = new Request(reset + "Update Special_Messages SET Description = @description, Active_StartDateTime  = @start, Active_EndDateTime = @end, is_active = @active "+extraFields+" WHERE Prompt = @prompt", function(err, rowCount) { if (err){log(err) } connection.close() });

		request.addParameter('active', TYPES.NVarChar, data.active);
		request.addParameter('prompt', TYPES.NVarChar, data.editPrompt);
		request.addParameter('description', TYPES.NVarChar, data.description);
		request.addParameter('start', TYPES.DateTime, new Date(data.start));
		request.addParameter('end', TYPES.DateTime,  new Date(data.end));

        if(data.hasOwnProperty('Barge_Allowed')){
            console.log(data.Barge_Allowed)
            request.addParameter('Barge_Allowed', TYPES.Bit, data.Barge_Allowed);
        }
        if(data.hasOwnProperty('Action')){
            console.log( data.Action)
            request.addParameter('Action', TYPES.NVarChar, data.Action);
        }

		request.on('doneProc', function () {
			getSpecialMessages(domain)
		})
        console.log(request)
		connection.execSql(request);
	});
}

//Holiday Routes
function getHolidays(domain) {
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("Select * from Holidays order by Holiday_Date ", function (err, rowCount) {
			if (err) {log(err) }
			connection.close();
		});

			request.on('row', function (columns) {
				var rowObject = {};
				columns.forEach(function (column) {
					if (column.value === null) { } else {
						rowObject[column.metadata.colName] = column.value;
					}
				});
				result.push(rowObject)
			});

			request.on('doneProc', async function () {
				app.orgs[domain].Holidays = result
				for (var listener in app.orgs[domain].listeners.Holidays) {
					send({route: 'setState', state: {Holidays: app.orgs[domain].Holidays}}, app.orgs[domain].listeners.Holidays[listener]);
				}
			})
		connection.execSql(request);
	});
}
for(domain in app.orgs){
	if(app.orgs[domain].hasOwnProperty('Holidays')){
		getHolidays(domain)
	}
}

app.routes.addHoliday = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain,  ws.info.user, "Holidays", "Add")){send({route: 'memo', color: '#903', content: 'You do not have the Add Holidays permission.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Added Holiday -- ' + data.name  )

	var connection = new Connection(app.orgs[domain].settings.database);connection.on('error', function(err) {log(err)});

	connection.on('connect', function (err) {
        var request = new Request("Insert INTO Holidays (is_enabled, Holiday_Name, Holiday_Date, Partial) VALUES (@active, @name, @date, @partial)", function(err, rowCount) { if (err){ log(err) } connection.close() });

		request.addParameter('partial', TYPES.NVarChar, data.partial);
		request.addParameter('active', TYPES.NVarChar, data.active);
		request.addParameter('name', TYPES.NVarChar, data.name);
		request.addParameter('date', TYPES.Date, new Date(data.date));

		request.on('doneProc', function () {
			getHolidays(domain)
		})

		connection.execSql(request);
	});
}

app.routes.editHoliday = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain,  ws.info.user, "Holidays", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Holidays permission.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Edited Holiday -- ' + data.name  )

	var connection = new Connection(app.orgs[domain].settings.database);connection.on('error', function(err) {log(err)});

	connection.on('connect', function (err) {
        var request = new Request("Update Holidays SET Holiday_Name = @name, Holiday_Date= @date,  is_enabled = @active, Partial = @partial where Id = @id", function(err, rowCount) { if (err){log(err) } connection.close() });

		request.addParameter('partial', TYPES.NVarChar, data.partial);
		request.addParameter('active', TYPES.NVarChar, data.active);
		request.addParameter('name', TYPES.NVarChar, data.name);
		request.addParameter('id', TYPES.UniqueIdentifier, data.id);
		request.addParameter('date', TYPES.Date, new Date(data.date));

		request.on('doneProc', function () {
			getHolidays(domain)
		})

		connection.execSql(request);
	});
}

app.routes.deleteHoliday = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain,  ws.info.user, "Holidays", "Delete")){send({route: 'memo', color: '#903', content: 'You do not have the Delete Holidays permission.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Deleted Holiday -- ' + data.name  )

	var connection = new Connection(app.orgs[domain].settings.database);connection.on('error', function(err) {log(err)});

	connection.on('connect', function (err) {
        var request = new Request("DELETE FROM Holidays WHERE Holiday_Name = @name", function(err, rowCount) { if (err){log(err) } connection.close() });

		request.addParameter('name', TYPES.NVarChar, data.name);

		request.on('doneProc', function () {
			getHolidays(domain)
		})

		connection.execSql(request);
	});
}


//Business Hour Routes
function getBusinessHours(domain) {
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT TOP (1000) [Business_Hours].[Id] ,[Day_of_Week] ,[is_open] ,[Open_Time] ,[Close_Time], Business_Hours_Sets.[Set_Name] as Application, Business_Hours_Sets.[Display_Name] as name FROM [dbo].[Business_Hours] LEFT OUTER JOIN Business_Hours_Sets on [Business_Hours].Business_Hours_Set_Id = Business_Hours_Sets.Id order by Business_Hours_Sets.[Set_Name] ", function (err, rowCount) {
			if (err) {log(err) }
			connection.close();
		});

			request.on('row', function (columns) {
				var rowObject = {};
				columns.forEach(function (column) {
					if (column.value === null) { } else {
						rowObject[column.metadata.colName] = column.value;
					}
				});
				result.push(rowObject)
			});

			request.on('doneProc', async function () {
				app.orgs[domain].BusinessHours = result
				for (var listener in app.orgs[domain].listeners.BusinessHours) {
					send({route: 'setState', state: {BusinessHours: app.orgs[domain].BusinessHours}}, app.orgs[domain].listeners.BusinessHours[listener]);
				}
			})
		connection.execSql(request);
	});
}

for(domain in app.orgs){
	if(app.orgs[domain].hasOwnProperty('BusinessHours')){
		getBusinessHours(domain)
	}
}

app.routes.editHours = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain,  ws.info.user, "BusinessHours", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Business Hours permission.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Edited Business Hours -- ' + data.id  )
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

	connection.on('connect', function (err) {
        var request = new Request("Update Business_Hours SET Open_Time = @start, Close_Time = @end, is_open = @open where Id = @id", function(err, rowCount) {
			if (err){log(err) }
			getBusinessHours(domain)
			connection.close()
		});

		request.addParameter('id', TYPES.UniqueIdentifier, data.id);
		request.addParameter('open', TYPES.Bit, data.open);
		request.addParameter('start', TYPES.DateTime, new Date(data.start));
		request.addParameter('end', TYPES.DateTime, new Date(data.end));

		connection.execSql(request);
	});
}

//Calling Hour Routes
function getCallingHours(domain) {
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("Select * from Admin_Functions", function (err, rowCount) {
			if (err) {log(err) }
			connection.close();
		});

			request.on('row', function (columns) {
				var rowObject = {};
				columns.forEach(function (column) {
					if (column.value === null) { } else {
						rowObject[column.metadata.colName] = column.value;
					}
				});
				result.push(rowObject)
			});

			request.on('doneProc', async function () {
				app.orgs[domain].CallingHours = result
				for (var listener in app.orgs[domain].listeners.CallingHours) {
					send({route: 'setState', state: {CallingHours: app.orgs[domain].CallingHours}}, app.orgs[domain].listeners.CallingHours[listener]);
				}
			})
		connection.execSql(request);
	});
}

for(domain in app.orgs){
	if(app.orgs[domain].hasOwnProperty('CallingHours')){
		getCallingHours(domain)
	}
}

app.routes.editCallingHours = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain,  ws.info.user, "CallingHours", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Calling Hours permission.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Edited Calling Hours -- ' + data.id  )

	var connection = new Connection(app.orgs[domain].settings.database);connection.on('error', function(err) {log(err)});

	connection.on('connect', function (err) {
        var request = new Request("Update Admin_Functions SET Earliest_Time_To_Call = @start, Latest_Time_To_Call = @end where recnum = @id", function(err, rowCount) { if (err){log(err) } connection.close() });

		request.addParameter('id', TYPES.BigInt, data.id);
		request.addParameter('start', TYPES.DateTime, new Date(data.start));
		request.addParameter('end', TYPES.DateTime, new Date(data.end));

		request.on('doneProc', function () {
			getCallingHours(domain)
		})

		connection.execSql(request);
	});
}

//Users
app.routes.addNewUser = function(data, ws) {
	if(!authenticate(ws.info.domain, ws.info.user, "Users", "Add")){send({route: 'memo', color: '#903', content: 'You do not have the Add Users permission.'}, ws);return false;}
	app.routes.logActivity(ws.info.domain,   ws.info.user, 'Added New User -- ' + data.newUserFirst + ' ' + data.newUserLast )
	log(new Date().toJSON() + ' -- Application -- New User Added -- ' + data.newUserFirst + ' ' + data.newUserLast )

	bcrypt.hash(data.newUserPIN, null, null, function(err, hash) { if (err) {log(err)}
		app.orgs[ws.info.domain].Users[data.newUserID] = {username: data.newUserID, firstName: data.newUserFirst, lastName: data.newUserLast, wholeName:  data.newUserFirst + ' ' + data.newUserLast, password: hash, failedAttempts: 0, roles:  {default : true} }

        var dup = false
        for(user in app.orgs[ws.info.domain].Users){
            if(data.recUserID && data.newUserID !== user && data.recUserID === app.orgs[ws.info.domain].Users[user].userID){
                send({route: 'memo', color: '#903', content: 'Can not duplicate another user\'s id.'}, ws);
                dup = true
            }
        }
        if(dup === false){
            app.orgs[ws.info.domain].Users[data.newUserID].userID = data.recUserID
            app.orgs[ws.info.domain].Users[data.newUserID].accessCode = data.recPassword
        }
		for (var listener in app.orgs[ws.info.domain].listeners.Users) {
			send({route: 'alterState', state: "Users", path: data.newUserID, value: app.orgs[ws.info.domain].Users[data.newUserID]}, app.orgs[ws.info.domain].listeners.Users[listener]);
		}
	});
};

app.routes.updateUserRoles = function(data,ws) {
	if(!authenticate(ws.info.domain, ws.info.user, "Users", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Users permission.'}, ws);return false;}
	if(ws.info.user === data.roleUser){send({route: 'memo', color: '#903', content: 'You are not permitted to adjust your own roles.'}, ws);return false;}


	for (i=0; i< data.rolesToAdd.length; i++){
		app.orgs[ws.info.domain].Users[data.roleUser].roles[data.rolesToAdd[i]] = true;
	}
	for (i=0; i< data.rolesToRemove.length; i++){
		if(data.rolesToRemove[i] === 'default'){
			send({route: 'memo', color: '#903', content: 'You are not permitted to remove the default role.'}, ws);
		} else {
			delete app.orgs[ws.info.domain].Users[data.roleUser].roles[data.rolesToRemove[i]];
		}
	}
	for (var listener in app.orgs[ws.info.domain].listeners.Users) {
		send({route: 'alterState', state: "Users", path: data.roleUser, value: app.orgs[ws.info.domain].Users[data.roleUser]}, app.orgs[ws.info.domain].listeners.Users[listener]);
	}


	if(app.orgs[ws.info.domain].connections[data.roleUser]){
		var availableViews = []
		var secObject = {}

		for(var view of app.orgs[ws.info.domain].settings.views){										//Loop through the views within the app...
			for (var role in app.orgs[ws.info.domain].Users[data.roleUser].roles ) {					//Look at every role assigned to the user
				for (var sec in app.orgs[ws.info.domain].Roles[role].security ) {						//Check each security right for the role...
					if(view.view === sec){												//When we find the one we're looking for...
						if(app.orgs[ws.info.domain].Roles[role].security[sec].view){						//if the user can view...
							if(availableViews.indexOf(view) < 0){					//And if we haven't already added it...
									availableViews.push(view)							//Add the view to the user's application and navigation
							}

							if(!app.orgs[ws.info.domain].listeners[sec]){										//Check to see if we have a listener set up already for that data
								app.orgs[ws.info.domain].listeners[sec] = {}									//Add it if we don't
							}

							app.orgs[ws.info.domain].listeners[sec][data.roleUser] = app.orgs[ws.info.domain].connections[data.roleUser]							//add the user's web socket to a listener for that data
							secObject[sec] = true											//add the permission to an object so we know what data to send to get them started
						}
					}
				}
			}
		}
		var 	sendData = {
					route: 'setState',
					state: {
						views: availableViews,
						availableViews: availableViews
					}
				}
		for (var sec in secObject ){
			sendData.state[sec] = app[sec]
		}
		send(sendData, app.orgs[ws.info.domain].connections[data.roleUser])
	}
}

app.routes.deleteUser = function(data, ws){
	if(!authenticate(ws.info.domain, ws.info.user, "Users", "Delete")){send({route: 'memo', color: '#903', content: 'You do not have the Delete Users permission.'}, ws);return false;}
	if(ws.info.user === data.delUser){send({route: 'memo', color: '#903', content: 'You are not permitted to delete yourself.'}, ws);return false;}
	app.routes.logActivity(ws.info.domain, ws.info.user, 'Deleted User -- ' + data.delUser )
	log(new Date().toJSON() + ' -- Application -- User Deleted -- ' + data.delUser)

	delete app.orgs[ws.info.domain].Users[data.delUser]

	for (var listener in app.orgs[ws.info.domain].listeners.Users) {
		send({route: 'deleteState', state: "Users", path: data.delUser}, app.orgs[ws.info.domain].listeners.Users[listener]);
	}
}

app.routes.unlockUser = function(data, ws) {
	if(!authenticate(ws.info.domain, ws.info.user, "Users", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Users permission.'}, ws);return false;}
	app.routes.logActivity(ws.info.domain, ws.info.user, 'Unlocked User -- ' + data.unlockUser )
	log(new Date().toJSON() + ' -- Application -- Account Unlocked -- ' + data.unlockUser )

	app.orgs[ws.info.domain].Users[data.unlockUser].failedAttempts = 0

	for (var listener in app.orgs[ws.info.domain].listeners.Users) {
		send({route: 'alterState', state: "Users", path: data.unlockUser, value: app.orgs[ws.info.domain].Users[data.unlockUser]}, app.orgs[ws.info.domain].listeners.Users[listener]);
	}
}

//Roles
app.routes.addRole = (data,ws) => {
	if(!authenticate(ws.info.domain, ws.info.user, "Roles", "Add")){send({route: 'memo', color: '#903', content: 'You do not have the Add Roles permission.'}, ws);return false;}

	if(app.orgs[ws.info.domain].Roles.hasOwnProperty(data.editRole)){send({route: 'memo', color: '#903', content: 'That role already exists.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Added Role -- ' + data.editRole )
	log(new Date().toJSON() + ' -- Application -- Role Added -- ' + data.editRole + ' -- by user '+ ws.info.user )

	var editRole = data.editRole
	var formatted = data.editRole.replace(' ', '').toLowerCase()
	var permissionsChanged = data.permissionsChanged

	app.orgs[ws.info.domain].Roles[editRole] = {}
	app.orgs[ws.info.domain].Roles[editRole].name = editRole
	app.orgs[ws.info.domain].Roles[editRole].security = {}
	for (module in app.orgs[ws.info.domain].Roles.default.security){
		app.orgs[ws.info.domain].Roles[editRole].security[module] = Object.assign({}, app.orgs[ws.info.domain].Roles.default.security[module])
	}

	for(module in permissionsChanged){
		for (right in permissionsChanged[module]) {
			app.orgs[ws.info.domain].Roles[editRole].security[module][right] = permissionsChanged[module][right]
		}
	}

	for (var listener in app.orgs[ws.info.domain].listeners.Roles) {
		send({route: 'alterState', state: "Roles", path: editRole, value: app.orgs[ws.info.domain].Roles[editRole]}, app.orgs[ws.info.domain].listeners.Roles[listener]);
	}
}

app.routes.deleteRole = (data,ws) => {
	if(!authenticate(ws.info.domain, ws.info.user, "Roles", "Delete")){send({route: 'memo', color: '#903', content: 'You do not have the Delete Roles permission.'}, ws);return false;}

	if(data.editRole === 'default'){send({route: 'memo', color: '#903', content: 'You are not permitted to delete the default role.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Deleted Role -- ' + data.editRole )

	log(new Date().toJSON() + ' -- Application -- Role Deleted -- ' + data.editRole + ' -- by user '+ ws.info.user )

	delete app.orgs[ws.info.domain].Roles[data.editRole]

	for (user in app.orgs[ws.info.domain].Users){
		delete app.orgs[ws.info.domain].Users[user].roles[data.editRole]
	}

	for (var listener in app.orgs[ws.info.domain].listeners.Roles) {
		send({route: 'deleteState', state: "Roles", path: data.editRole}, app.orgs[ws.info.domain].listeners.Roles[listener]);
	}
	for (var listener in app.orgs[ws.info.domain].listeners.Users) {
		send({route: 'setState', state: {Users: app.orgs[ws.info.domain].Users}}, app.orgs[ws.info.domain].listeners.Users[listener]);
	}
}

app.routes.updateRolePermissions = (data,ws) => {
	if(!authenticate(ws.info.domain, ws.info.user, "Roles", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Roles permission.'}, ws);return false;}
	if(data.editRole === 'default'){send({route: 'memo', color: '#903', content: 'You are not permitted to edit the default role permissions.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Role Permissions Adjusted -- ' + data.editRole )
	log(new Date().toJSON() + ' -- Application -- Permissions Adjusted -- ' + data.editRole + ' -- by user '+ ws.info.user  )

	var editRole = data.editRole
	var permissionsChanged = data.permissionsChanged

	for(module in permissionsChanged){
		for (right in permissionsChanged[module]) {
			app.orgs[ws.info.domain].Roles[editRole].security[module][right] = permissionsChanged[module][right]
		}
	}

	for (var listener in app.orgs[ws.info.domain].listeners.Roles) {
		send({route: 'alterState', state: "Roles", path: editRole, value: app.orgs[ws.info.domain].Roles[editRole]}, app.orgs[ws.info.domain].listeners.Roles[listener]);
	}
}

app.routes.updateRoleUsers = (data,ws) => {
	if(!authenticate(ws.info.domain, ws.info.user, "Roles", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Roles permission.'}, ws);return false;}
	if(data.editRole === 'default'){send({route: 'memo', color: '#903', content: 'You are not permitted to edit the default role users.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Role Users Adjusted -- ' + data.editRole )
	log(new Date().toJSON() + ' -- Application -- Role Users Adjusted  -- ' + data.editRole + ' -- by user '+ ws.info.user  )

	for(user of data.usersToAdd){
		app.orgs[ws.info.domain].Users[user].roles[data.editRole] = true
	}
	for(user of data.usersToRemove){
		delete app.orgs[ws.info.domain].Users[user].roles[data.editRole]
	}


	for (var listener in app.orgs[ws.info.domain].listeners.Roles) {
		send({route: 'alterState', state: "Roles", path: data.editRole, value: app.orgs[ws.info.domain].Roles[data.editRole]}, app.orgs[ws.info.domain].listeners.Roles[listener]);
	}
	for (var listener in app.orgs[ws.info.domain].listeners.Users) {
		send({route: 'setState', state: {Users: app.orgs[ws.info.domain].Users}}, app.orgs[ws.info.domain].listeners.Users[listener]);
	}
}


//Transfer Numbers Routes
function getTransferNumbers(domain) {
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("Select * FROM [Transfer_Numbers]", function (err, rowCount) {
			if (err) {log(err) }
			connection.close();
		});

			request.on('row', function (columns) {
				var rowObject = {};
				columns.forEach(function (column) {
					if (column.value === null) { } else {
						rowObject[column.metadata.colName] = column.value;
					}
				});
				result.push(rowObject)
			});

			request.on('doneProc', async function () {
				app.orgs[domain].TransferNumbers = result
				for (var listener in app.orgs[domain].listeners.TransferNumbers) {
					send({route: 'setState', state: {TransferNumbers: app.orgs[domain].TransferNumbers}}, app.orgs[domain].listeners.TransferNumbers[listener]);
				}

			})
		connection.execSql(request);
	});
}

for(domain in app.orgs){
	if(app.orgs[domain].hasOwnProperty('TransferNumbers')){
		getTransferNumbers(domain)
	}
}

app.routes.editTransferNumber = function(data, ws){
	var domain = ws.info.domain
	if(!authenticate(ws.info.domain, ws.info.user,  "TransferNumbers", "Edit")){send({route: 'memo', color: '#903', content: 'You do not have the Edit Transfer Numbers permission.'}, ws);return false;}

	app.routes.logActivity(ws.info.domain, ws.info.user, 'Edited Transfer Number -- '+ data.Description + ' -- '  + data.phoneNumber )

	var connection = new Connection(app.orgs[domain].settings.database);connection.on('error', function(err) {log(err)});
	connection.on('connect', function (err) {

        var request = new Request("Update Transfer_Numbers SET  is_enabled = @active, Phone_Number = @phoneNumber WHERE Description = @Description and Language = @Language", function(err, rowCount) { if (err){log(err) } connection.close() });

		request.addParameter('phoneNumber', TYPES.NVarChar, data.phoneNumber);
		request.addParameter('active', TYPES.NVarChar, data.active);
		request.addParameter('Description', TYPES.NVarChar, data.Description);
		request.addParameter('Language', TYPES.NVarChar, data.Language);

		request.on('doneProc', function () {
			getTransferNumbers(domain)
		})

		connection.execSql(request);
	});
}



app.routes.updateRealTimeStats = (domain) => {
	var config = Object.assign({}, app.orgs[domain].settings.database)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {log(err)});
	var options = [];
	var callGraph = [];
	var current = {columns: [], rows: []};
	var mainMenu = app.orgs[domain].settings.mainMenu


	connection.on('connect', function (err) {
        var request = new Request("SELECT Menu_Option_Descriptions.Menu_Response_Description as [option], Count(Menu_Reporting.Response_at_Menu) as [count] FROM Menu_Reporting JOIN Menu_Option_Descriptions on Menu_Option_Descriptions.Menu_Response = Menu_Reporting.Response_at_menu WHERE Menu_Option_Descriptions.menu_name = '"+mainMenu+"' and  Menu_Reporting.menu_name = '"+mainMenu+"' AND DATEDIFF(minute, Call_DateTime, GETUTCDATE() ) < 10080 group by Response_at_Menu, Menu_Response_Description ; Select DATEPART(Year, Call_Begin_DateTime) as [y], DATEPART(Month,Call_Begin_DateTime) -1 as [m], DATEPART(Day,Call_Begin_DateTime) as [d], DATEPART(HOUR,Call_Begin_DateTime) AS [h], Count(*) as [calls] FROM Call_Count WHERE DATEDIFF(minute, Call_Begin_DateTime, GETUTCDATE() ) < 1440 and CallDirection = 'Inbound' group by DATEPART(Year, Call_Begin_DateTime), DATEPART(Month,Call_Begin_DateTime) -1, DATEPART(Day,Call_Begin_DateTime), DATEPART(HOUR,Call_Begin_DateTime); SELECT Call_Count.Call_Begin_DateTime AS [Call Begin Date & Time],  Call_Count.RemoteParty_Phone_Number AS [Caller ID],  Menu_Reporting.Menu_Name AS [Current Menu], Menu_Descriptions.Menu_Description AS [Menu Description] FROM   Call_Count  left JOIN Menu_Reporting on Menu_Reporting.Id = (SELECT  TOP 1 [Id] FROM Menu_Reporting WHERE [CallId] = Call_Count.[CallId]ORDER by [Call_DateTime] desc) left JOIN Menu_Descriptions on Menu_Reporting.Menu_Name = Menu_Descriptions.Menu_Name WHERE        (DATEDIFF(minute, Call_Count.Call_Begin_DateTime, GETUTCDATE()) < 15) AND (Call_Count.Call_End_DateTime IS NULL)", function (err, rowCount) {
			if (err) {log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowObject = {};
			columns.forEach(function (column) {
				if (column.value === null) { } else {
					rowObject[column.metadata.colName] = column.value;
				}
			});
			if('option' in rowObject){options.push(rowObject)}
			else if('y' in  rowObject){callGraph.push(rowObject)}
			else if('Caller ID' in  rowObject){current.rows.push([rowObject['Call Begin Date & Time'], rowObject['Caller ID'], rowObject['Current Menu'], rowObject['Menu Description'] ])}
		});


			current.columns = [
				{ type: 'datetime', label: 'Call Begin Date & Time'},
				{ type: 'string', label: 'Caller ID'},
				{ type: 'string', label: 'Current Menu'},
				{ type: 'string', label: 'Menu Description'}
			]

		request.on('doneProc', function() {
			for (var listener in app.orgs[domain].listeners.RealTimeStats) {
				send({route: 'setState', state: {RealTimeStats: [{title: "Main Menu Options Chosen", type: 'pie', data: options}, {title: "Calls Received", type: 'bar', data: callGraph}, {title: "Current Calls", type: 'table', data: current}]}}, app.orgs[domain].listeners.RealTimeStats[listener]);
			}
		});

		connection.execSql(request);
	});
}



//     ***Notify Module Routes***
function getTemplates(domain) {
    var url = 'http://com-dirad-notifycore.azurewebsites.net/api/templates/' + app.orgs[domain].Notifications._id
    http.get({
        url: url,
        headers: {
            Accept: 'application/json'
        }
    }, function (error, response, body){
        if(error){log(error)};
        app.orgs[domain].Notifications.templates = JSON.parse(body)
		app.orgs[domain].Templates = JSON.parse(body)
        for (var listener in app.orgs[domain].listeners.Notifications) {
            send({route: 'setState', state: {Notifications: app.orgs[domain].Notifications}}, app.orgs[domain].listeners.Notifications[listener]);
        }
		 for (var listener in app.orgs[domain].listeners.Templates) {
            send({route: 'setState', state: {Templates: app.orgs[domain].Templates}}, app.orgs[domain].listeners.Templates[listener]);
        }
    });
}

for(var domain in app.orgs){
	if(app.orgs[domain].hasOwnProperty('Notifications')){
		getTemplates(domain)
	}
}

app.routes.editTemplate = function(data, ws) {
    var url = 'http://com-dirad-notifycore.azurewebsites.net/api/templates/' + app.orgs[domain].Notifications._id + "/" + data.activeTemplate.uniqueId

    http.put({
        url: url,
        headers: {
            Accept: 'application/json',
            "content-type": 'application/json'
        },
        body: data.activeTemplate
    }, function (error, response, body){
        if(error){log(error)};
        console.log(response)
        getTemplates(ws.info.domain)
    });
}

app.routes.addNotification = function(data, ws) {
	ObjectId("5a6557b1f24b291558a7799d")
}

app.routes.deleteNotification = function(data, ws) {
	var templateName = data.delNotification.templateName
	var domain = ws.info.domain

	for (var template of app.notifications.templates) {
		if(template.templateName === templateName){
			console.log("url: http://com-dirad-notifycore.azurewebsites.net/api/templates/" + app._id + "/" + template.uniqueId)
			request.delete({
				url: 'http://com-dirad-notifycore.azurewebsites.net/api/templates/' + app._id + '/' + template.uniqueId,
				headers: {
					Accept: 'application/json'
				}
			}, function (error, response, body) {
				if(error){console.log(error)};
				console.log(body)
				getTemplates(app);
			});
		}
	}
}

//Logging
function logAccess(domain,user){
	// var config = Object.assign({}, app.orgs[domain].settings.database)
	// config.options.useUTC = true
	// var connection = new Connection(config);connection.on('error', function(err) {log(err)});
    //
    // connection.on('connect', function (err) {
    //     var request = new Request("INSERT INTO WebAccess (username, FirstName, LastName, LoggedIn, Environment) VALUES (@user, @FirstName, @LastName, @LoggedIn, 'Web')", function (err, rowCount) {
	// 		if (err) { log(err) }
	// 		connection.close();
	// 	});
    //
	// 		request.addParameter('user', TYPES.VarChar, user);
	// 		request.addParameter('FirstName', TYPES.VarChar, app.orgs[domain].Users[user].firstName);
	// 		request.addParameter('LastName', TYPES.VarChar, app.orgs[domain].Users[user].lastName);
	// 		request.addParameter('LoggedIn', TYPES.DateTime, new Date());
    //
	// 		request.on('doneProc', function () {
    //
	// 		});
	// 	connection.execSql(request);
	// });
}

app.routes.logActivity = function(domain, user, activity){
	// var config = Object.assign({}, app.orgs[domain].settings.database)
	// config.options.useUTC = true
	// var connection = new Connection(config);connection.on('error', function(err) {log(err)});
    //
    // connection.on('connect', function (err) {
    //     var request = new Request("INSERT INTO WebActivity (username, activity, FirstName, LastName, created, Environment) VALUES (@user, @activity, @FirstName, @LastName, @created, 'Web')", function (err, rowCount) {
	// 		if (err) { log(err) }
	// 		connection.close();
	// 	});
    //
	// 		request.addParameter('user', TYPES.VarChar, user);
	// 		request.addParameter('activity', TYPES.VarChar, activity);
	// 		request.addParameter('FirstName', TYPES.VarChar, app.orgs[domain].Users[user].firstName);
	// 		request.addParameter('LastName', TYPES.VarChar, app.orgs[domain].Users[user].lastName);
	// 		request.addParameter('created', TYPES.DateTime, new Date());
    //
	// 		request.on('doneProc', function () {
    //
	// 		});
	// 	connection.execSql(request);
	// });
}

app.routes.logDisconnect = function(domain, user){

	// if(Object.keys(app.orgs[domain].connections).length < 1){
	// 	clearInterval(app.orgs[domain].rts)
	// }
    //
	// var config = Object.assign({}, app.orgs[domain].settings.database)
	// config.options.useUTC = true
	// var connection = new Connection(config);connection.on('error', function(err) {log(err)});
    //
    // connection.on('connect', function (err) {
    //     var request = new Request("Update WebAccess SET loggedOut = @LoggedOut Where Id = (SELECT TOP (1) [Id] FROM WebAccess WHERE username = @user order by LoggedIn desc)", function (err, rowCount) {
	// 		if (err) { log(err)}
	// 		connection.close();
	// 	});
    //
	// 		request.addParameter('user', TYPES.VarChar, user);
	// 		request.addParameter('LoggedOut', TYPES.DateTime, new Date());
    //
    //
	// 		request.on('doneProc', function () {
    //
	// 		});
	// 	connection.execSql(request);
	// });
}
