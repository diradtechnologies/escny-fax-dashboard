
//Reports
app.routes.newReport =  function(data, ws) {
	send({route: 'setState',state: {reportData: false, reportLoading: false, dynamicParams: {} }}, ws )
}

app.routes.incomingCallsSummary = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var languages = [];
	var transfersEnabled = false

	connection.on('connect', function (err) {
        var langRequest = new Request("SELECT Distinct(Language_Selected) FROM Call_Count where Language_Selected is not null ORDER BY Language_Selected", function (err, rowCount) {
			if (err) { console.log(err) }
			var transRequest = new Request("SELECT Count(*) from Transfer_History", function (err, rowCount) {
				if (err) { console.log(err) }

				if(data.exp){
					send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
				} else {
					send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
				}
				if(data.start){var start = new Date(data.start.datetime) /*- data.start.offset*/}
				if(data.end){var end =  new Date(data.end.datetime) /*- data.end.offset */}

				var where = " where CallDirection = 'Inbound' "
				var transWhere = " where 1=1 "

				if (start){
					where += ' and Call_Begin_DateTime > @start '
					transWhere += ' and Transfer_DateTime > @start '
				}
				if (end){
					where += ' and Call_End_DateTime < @end '
					transWhere += ' and Transfer_DateTime < @end '
				}

				var languageSQL = ''
				if(languages.length > 0){
					languageSQL += ", (Select null) as [langSpacer] "
					for(lang of languages){
						if(lang != null){
							languageSQL += ", (Select Count(*) from CALL_COUNT "+where+" and Language_Selected = '"+lang+"' ) as [Number of Calls Where "+lang+" Was Selected] "
						}
					}
					languageSQL += ", (Select Count(*) from CALL_COUNT "+where+" and Language_Selected IS NULL ) as [Number of Calls That Defaulted to English] "
					console.log(languageSQL)

				}

				var transSQL = ''
				if(transfersEnabled){
					transSQL += ", (Select null) as [transSpacer] "
					transSQL += ", (Select Count(*) from Transfer_History "+transWhere+"  ) as [Number of Calls Which Were Transferred],  (Select ConCAT( Cast(ROUND((Select CAST((Select Count(*) from Transfer_History "+transWhere+" ) as decimal)  * 100 / (Select Count(*) from CALL_COUNT "+where+" )), 0) as int),'%'))  as [Percentage of Calls Transferred], (Select ConCAT(100 - Cast(ROUND((Select CAST((Select Count(*) from Transfer_History "+transWhere+" ) as decimal)  * 100 / (Select Count(*) from CALL_COUNT "+where+" )), 0) as int),'%'))  as [Percentage of Calls Handled Exclusively by the IVR Application] "
				}

				var result = [];
					var request = new Request("Select (Select Count(*) from CALL_COUNT "+where+") as [Total Number of Calls Received], (SELECT RIGHT('0' + CAST(CAST(AVG(Call_Duration)AS INT) / 3600 AS VARCHAR),2) + ':' + RIGHT('0' + CAST((CAST(AVG(Call_Duration)AS INT) / 60) % 60 AS VARCHAR),2) + ':' + RIGHT('0' + CAST(CAST(AVG(Call_Duration)AS INT) % 60 AS VARCHAR),2) from CALL_COUNT "+where+") as [Average Duration of Calls on the IVR Application (hh:mm:ss)]" + languageSQL + transSQL, function (err, rowCount) {
						if (err) { console.log(err) }
						connection.close();
					});

					if (start ){
						request.addParameter('start', TYPES.DateTime, new Date(start));
					}
					if (end ){
						request.addParameter('end', TYPES.DateTime, new Date(end));
					}

					request.on('row', function (columns) {
						var rowObject = {};
						columns.forEach(function (column) {
							rowObject[column.metadata.colName] = column.value;
						});
						result.push(rowObject)
					});

					request.on('doneProc', async function () {
						var reportData = {title: 'Incoming Calls Summary Report', list: result[0]}
						if(data.exp === "excel"){
							app.routes.prepareExcel(data, ws, reportData)
						} else if (data.exp === "pdf"){
							preparePDF(data, ws, reportData)
						} else {
							let sent = await send({route: 'setState', state: {"reportData": reportData, reportLoading: false}}, ws )
						}
					})
				connection.execSql(request);
			});
			transRequest.on('row', function (columns) {
				columns.forEach(function (column) {
					if(column.value > 0) {
						transfersEnabled = true;
					}
				});
			});

			connection.execSql(transRequest);
		});

		langRequest.on('row', function (columns) {
			columns.forEach(function (column) {
				languages.push(column.value);
			});
		});

		connection.execSql(langRequest);
	});
}

app.routes.incomingCallsDetail_language = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(CASE WHEN Language_Selected IS NULL THEN 'Defaulted to English' ELSE Language_Selected END) FROM Call_Count ORDER BY CASE WHEN Language_Selected IS NULL THEN 'Defaulted to English' ELSE Language_Selected END", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "language", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.incomingCallsDetail = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) /*- data.start.offset*/}
	if(data.end){var end =  new Date(data.end.datetime ) /*- data.end.offset*/}
	var callerID = data.callerID
	var language = data.language

	var languageEnabled = ''

	for(param of data.report.parameters){
		if(param.parameter === 'language'){
			languageEnabled = ", CASE WHEN Language_Selected IS NULL THEN 'Defaulted to English' ELSE Language_Selected END as [Language Selected] "
		}
	}


	var where = " where CallDirection = 'Inbound' "
	if (start ){
		where += ' and Call_Begin_DateTime > @start '
	}
	if (end){
		where += ' and Call_End_DateTime < @end '
	}
	if(callerID) {
		where += ' and RemoteParty_Phone_Number =  @callerID '
	}
	if(language === 'Defaulted to English'){
		where += ' and Language_Selected IS NULL '
	}
	else if(language) {
		where += ' and Language_Selected =  @language '
	}

	var config = Object.assign({}, app.orgs[ws.info.domain].settings.databaseRP)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT TOP(100000) RemoteParty_Phone_Number as [Caller ID], Call_Begin_DateTime as [Call Start Date & Time], Call_End_DateTime as [Call End Date & Time], CONVERT(varchar, DATEADD(ss, [Call_Duration], 0), 108) as [Call Duration] "+languageEnabled+" FROM [dbo].[CALL_Count] "+where, function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(callerID) {
				request.addParameter('callerID', TYPES.VarChar, callerID);
			}
			if(language) {
				request.addParameter('language', TYPES.VarChar, language);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'string', label: 'Caller ID'},
				{ type: 'datetime', label: 'Call Start Date & Time'},
				{ type: 'datetime', label: 'Call End Date & Time'},
				{ type: 'string', label: 'Call Duration'}
			]
			if(languageEnabled){
				columns.push(
					{ type: 'string', label: 'Language Selected'}
				)
			}

			request.on('doneProc', async function () {
				var reportData = {title: 'Incoming Calls Detail Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.transferDetail_fromPrompt = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(fromPrompt) FROM Transfer_History ORDER BY FromPrompt", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "fromPrompt", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.transferDetail_transferNumber = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct( SUBSTRING([Transfer_To],5,100)) FROM Transfer_History WHERE Transfer_To != 'sip:Null' ORDER BY SUBSTRING([Transfer_To],5,100)", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "transferNumber", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.transferDetail = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) /*- data.start.offset*/}
	if(data.end){var end =  new Date(data.end.datetime ) /*- data.end.offset*/}
	var callerID = data.callerID
	var fromPrompt = data.fromPrompt
	var transferNumber = data.transferNumber

	var where = ""
	if (start ){
		where += ' and Transfer_DateTime > @start '
	}
	if (end){
		where += ' and Transfer_DateTime < @end '
	}
	if(callerID) {
		where += ' and CallerANI =  @callerID '
	}
	if(fromPrompt) {
		where += ' and FromPrompt =  @fromPrompt '
	}
	if(transferNumber) {
		where += ' and SUBSTRING([Transfer_To],5,100) =  @transferNumber '
	}

	var config = Object.assign({}, app.orgs[ws.info.domain].settings.databaseRP)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT TOP (100000) Transfer_DateTime as [Transfer Date and Time], CallerANI as [Caller ID], FromPrompt as [From Prompt], Menu_Description as [Prompt Description], SUBSTRING([Transfer_To],5,100) as [Transfer Number] FROM Transfer_History join Menu_Descriptions on FromPrompt = Menu_name WHERE 1=1 "+where + " order by Transfer_DateTime Desc", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(callerID) {
				request.addParameter('callerID', TYPES.VarChar, callerID);
			}
			if(fromPrompt) {
				request.addParameter('fromPrompt', TYPES.VarChar, fromPrompt);
			}
			if(transferNumber) {
				request.addParameter('transferNumber', TYPES.VarChar, transferNumber);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'datetime', label: 'Transfer Date and Time'},
				{ type: 'string', label: 'Caller ID'},
				{ type: 'string', label: 'From Prompt'},
				{ type: 'string', label: 'Prompt Description'},
				{ type: 'string', label: 'Transfer Number'}
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'Transfer HIstory Detail Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.transferSummary_fromPrompt = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(fromPrompt) FROM Transfer_History ORDER BY FromPrompt", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "fromPrompt", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.transferSummary_transferNumber = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct( SUBSTRING([Transfer_To],5,100)) FROM Transfer_History WHERE Transfer_To != 'sip:Null' ORDER BY SUBSTRING([Transfer_To],5,100)", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "transferNumber", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.transferSummary = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }
	var fromPrompt = data.fromPrompt
	var transferNumber = data.transferNumber

	var where = ""
	var ccWhere = ""
	if (start ){
		where += ' and Transfer_DateTime > @start '
		ccWhere += ' and Call_Begin_DateTime > @start '
	}
	if (end){
		where += ' and Transfer_DateTime < @end '
		ccWhere += ' and Call_Begin_DateTime < @end '
	}
	if(fromPrompt) {
		where += ' and FromPrompt =  @fromPrompt '
	}
	if(transferNumber) {
		where += ' and SUBSTRING([Transfer_To],5,100) =  @transferNumber '
	}


	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});

	var result = [];
	var result2 = [];
	connection.on('connect', function (err) {
        var request = new Request("Select CONCAT(SUBSTRING([Transfer_To],5,100), ': ', count(Transfer_To)) as [Transfer Number], count(Transfer_To) as [Number of Transfers] from Transfer_History WHERE 1=1 "+where+" group by Transfer_To order by count(Transfer_To) desc; SELECT FromPrompt as [From Prompt], SUBSTRING([Transfer_To],5,100) as [Transfer Number], count(FromPrompt) as [Number of Calls], CONCAT(CAST(count(FromPrompt) * 100.0 / sum(count(FromPrompt)) over() AS DECIMAL(18, 2)) , '%')  as [Percentage of Transfers], CONCAT(CAST(count(FromPrompt) * 100.0 / (select Count(*) from CALL_Count WHERE 1=1 "+ccWhere+" ) AS DECIMAL(18, 2)) , '%')  as [Percentage of Total Calls] FROM Transfer_History WHERE 1=1 "+where+" group by FromPrompt, SUBSTRING([Transfer_To],5,100) order by count(FromPrompt) desc", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(fromPrompt) {
				request.addParameter('fromPrompt', TYPES.VarChar, fromPrompt);
			}
			if(transferNumber) {
				request.addParameter('transferNumber', TYPES.VarChar, transferNumber);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				if(rowArr.length > 2){
					result.push(rowArr)
				} else {
					result2.push(rowArr)
				}

			});

			var columns = [
				{ type: 'string', label: 'From Prompt'},
				{ type: 'string', label: 'Transfer Number'},
				{ type: 'number', label: 'Number of Calls'},
				{ type: 'string', label: 'Percentage of Transfers'},
				{ type: 'string', label: 'Percentage of Total Calls'}
			]
			var columns2 = [
				{ type: 'string', label: 'Transfer Number'},
				{ type: 'number', label: 'Number of Transfers'}
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'Transfer Summary Report', graph: {type: 'pie', title: 'Calls by Transfer Number', columns: columns2, rows: result2 }, table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.outboundDetail_language = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(CASE WHEN Language_Selected IS NULL THEN 'Defaulted to English' ELSE Language_Selected END) FROM Call_Count ORDER BY CASE WHEN Language_Selected IS NULL THEN 'Defaulted to English' ELSE Language_Selected END", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "language", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.outboundDetail_callResult = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(Call_Result) FROM Calls_Made WHERE Call_Result NOT IN ('Answered', 'Dialing') ORDER BY Call_Result", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "callResult", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.outboundDetail = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	var language = data.language

	var languageEnabled = ''

	for(param of data.report.parameters){
		if(param.parameter === 'language'){
			languageEnabled = ", CASE WHEN Language_Selected IS NULL THEN 'Defaulted to English' ELSE Language_Selected END as [Language Selected] "
		}
	}
	if(data.start){var start = new Date(data.start.datetime ) /*- data.start.offset*/}
	if(data.end){var end =  new Date(data.end.datetime ) /*- data.end.offset*/}
	var phone = data.phone
	var RID = data.RID
	var first = data.first
	var last = data.last
	var callResult = data.callResult

	var where = ""
	if (start ){
		where += ' and Call_DateTime > @start '
	}
	if (end){
		where += ' and Call_DateTime < @end '
	}
	if(phone) {
		where +=  " and CASE WHEN Calls_Made.Phone_Number IS NULL THEN SUBSTRING(Call_Count.RemoteParty_Phone_Number, 7, 10) WHEN Calls_Made.Phone_Number = '' THEN SUBSTRING(Call_Count.RemoteParty_Phone_Number, 5, 10) ELSE SUBSTRING(Calls_Made.Phone_Number, 5, 10) END =  @phone "
	}
	if(RID) {
		where += ' and Recipient_ID =  @RID '
	}
	if(first) {
		where += ' and First_Name =  @first '
	}
	if(last) {
		where += ' and Last_Name =  @last '
	}
	if(language === 'Defaulted to English'){
		where += ' and Language_Selected IS NULL '
	}else if(language) {
		where += ' and Language_Selected =  @language '
	}
	if(callResult) {
		where += ' and Call_Result =  @callResult '
	}

	var config = Object.assign({}, app.orgs[ws.info.domain].settings.databaseRP)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT Top(100000) Call_DateTime as [Call Date and Time], CASE WHEN Call_Duration IS NULL THEN CONVERT(varchar, DATEADD(ss, 0, 0), 108)  ELSE CONVERT(varchar, DATEADD(ss, [Call_Duration], 0), 108) END as [Call Duration], CASE WHEN Calls_Made.Phone_Number IS NULL THEN SUBSTRING(Call_Count.RemoteParty_Phone_Number, 7, 10) WHEN Calls_Made.Phone_Number = '' THEN SUBSTRING(Call_Count.RemoteParty_Phone_Number, 5, 10) ELSE SUBSTRING(Calls_Made.Phone_Number, 5, 10) END as [Phone Number],   Recipient_ID as [Recipient ID], First_Name as [First Name], Last_Name as [Last Name] "+languageEnabled+" , Call_Result as [Call Result] FROM [MART].[dbo].[Calls_Made]  left outer join Contact_Info on Contact_Info_RecNum = RecNum left outer join  Call_Count on [Calls_Made].CallId = Call_Count.CallId WHERE Call_Result NOT IN ('Answered', 'Dialing') " + where , function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(phone) {
				request.addParameter('phone', TYPES.VarChar, phone);
			}
			if(RID) {
				request.addParameter('RID', TYPES.VarChar, RID);
			}
			if(first) {
				request.addParameter('first', TYPES.VarChar, first);
			}
			if(last) {
				request.addParameter('last', TYPES.VarChar, last);
			}
			if(language) {
				request.addParameter('language', TYPES.VarChar, language);
			}
			if(callResult) {
				request.addParameter('callResult', TYPES.VarChar, callResult);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'datetime', label: 'Call Date and Time'},
				{ type: 'string', label: 'Call Duration'},
				{ type: 'phone', label: 'Phone Number'},
				{ type: 'string', label: 'Recipient ID'},
				{ type: 'string', label: 'First Name'},
				{ type: 'string', label: 'Last Name'},
				{ type: 'string', label: 'Language Selected'},
				{ type: 'string', label: 'Call Result'}
			]

			request.on('doneProc', async function () {
console.log(result)
				var reportData = {title: 'Outbound Notification Detail Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.outboundSummary = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime) - data.start.offset}
	if(data.end){var end =  new Date(data.end.datetime) - data.end.offset }

	var where = ""

	if (start){
		where += ' and Call_DateTime > @start '
	}
	if (end){
		where += ' and Call_DateTime < @end '
	}
	var config = Object.assign({}, app.orgs[ws.info.domain].settings.databaseRP)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});
	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT  (Select Count(*) from Calls_Made WHERE Call_Result NOT IN ('Answered', 'Dialing')  "+where+" ) as [Total Number of Calls Made], (Select Count(*) from Calls_Made where Call_Result = 'Busy'  "+where+" ) as [Number of Calls With Result of Busy], (Select Count(*) from Calls_Made where Call_Result = 'Failed'  "+where+" ) as [Number of Calls With Result of Failed], (Select Count(*) from Calls_Made where Call_Result = 'Human'  "+where+" ) as [Number of Calls With Result of Human], (Select Count(*) from Calls_Made where Call_Result = 'Voicemail'  "+where+" ) as [Number of Calls With Result of Voicemail]", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end ){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}

			request.on('row', function (columns) {
				var rowObject = {};
				columns.forEach(function (column) {
					if (column.value === null) { } else {
						rowObject[column.metadata.colName] = column.value;
					}
				});
				result.push(rowObject)
			});

			request.on('doneProc', async function () {
				var reportData = {title: 'Outbound Notification Summary Report', list: result[0]}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState', state: {"reportData": reportData, reportLoading: false}}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.optionsDetail_menuName = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(Menu_Name) FROM Menu_Reporting WHERE Response_at_Menu != ''  ORDER BY Menu_Name", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "menuName", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.optionsDetail = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }
	var menuName = data.menuName
	var callerID = data.callerID

	var where = ""
	if (start ){
		where += ' and a.Call_DateTime > @start '
	}
	if (end){
		where += ' and a.Call_DateTime < @end '
	}
	if(menuName) {
		where += ' and a.Menu_Name =  @menuName '
	}
	if(callerID) {
		where += ' and a.Caller_ANI =  @callerID '
	}

	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT TOP(100000) a.Call_DateTime, a.Caller_ANI, a.Menu_Name, b.Menu_Description, a.Response_at_Menu, { fn IFNULL(c.Menu_Response_Description, '') } AS Option_Description FROM Menu_Reporting AS a INNER JOIN Menu_Descriptions AS b ON a.Menu_Name = b.Menu_Name LEFT OUTER JOIN Menu_Option_Descriptions AS c ON a.Menu_Name = c.Menu_Name AND a.Response_at_Menu = c.Menu_Response WHERE (a.Response_at_Menu <> '')" + where, function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(menuName) {
				request.addParameter('menuName', TYPES.VarChar, menuName);
			}
			if(callerID) {
				request.addParameter('callerID', TYPES.VarChar, callerID);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'datetime', label: 'Date and Time'},
				{ type: 'string', label: 'Caller ID'},
				{ type: 'string', label: 'Menu Name'},
				{ type: 'string', label: 'Menu Description'},
				{ type: 'string', label: 'Response at Menu'},
				{ type: 'string', label: 'Option Description'}
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'Menu Options Summary Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.menuOptionsSummary_menuName = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(Menu_Name) FROM Menu_Reporting WHERE Response_at_Menu != ''  ORDER BY Menu_Name", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "menuName", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.menuOptionsSummary = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }
	var menuName = data.menuName

	var where = ""
	if (start ){
		where += ' and a.Call_DateTime > @start '
	}
	if (end){
		where += ' and a.Call_DateTime < @end '
	}
	if(menuName) {
		where += ' and a.Menu_Name =  @menuName '
	}

	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT  a.Menu_Name, b.Menu_Description, a.Response_at_Menu, { fn IFNULL(c.Menu_Response_Description, '') } AS Option_Description, COUNT(*) AS TotalCount FROM  Menu_Reporting AS a INNER JOIN Menu_Descriptions AS b ON a.Menu_Name = b.Menu_Name LEFT OUTER JOIN Menu_Option_Descriptions AS c ON a.Menu_Name = c.Menu_Name AND a.Response_at_Menu = c.Menu_Response WHERE  c.Menu_Response_Description is NOT NULL "+where+" GROUP BY a.Menu_Name, a.Response_at_Menu, b.Menu_Description, c.Menu_Response_Description ORDER BY a.Menu_Name, a.Response_at_Menu", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(menuName) {
				request.addParameter('menuName', TYPES.VarChar, menuName);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					// if(column.metadata.colName == "date"){
						// rowArr.push(format(column.value, 'yyyy/mm/dd hh:mm:ss TT'))
					// } else {
						rowArr.push(column.value);
					// }

				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'string', label: 'Menu Name'},
				{ type: 'string', label: 'Menu Description'},
				{ type: 'string', label: 'Response at Menu'},
				{ type: 'string', label: 'Option Description'},
				{ type: 'number', label: 'Total Count'}
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'Menu Options Summary Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.importHistory = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }

	var where = ""
	if (start ){
		where += ' and Import_Begin_DateTime > @start '
	}
	if (end){
		where += ' and Import_Begin_DateTime < @end '
	}

	var config = Object.assign({}, app.orgs[ws.info.domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT TOP (100000) Import_Begin_DateTime as [Import Date and Time], Name_Of_File_Imported as [Name of File Imported], Number_Of_Records_Imported as [Number of Records Imported],  Errors_During_Import as [Errors During Import] FROM            Import_History where 1=1 " + where, function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'datetime', label: 'Import Date and Time'},
				{ type: 'string', label: 'Name of File Imported'},
				{ type: 'string', label: 'Number of Records Imported'},
				{ type: 'string', label: 'Errors During Import'}
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'File Import History Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.webActivity_user = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.database);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(username) FROM WebActivity ORDER BY username", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "user", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webActivity_firstName = function(data, ws) {
	var config = Object.assign({}, app.orgs[ws.info.domain].settings.database)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(FirstName) FROM WebActivity ORDER BY FirstName", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "firstName", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webActivity_lastName = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.database);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(LastName) FROM WebActivity ORDER BY LastName", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "lastName", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webActivity_activity = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.database);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(activity) FROM WebActivity ORDER BY activity", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "activity", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webActivity = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }
	var user = data.user
		firstName = data.firstName
		lastName = data.lastName
		activity =  data.activity

	var where = ""
	if (start ){
		where += ' and created > @start '
	}
	if (end){
		where += ' and created < @end '
	}
	if(user) {
		where += ' and username =  @user '
	}
	if(firstName) {
		where += ' and FirstName =  @firstName '
	}
	if(lastName) {
		where += ' and LastName =  @lastName '
	}
	if(activity) {
		where += ' and activity =  @activity '
	}

    var config = Object.assign({}, app.orgs[ws.info.domain].settings.database)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT created, username, FirstName, LastName, activity FROM WebActivity where 1=1 "+ where, function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(user) {
				request.addParameter('user', TYPES.VarChar, user);
			}
			if(firstName) {
				request.addParameter('firstName', TYPES.VarChar, firstName);
			}
			if(lastName) {
				request.addParameter('lastName', TYPES.VarChar, lastName);
			}
			if(activity) {
				request.addParameter('activity', TYPES.VarChar, activity);
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'datetime', label: 'Date and Time'},
				{ type: 'string', label: 'Username'},
				{ type: 'string', label: 'First Name'},
				{ type: 'string', label: 'Last Name'},
				{ type: 'string', label: 'Activity'}
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'Menu Options Summary Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.webAccess_user = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.database);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(username) FROM webAccess ORDER BY username", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "user", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webAccess_firstName = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.database);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(FirstName) FROM webAccess ORDER BY FirstName", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "firstName", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webAccess_lastName = function(data, ws) {
	var connection = new Connection(app.orgs[ws.info.domain].settings.database);connection.on('error', function(err) {console.log(err)});
	var result = [];

	connection.on('connect', function (err) {
        var request = new Request("SELECT Distinct(LastName) FROM webAccess ORDER BY LastName", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

		request.on('row', function (columns) {
			var rowArr = [];
			columns.forEach(function (column) {
					rowArr.push(column.value);
			});
			result.push(rowArr)
		});

		request.on('doneProc', async function () {
			send({route: 'alterState', state: "dynamicParams", path: "lastName", value: result }, ws )
			send({route: 'setState', state:{ reportData: {list: '', table: '', graph:'', download: ''}}}, ws)
		})

		connection.execSql(request);
	});
}
app.routes.webAccess = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }
	var user = data.user
		firstName = data.firstName
		lastName = data.lastName

	var where = ""
	if (start ){
		where += ' and LoggedIn > @start '
	}
	if (end){
		where += ' and LoggedIn < @end '
	}
	if(user) {
		where += ' and username =  @user '
	}
	if(firstName) {
		where += ' and FirstName =  @firstName '
	}
	if(lastName) {
		where += ' and LastName =  @lastName '
	}
    var config = Object.assign({}, app.orgs[ws.info.domain].settings.database)
	config.options.useUTC = true
	var connection = new Connection(config);connection.on('error', function(err) {console.log(err)});

	var result = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT username, FirstName, LastName, LoggedIn, LoggedOut FROM WebAccess where 1=1 "+ where, function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}
			if(user) {
				request.addParameter('user', TYPES.VarChar, user);
			}
			if(firstName) {
				request.addParameter('firstName', TYPES.VarChar, firstName);
			}
			if(lastName) {
				request.addParameter('lastName', TYPES.VarChar, lastName);
			}


			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
					rowArr.push(column.value);
				});
				result.push(rowArr)
			});

			var columns = [
				{ type: 'string', label: 'Username'},
				{ type: 'string', label: 'First Name'},
				{ type: 'string', label: 'Last Name'},
				{ type: 'datetime', label: 'Logged In'},
				{ type: 'datetime', label: 'Logged Out'}
			]

			request.on('doneProc', async function () {

				var reportData = {title: 'Menu Options Summary Report', table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

app.routes.callDuration = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime ) }
	if(data.end){var end =  new Date(data.end.datetime ) }

	var where = ""

	if (start ){
		where += ' and Call_Begin_DateTime > @start '
	}
	if (end){
		where += ' and Call_Begin_DateTime < @end '
	}

	var connection = new Connection(app.orgs[ws.info.domain].settings.databaseRP);connection.on('error', function(err) {console.log(err)});

	var result = [];
	var result2 = [];
	connection.on('connect', function (err) {
        var request = new Request("Select Case WHEN dur = 0 then '0-9 Seconds' WHEN dur = 1 then '10-29 Seconds' WHEN dur = 2 then '30-59 Seconds' WHEN dur = 3 then '60-119 Seconds' Else '120+ Seconds' end as Duration,  count(dur) as [Number of Calls],CONCAT(CAST(count(dur) * 100.0 / sum(count(dur)) over() AS decimal(18,2)), '%') as [Percentage of Total Calls] from (SELECT Case WHEN Call_Duration < 9 then 0 WHEN call_duration < 29 then 1 WHEN call_duration < 59 then 2 WHEN call_duration < 119 then 3 Else 4 end as dur FROM [dbo].[Call_Count]) as a  group by dur order by dur", function (err, rowCount) {
			if (err) { console.log(err) }
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}

			request.on('row', function (columns) {
				var rowArr = [];
				var rowArr2  =[]
				columns.forEach(function (column) {
					if(column.metadata.colName == 'Duration' || column.metadata.colName == 'Number of Calls'){
						rowArr2.push(column.value);
					}
					rowArr.push(column.value);
				});

				result.push(rowArr)
				result2.push(rowArr2)

			});

			var columns = [
				{ type: 'string', label: 'Duration'},
				{ type: 'number', label: 'Number of Calls'},
				{ type: 'string', label: 'Percentage of Total Calls'}
			]
			var columns2 = [
				{ type: 'string', label: 'Duration'},
				{ type: 'number', label: 'Number of Calls'},
			]

			request.on('doneProc', async function () {
				var reportData = {title: 'IVR Call Duration Report', graph: {type: 'pie', title: 'Calls by Duration', columns: columns2, rows: result2 }, table: {columns: columns, rows: result }}
				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}


app.routes.reportError = (data, ws) => {
	send({route: 'setState',state: {progress: false, reportLoading: false }}, ws )
	send({route: 'memo', color: '#903', content: 'The report could not be exported.'}, ws);
}
app.routes.peakTimes = function(data, ws) {
	if(data.exp){
		send({route: 'progress', color: '#397', content: 'Requesting Data', perc: 10 }, ws);
	} else {
		send({route: 'setState',state: {reportData: false, reportLoading: true }}, ws )
	}
	if(data.start){var start = new Date(data.start.datetime).toISOString().slice(0, 23) }
	if(data.end){var end =  new Date(data.end.datetime).toISOString().slice(0, 23) }

	var where = ""
	if (start ){
		where += ' and Call_Begin_DateTime > @start '
	}
	if (end){
		where += ' and Call_Begin_DateTime < @end '
	}

	var config = Object.assign({}, app.orgs[ws.info.domain].settings.databaseRP)
	config.options.useUTC = false
	var connection = new Connection(config);connection.on('error', function(err) {log(err);app.routes.reportError(data, ws);return false;});

	var result1 = [];
	var result2 = [];
	connection.on('connect', function (err) {
        var request = new Request("SELECT DATEPART(year, Call_Begin_DateTime) AS y, DATEPART(month, Call_Begin_DateTime) AS m, DATEPART(day, Call_Begin_DateTime) AS d, DATEPART(hour, Call_Begin_DateTime) AS h, CEILING(DATEPART(minute, Call_Begin_DateTime) / 5) * 5 AS mi, MAX(concurrentCalls) AS [Max Calls] FROM            ConcurrentCalls where 1=1 "+ where + " GROUP BY DATEPART(year, Call_Begin_DateTime), DATEPART(month, Call_Begin_DateTime), DATEPART(day, Call_Begin_DateTime), DATEPART(hour, Call_Begin_DateTime), CEILING(DATEPART(minute, Call_Begin_DateTime) / 5) * 5 ORDER bY DATEPART(year, Call_Begin_DateTime), DATEPART(month, Call_Begin_DateTime), DATEPART(day, Call_Begin_DateTime), DATEPART(hour, Call_Begin_DateTime), CEILING(DATEPART(minute, Call_Begin_DateTime) / 5) * 5; SELECT DATEPART(weekday, Call_Begin_DateTime) AS d, DATEPART(hour, CONVERT(varchar, Call_Begin_DateTime, 109)) AS h, CEILING(DATEPART(minute, Call_Begin_DateTime) / 5) * 5 AS mi, MAX(concurrentCalls) AS [Max Calls], MAX(Call_Begin_DateTime) AS day FROM            ConcurrentCalls where 1=1 "+ where + "GROUP BY DATEPART(weekday, Call_Begin_DateTime), DATEPART(hour, CONVERT(varchar, Call_Begin_DateTime, 109)), CEILING(DATEPART(minute, Call_Begin_DateTime) / 5) * 5, DATEName(weekday, Call_Begin_DateTime) ORDER bY DATEPART(weekday, Call_Begin_DateTime), DATEPART(hour, CONVERT(varchar, Call_Begin_DateTime, 109)), CEILING(DATEPART(minute, Call_Begin_DateTime) / 5) * 5", function (err, rowCount) {
			if (err) {log(err);app.routes.reportError(data, ws);return false;}
			connection.close();
		});

			if (start ){
				request.addParameter('start', TYPES.DateTime, new Date(start));
			}
			if (end){
				request.addParameter('end', TYPES.DateTime, new Date(end));
			}

			request.on('row', function (columns) {
				var rowArr = [];
				columns.forEach(function (column) {
						rowArr.push(column.value);
				});
				if( rowArr.length > 5){
					result1.push(rowArr)
				}else {
					result2.push(rowArr)
				}

			});

			var columns1 = [
				{ type: 'datetime', label: 'Date and Time'},
				{ type: 'number', label: 'Max Simultaneous Calls'}
			]
			var columns2 = [
				{ type: 'datetime', label: 'Time of Week'},
				{ type: 'number', label: 'Max Simultaneous Calls'},
				{ type: 'string', role: 'tooltip', p: {html: true}}
			]
			request.on('doneProc', async function () {
				if(data.exp){
					send({route: 'progress', color: '#397', content: 'Exporting Data', perc: 20 }, ws);
				}
				for (var row of result1){
					row.unshift(Date.UTC(row.shift(),row.shift() -1,row.shift(),row.shift(),row.shift()))
					console.log(row)
				}
				for (var row of result2){
					var dayOffset = row.shift()-1 - new Date().getDay()
					if(dayOffset < 0){dayOffset += 7}
					row.unshift(
						Date.UTC(
							new Date().getYear() + 1900,
							new Date().getMonth(),
							new Date().getDate() + dayOffset,
							row.shift(),
							row.shift()
						)
					)
				}
				var reportData = {title: 'Peak Times of Day Report', table: {columns: columns1, rows: result1 }, graph: result2.length > 0? {type: 'bar', columns: columns2, rows: result2 } : undefined}

				if(data.exp === "excel"){
					app.routes.prepareExcel(data, ws, reportData)
				} else if (data.exp === "pdf"){
					preparePDF(data, ws, reportData)
				} else {
					let sent = await send({route: 'setState',state: {reportData: reportData, reportLoading: false }}, ws )
				}
			})
		connection.execSql(request);
	});
}

PDFDocument = require('pdfkit')
async function preparePDF(data, ws, reportData){
	doc = new PDFDocument({layout: 'landscape'})
	var stream = doc.pipe(fs.createWriteStream('export.pdf'))

	stream.on('finish', () => {
		fs.readFile('export.pdf', function(err, data) {if(err){console.log(err)};
			send({route: 'setState', state: {progress: false, download: {data: data.toString('base64'), type:'pdf'}}}, ws)
		});
	});

	doc.image('./src/img/DiRAD.png', 12,0, {width: 125})

	doc.fontSize(16).text(reportData.title, 150, 26, {width: 675, underline: true})
	doc.moveDown(2)

	if(Object.keys(data).length > 0 ) {
		for (report of app.orgs[ws.info.domain].Reports.reportList){                           	//Loop through the organzation's reports
		if(data.route && report.route === data.route){											// Find the specific report that we ran
				for ( key in data ) {																				//Loop through the sent parameters
					if (key === 'route' || key ==='offset'){												//Ignore route and offset.
						//These two are not user defined parameters
					}else {
						for(param of report.parameters){													//Loop through the available parameters for this report
							if (param.parameter === key){												//If it matches one that was sent
								if(param.type === 'datetime'){
									doc.fontSize(12).text(param.label + ': ' + new Date(new Date(data[key].datetime) - data[key].offset).toISOString().slice(0, 16).replace('T', ' '), 25)
								}else if (param.type === 'date') {
									doc.fontSize(12).text(param.label + ': ' + new Date(new Date(data[key].datetime) - data[key].offset).toISOString().slice(0, 10).replace('T', ' '), 25)
								}else {
									doc.fontSize(12).text(param.label + ': ' +  data[key], 25)
								}
							}
						}
					}
				}
			}
		}
	}

	doc.moveDown()
	doc.moveDown()
	if(reportData.list){
		for(prop in reportData.list ){
			var reportText = ''
			if(prop){
				reportText += prop
			}
			if(reportData.list[prop] || reportData.list[prop] == 0){
				reportText += ': ' + reportData.list[prop].toString()
			}
			doc.fontSize(11).text(reportText,25)
		}
	}

	if(reportData.table){
		var perc = Math.floor(reportData.table.rows.length / 8)
		var tick = 0
		var val = 20
		for(entry of reportData.table.columns){
			doc.fontSize(11).text(entry.label.toString() + '     ', {continued: true, align: 'justify'})
		}
		for(entry of reportData.table.rows){
			tick++
			if (tick === perc){
				tick = 0
				send({route: 'progress', color: '#397', content: 'Generating PDF File', perc: val }, ws);
				val = val + 10
			}
			if(doc.y > 650){
				doc.addPage()
			}
			//var boxTop = doc.y
			//doc.moveDown()
			doc.x = 25
			for(i=0; i< entry.length; i++){
				//var column = reportData.table.columns[i].label.toString()
				var row = entry[i] ? entry[i].toString() : ''
				var continued = i == entry.length -1 ? false : true
				//doc.fontSize(11).text(column + ': ', {continued: true, align: 'justify'})
				doc.fontSize(11).text(row + '     ', {continued: continued, align: 'justify'})
			}
			//doc.moveDown()
			//doc.rect(20, boxTop , 550, doc.y - boxTop).stroke()


		}
	}

	doc.end()
}


var	xl = require('excel4node');
app.routes.prepareExcel = (data, ws, reportData) => {
	var wb = new xl.Workbook();

	var sheet = wb.addWorksheet('Sheet1',{
		'sheetFormat': {
			'baseColWidth': 16,
			'defaultColWidth': 16
		}
	});
	var headerStyle = wb.createStyle({
		font: {
			color: '#000000',
			size: 12
		}, alignment: {
			horizontal: 'center'
		}
	});
	var style = wb.createStyle({
		font: {
			color: '#000000',
			size: 12
		},alignment: {
			horizontal: 'right'
		}
	});
	var qStyle = wb.createStyle({
		font: {
			color: '#000000',
			size: 12
		},alignment: {
			horizontal: 'left',
			wrapText: true
		}
	});
	var percStyle = wb.createStyle({
		font: {
			color: '#000000',
			size: 12
		},
		numberFormat: '#.00%; #.00%; #.00%'
	});

	var row = 1
	var col = 1
	var start = data.start
	var end = data.end
	var cols = 2
	if(reportData.table){
		cols = reportData.table.columns.length
	}

	sheet.cell(row,1,row,cols,true).string(reportData.title).style(headerStyle);

	row++
	maxLengths = []

	if(Object.keys(data).length > 0 ) {
		for (report of app.orgs[ws.info.domain].Reports.reportList){                           									//Loop through the organzation's reports
			if(report.route === data.route){											// Find the specific report that we ran
				for ( key in data ) {																				//Loop through the sent parameters
					if (key === 'report' || key ==='offset' || key ==='type' || key ==='reportData'){												//Ignore route and offset.
						//These are not user defined parameters
					}else {
						for(param of report.parameters){													//Loop through the available parameters for this report
							console.log(param)
							if (param.parameter === key){												//If it matches one that was sent
								sheet.cell(row,col).string(param.label).style(headerStyle);
								maxLengths[col] = param.label.length


								if(param.type === 'datetime'){
									sheet.cell(row + 1,col).string(new Date(new Date(data[key].datetime) - data[key].offset).toISOString().slice(0, 16).replace('T', ' ')).style(headerStyle);
									maxLengths[col] = maxLengths[col] > 16 ? maxLengths[col] : 16
								}else if (param.type === 'date') {
									sheet.cell(row + 1,col).string(new Date(new Date(data[key].datetime) - data[key].offset).toISOString().slice(0, 10).replace('T', ' ')).style(headerStyle);
									maxLengths[col] = maxLengths[col] > 10 ? maxLengths[col] : 10
								}else {
									sheet.cell(row + 1,col).string(data[key]).style(headerStyle);
									maxLengths[col] = maxLengths[col] > data[key].length ? maxLengths[col] : data[key].length
								}
							}
						}
						col++
					}
				}
			}
		}
		row = 4
	}



	if(reportData.list){
		for(prop in reportData.list ){
			var name = prop.indexOf('Spacer') > -1 ? ' ' : prop
			if(prop){sheet.cell(row,1).string(name).style(qStyle);
				maxLengths[1] = !maxLengths[1] || prop.length > maxLengths[1] ? prop.length : maxLengths[1]
			}
			if(reportData.list[prop] || reportData.list[prop] == 0){

				sheet.cell(row,2).string(reportData.list[prop].toString()).style(style);
				maxLengths[2] = !maxLengths[2] || reportData.list[prop].toString().length > maxLengths[2] ? reportData.list[prop].toString().length : maxLengths[2]
			}
			row++
		}
	}

	if(reportData.table){
		var col = 1
		for(prop of reportData.table.columns ){
			sheet.cell(row,col).string(prop.label).style(headerStyle);
				maxLengths[col] = !maxLengths[col] || prop.label.toString().length > maxLengths[col] ? prop.label.toString().length : maxLengths[col]
			col++
		}
		row++
		var perc = Math.floor(reportData.table.rows.length / 5)
		var tick = 0
		for(entry of reportData.table.rows){
			tick++
			if (tick === perc){
				tick = 0
				send({route: 'progress', color: '#397', content: 'Generating Excel File', perc: row / reportData.table.rows.length * 100 }, ws);
			}

			col = 1
			for(prop of entry){
				if(prop){sheet.cell(row,col).string(prop.toString()).style(qStyle);
					maxLengths[col] = !maxLengths[col] || prop.toString().length > maxLengths[col] ? prop.toString().length : maxLengths[col]
				}
				col++
			}
			row++
		}
	}

	for ([i,num] of maxLengths.entries()){
		if(i && num){
			sheet.column(i).setWidth(num < 75 ? num + 2 : 85);
		}
	}
// var filename = reportData.title+ '-' + new Date().toJSON()+ '.xlsx'
//     wb.write(filename, (err, stats) =>{
//         send({route: 'setState', state: {progress: false, download: {data: filename , type:'xlsx'}}}, ws)
//     })
	wb.writeToBuffer().then(function (buffer) {
        console.log('buffer done')
		send({route: 'setState', state: {progress: false, download: {data: buffer.toString('base64') , type:'xlsx'}}}, ws)
	});
}
