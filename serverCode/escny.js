var http = require('request')
var client = require('twilio')('ACf947f6b2be3e29469b463219299a7981', 'faabce97715650757f3acdea68a60a50');
var pdf = require('hummus')
var sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');
exp.use(require('body-parser').urlencoded({ extended: true }))
exp.use(express.static('outbound'))
var http = require('request')

exp.post('/fax/sent', (req, res) => {
	const twiml = `
  <Response>
    <Receive action="/fax/received"/>
  </Response>
  `;

  // Send Fax twiml response
  res.type('text/xml');
  res.send(twiml);
});

exp.post('/fax/received', (req, res) => {
  // log the URL of the PDF received in the fax
  console.log(req.body.MediaUrl);
  sendEmail(JSON.stringify(req.body))
  // Respond with empty 200/OK to Twilio
  res.status(200).end();
});

exp.post('/fax/status', (req, res) => {
	log(req.body);
	var fax = req.body
	if(app.orgs.escny.Faxing.faxes.hasOwnProperty(fax.FaxSid)){
		for(param in fax){
            app.orgs.escny.Faxing.faxes[fax.FaxSid][param] = fax[param]
        }
	} else {
		app.orgs.escny.Faxing.faxes[fax.FaxSid] = fax
	}

	for (var listener in app.orgs.escny.listeners.Faxing) {
		send({route: 'alterState', state: "Faxing", path: ["faxes", fax.FaxSid], value: app.orgs.escny.Faxing.faxes[fax.FaxSid] }, app.orgs.escny.listeners.Faxing[listener]);
	}
  // Respond with empty 200/OK to Twilio
  res.status(200).end();
});


exp.get('/fax/download/:sid', (req, res) => {
    res.setHeader("Cache-Control", "no-cache");
    console.log(req.params.sid)
    console.log(app.orgs.escny.Faxing.faxes[req.params.sid])
    if(!app.orgs.escny.Faxing.faxes[req.params.sid].flipped){
        client.fax.faxes(req.params.sid)
            .fetch()
            .then((fax) => {
                res.setHeader("content-disposition", "attachment; filename="+req.params.sid+".pdf");
                http(fax.mediaUrl).pipe(res);
            });
    } else {
        client.fax.faxes(req.params.sid)
            .fetch()
            .then((fax) => {
                var filename = req.params.sid+".pdf"
                var file = fs.createWriteStream(filename);

                file.on('finish', function() {
                    console.log('file finished')
                    var pdfWriter = pdf.createWriterToModify(req.params.sid+".pdf",{modifiedFilePath: req.params.sid+"-flipped.pdf"});
                    var copyingContext = pdfWriter.createPDFCopyingContextForModifiedFile();
                    var parser = copyingContext.getSourceDocumentParser();

                    for(i=0; i< parser.getPagesCount(); i++){
                        var pageObjectID = parser.getPageObjectID(i);
                        var page = parser.parsePage(i);
                        var pageJSObject = page.getDictionary().toJSObject();

                        // in case the prior rotation is needed to calculate the new rotation, as it was in my case, it's available here:
                        var oldRotation = page.getRotate();

                        // create a new version of the page object
                        var objectsContext = pdfWriter.getObjectsContext();
                        objectsContext.startModifiedIndirectObject(pageObjectID);
                        var modifiedPageObject = pdfWriter.getObjectsContext().startDictionary();

                        // copy all but Rotate elements
                        Object.getOwnPropertyNames(pageJSObject).forEach(function(element,index,array) {
                            if (element != 'Rotate') {
                                modifiedPageObject.writeKey(element);
                                copyingContext.copyDirectObjectAsIs(pageJSObject[element]);
                            }
                        });

                        // setup new rotate and finish object
                        modifiedPageObject.writeKey('Rotate');
                        objectsContext
                            .writeNumber(180)
                            .endLine()
                            .endDictionary(modifiedPageObject)
                            .endIndirectObject();
                    }
                    pdfWriter.end();

                    res.setHeader("content-disposition", "attachment; filename="+req.params.sid+"-flipped.pdf");
                    res.download(req.params.sid+"-flipped.pdf", (err)=>{
                        if(err){log(err)};
                        fs.unlink(req.params.sid+"-flipped.pdf", (err)=>{
                            if(err){log(err)};
                        })
                        fs.unlink(req.params.sid+".pdf", (err)=>{
                            if(err){log(err)};
                        })
                    });
                });
                http(fax.mediaUrl).pipe(file);
            });
    }
});

function sendEmail(fax){
	var msg = {
		to: app.orgs.escny.Faxing.settings.notifyReceived,
		from: 'techstaff@dirad.com',
		subject: 'ESCNY Fax Received',
		text: 'A fax has been received.',
		html: '<pre><strong>A fax has been received. <a href="https://faxing.appspot.com?fx='+fax.sid+'#Faxing">Click Here for Details</a>  </strong></pre><br/>'
	};
	sgMail.send(msg);
}

setInterval(getRecentFaxes, 60000)

getRecentFaxes()
function getRecentFaxes(){
    client.fax.faxes.list({limit: 500, pageSize: 500}).then((faxes) => {
        var faxList = {}
        for(fax of faxes){
            faxList[fax.sid] = true

            if(app.orgs.escny.Faxing.faxes.hasOwnProperty(fax.sid)){
                for(param in fax){
                    app.orgs.escny.Faxing.faxes[fax.sid][param] = fax[param]
                }
            } else {
                app.orgs.escny.Faxing.faxes[fax.sid] = fax
            }

            if(!app.orgs.escny.Faxing.faxes[fax.sid].hasOwnProperty('read') && fax.direction == 'outbound' && fax.status == 'delivered'){
                app.orgs.escny.Faxing.faxes[fax.sid].read = true
            }
			else if(fax.direction == 'outbound' && ['busy','failed','no-answer'].indexOf(fax.status) > -1 && (new Date(fax.dateCreated).getTime() > new Date(2019,4,6).getTime()) && false ){
                if(!app.orgs.escny.Faxing.faxes[fax.sid].hasOwnProperty('retryCounter')){
                    app.orgs.escny.Faxing.faxes[fax.sid].retryCounter = 0
                }
                if(app.orgs.escny.Faxing.faxes[fax.sid].retryCounter < 3){
                    app.orgs.escny.Faxing.faxes[fax.sid].status += ' (retrying)'

                    if(!app.orgs.escny.Faxing.faxes[fax.sid].retried){
                        (function(retry){
                            setTimeout(function(){
                                app.orgs.escny.Faxing.faxes[retry.sid].status = 'retried'
                                client.fax.faxes
                                    .create({
                                     from: retry.from,
                                     to: retry.to,
                                     mediaUrl: retry.mediaUrl
                                   })
                                  .then((fax) => {
                                        app.orgs.escny.Faxing.faxes[fax.sid] = fax
                                        app.orgs.escny.Faxing.faxes[fax.sid].userID = retry.userID
                                        app.orgs.escny.Faxing.faxes[fax.sid].user = retry.user
                                        app.orgs.escny.Faxing.faxes[fax.sid].status += 'retrying'
                                        app.orgs.escny.Faxing.faxes[fax.sid] = retry.retryCounter + 1
                                        for (var listener in app.orgs.escny.listeners.Faxing) {
                                            send({route: 'alterState', state: "Faxing", path: ["faxes", data.sid], value: app.orgs.escny.Faxing.faxes[fax.sid] }, app.orgs.escny.listeners.Faxing[listener]);
                                        }
                                  }).catch((err)=>{
                                      log('ERROR SENDING FAX')
                                      log(err)
                                  });
                            },300000)
                        })(Object.assign({},fax))
                    }

                    app.orgs.escny.Faxing.faxes[fax.sid].retried = true
                }else {
                    app.orgs.escny.Faxing.faxes[retry.sid].status = 'retried'
                }
            } else {

			}
        }

        for(fax in app.orgs.escny.Faxing.faxes){
            if(!faxList.hasOwnProperty(fax)){
                delete app.orgs.escny.Faxing.faxes[fax]
            }
        }

        for (var listener in app.orgs.escny.listeners.Faxing) {
            send({route: 'setState', state: {Faxing: app.orgs.escny.Faxing}}, app.orgs.escny.listeners.Faxing[listener]);
        }
    })

}

app.routes.markFaxAsFlipped = (data, ws) => {
    app.orgs.escny.Faxing.faxes[data.sid].flipped = true
    for (var listener in app.orgs.escny.listeners.Faxing) {
        send({route: 'alterState', state: "Faxing", path: ["faxes", data.sid], value: app.orgs.escny.Faxing.faxes[data.sid] }, app.orgs.escny.listeners.Faxing[listener]);
    }
}

app.routes.markFaxAsUnflipped = (data, ws) => {
    app.orgs.escny.Faxing.faxes[data.sid].flipped = false
    for (var listener in app.orgs.escny.listeners.Faxing) {
        send({route: 'alterState', state: "Faxing", path: ["faxes", data.sid], value: app.orgs.escny.Faxing.faxes[data.sid] }, app.orgs.escny.listeners.Faxing[listener]);
    }
}

app.routes.markFaxAsRead = (data, ws) => {
    app.orgs.escny.Faxing.faxes[data.sid].read = true
    for (var listener in app.orgs.escny.listeners.Faxing) {
        send({route: 'alterState', state: "Faxing", path: ["faxes", data.sid], value: app.orgs.escny.Faxing.faxes[data.sid] }, app.orgs.escny.listeners.Faxing[listener]);
    }
}

app.routes.markFaxAsUnread = (data, ws) => {
    app.orgs.escny.Faxing.faxes[data.sid].read = false
    for (var listener in app.orgs.escny.listeners.Faxing) {
        send({route: 'alterState', state: "Faxing", path: ["faxes", data.sid], value: app.orgs.escny.Faxing.faxes[data.sid] }, app.orgs.escny.listeners.Faxing[listener]);
    }
}

app.routes.deleteFax = (data, ws) => {
    client.fax.faxes(data.sid).remove()
    .then((fax) => {
        log(fax)
    	delete app.orgs.escny.Faxing.faxes[data.sid]

        for (var listener in app.orgs.escny.listeners.Faxing) {
            send({route: 'deleteState', state: "Faxing", path: ["faxes", data.sid]}, app.orgs.escny.listeners.Faxing[listener]);
        }
    }).catch((err) => {
        log(err)
    })
}

app.routes.sendFax = (data, ws) => {
    var filename = new Date().getTime()
    fs.writeFile('files/' + filename + '.pdf', data.fax.file, {encoding: 'base64'}, (err)=>{if(err){log(err)}
        var pdfReader = pdf.createReader('files/' + filename + '.pdf');
        var pageCount = pdfReader.getPagesCount() + 1;
        delete pdfReader;

        var pdfWriter = pdf.createWriter('outbound/'+ filename +'.pdf')

        var coverPage = pdfWriter.createPage(0,0,850,1100)

        var nplogo = pdfWriter.createFormXObjectFromPNG('public/logo2.png')
        var custlogo = pdfWriter.createFormXObjectFromPNG('public/eye-surgeons-logo.png')
        var content = pdfWriter.startPageContentContext(coverPage)

        var headerOptions = {
        			font: pdfWriter.getFontForFile('arial.ttf'),
        			size: 20,
        			colorspace: 'gray',
        			color: 0x00,
        			underline: false
        		};

        var textOptions = {
        			font: pdfWriter.getFontForFile('arial.ttf'),
        			size: 14,
        			colorspace: 'gray',
        			color: 0x00,
        			underline: false
        		};

        content.q()
            .cm(1,0,0,1,169,900)
            .doXObject(custlogo)
            .Q();

        content.q()
            .cm(1,0,0,1,100,100)
            .doXObject(nplogo)
            .Q();

        content.writeText('Date:', 100, 700, headerOptions)
        content.writeText(new Date().toLocaleString(), 300, 700, headerOptions)

        content.writeText('Fax Sent To:', 100, 670, headerOptions)
        content.writeText(data.fax.toName, 300, 670, headerOptions)

        content.writeText('At Company:', 100, 640, headerOptions)
        content.writeText(data.fax.toCompany, 300, 640, headerOptions)

        content.writeText('Fax Number:', 100, 610, headerOptions)
        content.writeText(data.fax.toNumber, 300, 610, headerOptions)

        content.writeText('Number of Pages:', 100, 580, headerOptions)
        content.writeText(pageCount, 300, 580, headerOptions)

        content.writeText('Fax Sent From:', 100, 550, headerOptions)
        content.writeText(data.fax.fromName, 300, 550, headerOptions)

        content.writeText('Message:', 100, 520, headerOptions)

        var messageArray = data.fax.message.split('')

        var i = 0
        var lineOfText = []
        var lineHeight = 490
        while(messageArray.length > 0){
            i++
            var letter = messageArray.shift()

            lineOfText.push(letter)
            if((i > 100 && letter == ' ') || i > 110 || messageArray.length == 0){
                content.writeText(lineOfText.join(''), 100, lineHeight, textOptions)
                i = 0
                lineOfText = []
                lineHeight = lineHeight - 20
            }
        }


        content.writeText('Confidentiality Notice: This facsimile transmission is intended only for the use by the individual or entity', 100, 200, textOptions)
        content.writeText('to which it is addressed and may contain confidential information belonging to the sender who is protected', 100, 180, textOptions)
        content.writeText('by the doctor-patient privilege.  If you are not the intended recipient, you are hereby notified that any', 100, 160, textOptions)
        content.writeText('disclosure, copying, distribution or taking any action in reliance on the contents of this information is', 100, 140, textOptions)
        content.writeText('strictly prohibited.  If you have received this transmission in error, please immediately notify the sender', 100, 120, textOptions)
        content.writeText('by telephone to arrange for the documents return. Thank you.', 100, 100, textOptions)

        pdfWriter.writePage(coverPage)
        pdfWriter.appendPDFPagesFromPDF('files/' + filename + '.pdf')
        pdfWriter.end()

        client.fax.faxes
            .create({
             from: '+13154587818',
             to: '+1' + data.fax.toNumber,
             mediaUrl: 'https://faxer.azurewebsites.net/'+filename+'.pdf'
           })
          .then((fax) => {
                app.orgs.escny.Faxing.faxes[fax.sid] = fax
                app.orgs.escny.Faxing.faxes[fax.sid].userID = ws.info.user
                app.orgs.escny.Faxing.faxes[fax.sid].user = app.orgs.escny.Users[ws.info.user].wholeName
                for (var listener in app.orgs.escny.listeners.Faxing) {
                    send({route: 'alterState', state: "Faxing", path: ["faxes", data.sid], value: app.orgs.escny.Faxing.faxes[fax.sid] }, app.orgs.escny.listeners.Faxing[listener]);
                }
          }).catch((err)=>{
              log('ERROR SENDING FAX')
              log(err)
          });
    })
}


//client.usage.records.each({category: ['pfax-pages' ]}, (record) => console.log(record));
//client.usage.records.each({category: ['pfax-minutes' ]}, (record) => console.log(record));
