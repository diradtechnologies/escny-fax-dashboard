import React from 'react';
import menuUser from './img/menuUser.svg';
import menuChart from './img/menuChart.svg';
import menuClock from './img/menuClock.svg';
import menuHelp from './img/menuHelp.svg';
import menuMic from './img/menuMic.svg';
import menuStar from './img/menuStar.svg';
import menuRole from './img/menuRole.svg';
import menuCalendar from './img/menuCalendar.svg';
import menuHours from './img/menuHours.svg';
import menuPhone from './img/menuPhone.svg';
import menuGear from './img/menuGear.svg';
import menuSpeech from './img/menuSpeech.svg';
import menuWorkflow from './img/menuWorkflow.svg';
import menuExclusion from './img/menuExclusion.svg';
import menuWarning from './img/menuWarning.svg';
import fax from './img/fax.svg';

export class Navigation extends React.PureComponent {
	activate = view => e => {
		window.location.hash = view;
		e.stopPropagation();
	}

	render() {
		var navOptions = []
		for (var option of this.props.availableViews){
			switch(option.view) {
				case 'RealTimeStats':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuClock} alt="RealTimeStats Icon"/><div>{option.title}</div></div>)
					break;
				case 'SpecialMessages':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuMic} alt="SpecialMessages Icon"/><div>Special Messages</div></div>)
					break;
				case 'PromoJackpots':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuStar} alt="PromoJackpots Icon"/><div>Promo Jackpots</div></div>)
					break;
				case 'HolidayRaffle':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuStar} alt="Holiday Raffle Icon"/><div>Holiday Raffle</div></div>)
					break;
				case 'Users':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuUser} alt="Users Icon"/><div>Users</div></div>)
					break;
				case 'Roles':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuRole} alt="Roles Icon"/><div>Roles</div></div>)
					break;
				case 'Reports':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuChart} alt="Reports Icon"/><div>Reports</div></div>)
					break;
				case 'Help':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuHelp} alt="Help Icon"/><div>Help</div></div>)
					break;
				case 'Holidays':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuCalendar} alt="Holidays Icon"/><div>Holidays</div></div>)
					break;
				case 'BusinessHours':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuHours} alt="Business Hours Icon"/><div>Business Hours</div></div>)
					break;
				case 'CallingHours':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuPhone} alt="Calling Hours Icon"/><div>Calling Hours</div></div>)
					break;
				case 'Settings':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuGear} alt="Settings Icon"/><div>Admin Settings</div></div>)
					break;
				case 'SayAs':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuSpeech} alt="Say As Icon"/><div>Say As...</div></div>)
					break;
				case 'Exclusions':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuExclusion} alt="Exclusions Icon"/><div>Exclusions</div></div>)
					break;
				case 'Workflows':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuWorkflow} alt="Workflows Icon"/><div>Workflows</div></div>)
					break;
				case 'TransferNumbers':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuWorkflow} alt="Workflows Icon"/><div>Transfer Numbers</div></div>)
					break;
				case 'DelayMessages':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuWarning} alt="Workflows Icon"/><div>Delay Messages</div></div>)
					break;
                case 'Faxing':
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={fax} alt="Workflows Icon"/><div>Faxing Dashboard</div></div>)
					break;
				default:
					navOptions.push(<div key={option.view} className="menuOption" onClick={this.activate(option.view)}><img src={menuHelp} alt="Default Icon"/><div>{option.view}</div></div>)
					break;
			}
		}

		return(
			<nav className={this.props.menu ? 'active' : ''} style={{overflowY: 'auto',overflowX: 'hidden'}}>
				<div style={{position: 'absolute', width: '300px'}}>
					{navOptions}
				</div>
			</nav>
		)
	}
}
