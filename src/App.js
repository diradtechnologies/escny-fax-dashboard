import React, { Component }     from 'react';
import './App.css';
import { Navigation }			from './Navigation.js';
import { Login }				from './login.js';
import { instanceOf }			from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

var Views = {}
var comps = require.context('./Views', false, /.*/)
comps.keys().forEach((item) => { Views[item.replace('./', '')] = comps(item).default})

var img = {}
var imgs = require.context('./img', false, /.*/)
imgs.keys().forEach((item) => { img[item.replace('./', '').split('.')[0]] = imgs(item)})

class App extends Component {
	static propTypes = {cookies: instanceOf(Cookies).isRequired}

	constructor(props) {
		super(props);
		this.state = {
			loggedIn: false,
			user: '',
			domain:'',
			org: '',
			PIN: '',
			progress: false,
			dynamicParams: {},
			RealTimeStats: {},
			firstName: '',
			lastName: '',
			adjustingPIN: false,
			adjustPIN: '',
			adjustPINError: '',
			role: '',
			availableViews: [],
			views: [],
			memo: false,
			loginError: '',
			activeView: "RealTimeStats"
		};
	}

	adjustPIN = (e) => {
		this.setState({adjustingPIN: true, adjustPIN: '', adjustPINError:''})
	}
	adjustPINChange = (e) => {
		this.setState({adjustPIN: e.target.value, adjustPINError: ''});
	}
	confirmAdjustPIN = () => {
		if (this.state.adjustPIN.length < 1){this.setState({adjustPINError: 'Please Enter a New Password'})};
		this.setState({ loading: true });
		this.ws.send(JSON.stringify({route: 'adjustPIN', user: this.state.user,  adjustPIN: this.state.adjustPIN }))
	}

	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmAdjustPIN();
		}
	}

	revertToDashboard(e) {
		window.location.hash = ''
		e.stopPropagation();
	}

	componentWillMount(){
		this.connectWebSocket()

		this.cookies = this.props.cookies;
		this.saveCreds = (loginCreds) => {
			this.setState({loginCreds: loginCreds})
			this.cookies.set('loginCreds', loginCreds, { path: '/' })
		}
		if(this.cookies.get('loggedIn') === 'true' ) {
			this.setState({
				loggedIn: this.cookies.get('loggedIn') === 'true' ? true : false,
				loginCreds: this.cookies.get('loginCreds'),
				activeView: '',
			});
		}

		this.pingTimer = 1000
		this.ping = setTimeout(this.checkSocket, this.pingTimer)
	}

	connectWebSocket = () => {
		if(this.ws){
			this.ws.close()
			delete this.ws
		}
		var protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:'
        var port = window.location.hostname == 'localhost' ? ':8765' : window.location.port ? ':' + window.location.port : ''

		this.forceUpdate()

		this.ws = new WebSocket(protocol + '//'+ window.location.hostname + port);

		this.ws.onopen = () => {
			if(this.state.loggedIn) {
				this.ws.send(JSON.stringify({route: 'login', loginCreds: this.state.loginCreds }))
			}
		};

		this.ws.onmessage  = (e) => {
			var data = JSON.parse(e.data);

			if(data.route === "setState") {
				if(data.state.hasOwnProperty('RealTimeStats')){
					if(this.state.activeView === 'RealTimeStats'){
						console.log("Setting Application State")
						console.log(data.state)
						this.setState(data.state)
					}
				} else {
					console.log("Setting Application State")
					console.log(data.state)
					this.setState(data.state)
					//this.setState({adjustingPIN: false, loading: false})
					for(var stateType in data.state) {
                        if(stateType === 'loggedIn' ) {
                            this.cookies.set(stateType, data.state[stateType], { path: '/' })
                        }
					}
				}
			} else if (data.route === "alterState"){
				console.log("Altering Application State")
                console.log(data.state)
                console.log(data.path)
				console.log(data.value)

				var altState = {}

				if(this.state.hasOwnProperty(data.state)){
					altState[data.state] = this.state[data.state]
				} else {
					altState[data.state] = {}
				}

                if(typeof data.path == 'string' ){
                    altState[data.state][data.path] = data.value
                } else if(typeof data.path == 'object' ) {
                    var target = altState[data.state]
                    while (data.path.length > 1 ) {
                        target = target[data.path.shift()]
                        console.log(target)
                    }
                    target[data.path.shift()] = data.value
                }

				this.setState(altState)
			} else if (data.route === "deleteState"){
				console.log("Deleting Application State")
				console.log(data.path)

				var delState = {}
                delState[data.state] = this.state[data.state]

                if(typeof data.path == 'string' ){
                    delete delState[data.state][data.path]
                } else if(typeof data.path == 'object' ) {
                    var target = delState[data.state]
                    while (data.path.length > 1 ) {
                        target = target[data.path.shift()]
                        console.log(target)
                    }
                    delete target[data.path.shift()]
                }

				this.setState(delState)
			} else if (data.route === "memo"){
				console.log("Alerting with memo")
				console.log(data.content)

				this.setState({memo: data})
			}else if (data.route === "progress"){
				console.log("Updating Progress")
				console.log(data.content)

				this.setState({progress: data})
			}
		};
	}

    appendPath = (state, path) => {

    }

	checkSocket = () => {
		this.ping = setTimeout(this.checkSocket, this.pingTimer)

		if(this.ws.readyState !== 1){
			this.forceUpdate();
			console.log(this.ws.readyState)
			this.pingTimer = Math.round(this.pingTimer * 1.25)
			this.connectWebSocket()
		} else {
			this.pingTimer = 1000
		}
	}

	logout = () => {
		this.ws.send(JSON.stringify({route: 'logOut'}))
	}

	cancel = () => {
		this.setState({memo: false, adjustingPIN: false, adjustPIN: '', adjustPINError: '', progress: false})
	}

	activateView = () => {
		if(window.location.hash.length > 3){
			this.setState({activeView: window.location.hash.replace('#', '')})
		}else if(this.state.availableViews.length >0){
			window.location.hash = this.state.availableViews[0].view
			this.activateView()
		}
	}

	componentDidMount() {
		window.addEventListener("hashchange", this.activateView, false);
	}

	componentDidUpdate() {
		if(window.location.hash.length < 3 && this.state.views.length > 0 ){
			this.activateView()
		}
		if(this.state.loggedIn && !this.state.activeView){
			this.activateView()
		}
	}

	componentWillUnmount() {
		window.removeEventListener("hashchange", this.activateView, false);
	}

	render() {
		var adjustPIN = ""
		if (this.state.adjustingPIN || this.state.passwordExpired) {
			adjustPIN = (
				<div className="blackout">
					<div className="pop" onKeyUp={this.checkEnter}>
						{this.state.passwordExpired ? '' : <div className="viewCloser" style={{display: 'block'}} onClick={this.cancel}>X</div>}

						<div className="snazzyInputBox">
							<input className="textInput" type="text" required value={this.state.adjustPIN} onChange={this.adjustPINChange} ></input>
							<span className="inputBar"></span><label className="textLabel">Choose Your New Password</label>
						</div>
                        <div className="error">{this.state.adjustPINError}</div>
						{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmAdjustPIN}>Change My Password</div>}
					</div>
				</div>
			)
		}
		var memo = ""
		if (this.state.memo) {
			memo = (
				<div className="blackout">
					<div className="pop" onKeyUp={this.checkEnter}>
						<div className="viewCloser" style={{display: 'block'}} onClick={this.cancel}>X</div>

						<div style={{color: this.state.memo.color, marginBottom: '25px'}} >
							{this.state.memo.content}
						</div>

						{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.cancel}>Okay</div>}
					</div>
				</div>
			)
		}

		var progress = ""
		if (this.state.progress) {
			progress = (
				<div className="blackout">
					<div className="pop" onKeyUp={this.checkEnter} style={{ width: '300px'}}>
						<div className="viewCloser" style={{display: 'block'}} onClick={this.cancel}>X</div>
						{this.state.progress.content}
						<div style={{height: '20px', transition: 'width .5s', backgroundColor: this.state.progress.color, width: this.state.progress.perc + '%', margin: '25px 0px'}} ></div>
					</div>
				</div>
			)
		}
		if (!this.state.loggedIn) {
			return (
				<Login saveCreds={this.saveCreds} ws={this.ws} onLoginAttempt={this.loginAttempt} onLogin={this.successfulLogin}  loggedIn={this.state.loggedIn} loginError={this.state.loginError}/>
			);
		}else {
			for (var views = [], i = 0; i < this.state.views.length; i++){
				for (var viewData = {}, a = 0; a < this.state.views[i].data.length; a++) {
					viewData[this.state.views[i].data[a]] = this.state[this.state.views[i].data[a]];
				}
				viewData[this.state.views[i].view] = this.state[this.state.views[i].view]
				//views.push(<View features={this.state.features} view={this.state.views[i].view} title={this.state.views[i].title} active={this.state.activeView === this.state.views[i].view ? true: false} ws={this.ws} data={viewData} key={"view" + i}/>)
                var  ViewType = Views[this.state.views[i].view]
                window[this.state.views[i].view + 'Functions'] = ViewType
                views.push(
            		<div key={"view" + i} id={this.state.views[i].view} className={this.state.activeView === this.state.views[i].view ? 'active view' : 'view'} >
            			<div className="viewHeader">
                            <h1>{this.state.views[i].title}</h1>
            			</div>
            			<ViewType img={img} view={this.state.views[i].view} ref={(component) => {if(component){window[component.props.view + 'Functions'] = component }}} features={this.state.features} ws={this.ws} data={viewData} active={this.state.activeView === this.state.views[i].view}/>
            		</div>
                )
			}

			return (
				<div style={{display: "flex"}}>
					<Navigation menu={this.state.menu} availableViews={this.state.availableViews} />

					<div className="App">
						<div className="header">
							<img src={img.dirad_logo} className="App-logo" alt="logo" onClick={this.revertToDashboard} />
						</div>

						{adjustPIN}
						{memo}
						{progress}

						<div className="body">
							<div className="viewer activated">
								{views}
							</div>
						</div>

						<div className="footer">
							<span>
								<img src={this.ws && this.ws.readyState === 1 ? img.tinyConnected : img.tinyDisconnected} alt="Connection Icon"/>
								<span style={{lineHeight: '28px'}}>{this.ws && this.ws.readyState === 1 ? "Connected" : "Reconnecting"}</span>
							</span>

							<span>
								<img style={{margin: '0px 5px'}} src={img.tinyBuilding} alt="Domain Icon"/>
								<span style={{lineHeight: '28px'}}>{this.state.org}</span>
							</span>

							<span>
								<img src={img.tinyUser} alt="User Icon"/>
								<span style={{lineHeight: '28px'}}>{this.state.user + " ("  +this.state.firstName + ' ' + this.state.lastName + ")"}</span>
							</span>

							<span style={{cursor: 'pointer'}} onClick={this.adjustPIN}>
								<img src={img.tinyLock} alt="Lock Icon"/>
								<span style={{lineHeight: '28px'}}>Change My Password</span>
							</span>

							<span style={{cursor: 'pointer'}} onClick={this.logout}>
								<img src={img.tinyLogout} alt="Logout Icon"/>
								<span style={{lineHeight: '28px'}}>Logout</span>
							</span>
						</div>
					</div>
				</div>
			);
		}
	}
}

export default withCookies(App);
