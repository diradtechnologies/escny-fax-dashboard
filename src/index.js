import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { CookiesProvider } from 'react-cookie';
import 'core-js'

ReactDOM.render(<CookiesProvider><App /></CookiesProvider>, document.getElementById('root'));

