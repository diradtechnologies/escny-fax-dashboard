import React, { Component } from 'react';
import logo from './img/dirad_logo.png';

export class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			errorBg: 0,
			loginConfig: [
				{
					data : "user",
					type : "text",
					label : "Username",
					placeholder : "Username",
					error : "",
					validation : {}
				},
				{
					data : "password",
					type : "password",
					label : "Password",
					placeholder : "6+ Character Password",
					error : "",
					validation : {}
				}
			],
			loginCreds: {},
		};
	}

	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.login();
		}
	}

	fieldChange = (e) =>{
		var newCreds = this.state.loginCreds
		newCreds[e.currentTarget.getAttribute('data-value')] = e.currentTarget.value
		this.setState({loginCreds: newCreds})
	}

	login = (e) => {
		this.setState({ loading: true });
		this.props.saveCreds(this.state.loginCreds)
		window.setTimeout(function(){
			this.props.ws.send(JSON.stringify({route: 'login', loginCreds: this.state.loginCreds   }))
		}.bind(this), 200)

	}

	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.setState({ loading: false, errorBg: 1 });
		}
		setTimeout(function(){
			this.setState({ errorBg: 0 });
		}.bind(this),1000)
	}

	render() {
		var loginComps = []

		for ( var comp of this.state.loginConfig ) {
			loginComps.push(
				<div className="snazzyInputBox" key={comp.data}>
					<div className="error">{comp.error}</div>
					<input className="textInput" type={comp.type} required value={this.state.loginCreds[comp.data] ? this.state.loginCreds[comp.data] : ''} data-value={comp.data} onChange={this.fieldChange} ></input>
    				<span className="inputBar"></span>
					<label className="textLabel">{comp.label}</label>
				</div>
			)
		}

		return (
			<div className="App ">
				<div className="backdrop"></div>

				<div className="loginBox" onKeyUp={this.checkEnter}>
					<img src={logo} className="App-logo" alt="logo" />

					{loginComps}

					<div style={{position: 'relative', padding: '1px', margin: '0px auto'}}>
						{this.props.loginError ? <div className="error" style={{backgroundColor: 'rgba(144,48,112,'+this.state.errorBg+')'}}>{this.props.loginError}</div> : ''}
						{this.state.loading ? <div className="loader"></div> : <div className="button" onClick={this.login}>Log In</div>}
					</div>
				</div>
			</div>
		)
	}
}
