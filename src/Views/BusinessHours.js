import React, { Component }  from 'react';
import Flatpickr from 'react-flatpickr'
var ReactSuperSelect = require('react-super-select')

class BusinessHours extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editingHours: false,
			loading: false,
			application: false
		};
	}

	cancel = () => {
		this.setState({editingHours: false, loading: false, game: false, start:false, end: false})
	}

	editHours = (day) => e => {
		this.setState({editingHours: true, loading: false, id: day.Id, day: day.Day_of_Week, error: '', start: day.Open_Time, end: day.Close_Time, open: day.is_open})
	}
	confirmHours = () => {
		if (new Date(this.state.start) > new Date( this.state.end)) {
			this.setState({error: 'Close time must not be before Open time.'});
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editHours', id: this.state.id, start: this.state.start, end: this.state.end, open: this.state.open  }))
		}
	}

	toggle = (param) => (e) => {
		var newState = Object.assign({}, this.state)
		newState[param] = this.state[param] ? false : true
		this.setState(newState)
	}

	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmHours();
		}
	}
	toggleApplication = (option) =>  {
		if(option){
			this.setState({application: option.id, error: ''});
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.data.BusinessHours && nextProps.data.BusinessHours.length > 0 && this.state.application === false){
			this.setState({application: nextProps.data.BusinessHours[0].Application})
		}
		if(this.state.loading) {
			this.cancel();
		}
	}

	render() {
		if (this.props.active) {
			var appList = []
			var options = []
			var bhList = []
			var bhData = JSON.parse(JSON.stringify(this.props.data.BusinessHours))
			bhData.sort(function(a,b){
				var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Partial Holiday','Partial_Holiday']
				return days.indexOf(a.Day_of_Week) - days.indexOf(b.Day_of_Week)
			})
			for (var day of bhData) {
				if(appList.indexOf(day.Application) < 0 ){
					appList.push(day.Application)
					options.push({id:day.Application, name: day.name})
				}
				if(day.Application === this.state.application){
					bhList.push(
						<div className="dataRow" key={day.Id} onClick={this.editHours(day)}>
							<div className="activeBox">
								<div className={day.is_open ? "enabled" : "disabled"}></div>
							</div>
							<span style={{flex: '0 0 150px'}} >{day.Day_of_Week}</span>
							<span style={{flex: '1 0 20%', textAlign:'center'}}>{new Date(day.Open_Time).toLocaleTimeString()}</span>
							<span style={{flex: '1 0 20%', textAlign:'center'}}>{new Date(day.Close_Time).toLocaleTimeString()}</span>
						</div>
					)
				}
			}


			var editHours = ""
			if (this.state.editingHours) {
				editHours = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Editing {this.state.application} Hours for {this.state.day}</div>
							<br/>

							<div style={{display: 'inline-block'}}>
								<div>Open Time</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{enableTime: true, noCalendar:true, dateFormat:"h:i K"}} value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>

							<div style={{display: 'inline-block'}}>
								<div>Close Time</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{enableTime: true, noCalendar:true, dateFormat:"h:i K"}} value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) : this.setState({end: undefined}) }} />
							</div>

							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Open</div>
									<div className="activeBox" onClick={this.toggle('open')}>
										<div className={this.state.open ? "enabled" : "disabled"}></div>
									</div>
								</div>
							</div>

							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>

							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmHours}>Confirm Hours</div>}
						</div>
					</div>
				)
			}


			return (
				<div width="100%">
					<div style={{display: 'flex'}}>
						<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px', display: 'inline-block'}}  onChange={this.toggleApplication} initialValue={{id:this.props.data.BusinessHours[0].Application, name: this.props.data.BusinessHours[0].name}} placeholder={"Application"} clearable={false} closeOnSelectedOptionClick={false} dataSource={options} />
					</div>
					<div className="dataHeaders">
						<span style={{flex: '0 0 86px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="enabled">Open?</span>
						<span style={{flex: '0 0 150px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="name">Day</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Open Time</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Close Time</span>
					</div>
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{bhList}
					</div>

					{editHours}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default BusinessHours
