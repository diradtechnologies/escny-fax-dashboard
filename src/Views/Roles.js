import React from 'react';
import SnazzyInput from '../snazzyInput.js'
import del from '../img/delete.svg';
import plus from '../img/plus.svg';
import userIcon from '../img/user.svg';
import roleIcon from '../img/role.svg';

class Roles extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			editingRole: false,
			editingUsers: false,
			usersToAdd: [],
			usersToRemove:[],
			editRole: '',
			addingRole: false,
			permissionsChanged: {},
			filterBy: {},
			sortBy:'name',
			sortOrder: 1,
		};
	}
	cancel = () => {
		this.setState({editingRole: false, editingUsers: false, addingRole: false, deletingRole: false, editRole: '', loading: false, permissionsChanged:{},usersToAdd: [], usersToRemove:[]})
	}

	addRole = () => {
		this.setState({addingRole: true, editRole: '', permissionsChanged: {}, roleNameError: ''})
	}
	addRoleNameChange = (e) => {
		if(e.currentTarget.value.length < 1 ){
			this.setState({editRole: '', roleNameError: 'Please enter a name for the new role.'})
		}else if (this.props.data.Roles.hasOwnProperty(e.currentTarget.value)) {
			this.setState({editRole: e.currentTarget.value, roleNameError: 'This role already exists.'})
		} else {
			this.setState({editRole: e.currentTarget.value, roleNameError: ''})
		}
	}
	confirmAddRole = (e) => {
		if(this.state.editRole.length < 1 ){
			this.setState({roleNameError: 'Please enter a name for the new role.'})
		}else if (this.props.data.Roles.hasOwnProperty(this.state.editRole)) {
			this.setState({roleNameError: 'This role already exists.'})
		} else {
			this.setState({loading: true})
			this.props.ws.send(JSON.stringify({route: 'addRole', editRole: this.state.editRole,  permissionsChanged:  this.state.permissionsChanged}))
		}
	}


	editRole = (e) => {
		this.setState({editingRole: true, editRole: e.currentTarget.getAttribute('data-role') })
	}
	togglePermission = (e) => {
		var module = e.currentTarget.getAttribute('data-module')
		var right = e.currentTarget.getAttribute('data-right')
		var enabled = e.currentTarget.getAttribute('data-enabled') === 'true' ? false : true
		var permissionsChanged = Object.assign({},this.state.permissionsChanged)

		if(!permissionsChanged[module]){
			permissionsChanged[module] = {}
		}
		permissionsChanged[module][right] = enabled

		this.setState({permissionsChanged: permissionsChanged})
	}
	confirmEditRole = () => {
		this.setState({loading: true})
		this.props.ws.send(JSON.stringify({route: 'updateRolePermissions', editRole: this.state.editRole,  permissionsChanged:  this.state.permissionsChanged}))
	}

	editUsers = (e) => {
		this.setState({editingUsers: true, editRole: e.currentTarget.getAttribute('data-role') })
	}
	addUser = (e) => {
		var usersToAdd = this.state.usersToAdd.slice()
		var usersToRemove = this.state.usersToRemove.slice()
		usersToAdd.push(e.target.getAttribute('data-user'))
		if(usersToRemove.indexOf(e.target.getAttribute('data-user')) > -1 ) {
			usersToRemove.splice(usersToRemove.indexOf(e.target.getAttribute('data-user')), 1)
		}
		this.setState({usersToAdd: usersToAdd, usersToRemove: usersToRemove})
	}
	removeUser = (e) => {
		var usersToAdd = this.state.usersToAdd.slice()
		var usersToRemove = this.state.usersToRemove.slice()
		usersToRemove.push(e.target.getAttribute('data-user'))
		if(usersToAdd.indexOf(e.target.getAttribute('data-user')) > -1 ) {
			usersToAdd.splice(usersToAdd.indexOf(e.target.getAttribute('data-user')), 1)
		}
		this.setState({usersToAdd: usersToAdd, usersToRemove: usersToRemove})
	}
	confirmEditUsers = () => {
		this.setState({loading: true})
		this.props.ws.send(JSON.stringify({route: 'updateRoleUsers', editRole: this.state.editRole,  usersToAdd:  this.state.usersToAdd, usersToRemove:  this.state.usersToRemove}))
	}


	deleteRole = (e) => {
		this.setState({deletingRole: true, editRole: e.currentTarget.getAttribute('data-role')})
	}
	confirmDeleteRole = () => {
		this.setState({loading: true})
		this.props.ws.send(JSON.stringify({route: 'deleteRole', editRole: this.state.editRole}))
	}

	sort = (e) => {
		var sortOrder
		if(this.state.sortOrder === -1){
			sortOrder = 1;
		}else{
			sortOrder = -1;
		}
		this.setState({sortBy: e.target.getAttribute("data-sortBy"), sortOrder: sortOrder})
	}

	filter = (e) => {
		var filterBy = Object.assign({},this.state.filterBy)
		if(e.target.value.length > 0) {
			filterBy[e.target.getAttribute("data-filterBy")] = e.target.value
		} else {
			delete filterBy[e.target.getAttribute("data-filterBy")]
		}
		this.setState({filterBy: filterBy})
	}

	sortFunc(sortBy, sortOrder){
		return function(a,b) {
			if(sortBy === "status" || sortBy ==="ActivityDateTime") {
				if('status' in a && 'status' in b){
					if(!a.loggedIn && !b.loggedIn){
						return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
					}else if(!a.loggedIn) {
						return 1
					}else if(!b.loggedIn) {
						return -1
					}
					if(a[sortBy] === b[sortBy]){
						return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
					}
					return a[sortBy] > b[sortBy] ? 1 * sortOrder : -1 * sortOrder
				}else {
					if('status' in a){
						return -1
					}
					return 1
				}
			}

			if(sortBy in a && sortBy in b){
				if(a[sortBy] === b[sortBy]){
					return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
				}
				return a[sortBy] > b[sortBy] ? 1 * sortOrder : -1 * sortOrder
			}else if (!(sortBy in a) && !(sortBy in b)){
				return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
			}else {
				return sortBy in a ? -1 : 1
			}
		}
	}

	filterFunc(array, filterBy){
		return array.filter(function(object) {
			for(var prop in filterBy){
				if(typeof object[prop] === "undefined") {
					return false
				}
				if (object[prop].toString().toLowerCase().indexOf(filterBy[prop].toString().toLowerCase()) < 0) {
					return false
				}
			}
			return true
		})
	}
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.setState({editingRole: false, editingUsers: false, addingRole: false, deletingRole: false, editRole: '', loading: false, permissionsChanged:{},usersToAdd: [], usersToRemove:[]});
		}
	}
	render() {
		var rolePermissions, modules, module, rights, right, view, title, user
		if (this.props.active) {
			var roles = [];

			for (var role in this.props.data.Roles) {
				this.props.data.Roles[role].role = role
				roles.push(this.props.data.Roles[role]);
			}

			//Filtering Role List
			if(Object.keys(this.state.filterBy).length > 0) {
				roles = this.filterFunc(roles, this.state.filterBy)
			}

			//Sorting filtered Role List
			if(this.state.sortBy !== '')  {
				roles.sort(this.sortFunc(this.state.sortBy, this.state.sortOrder))
			}

			var roleList = []

			for (role of roles) {
				var roleCount = 0

				for (user in this.props.data.Users){
					if(user !== 'dirad'){
						for(var userRole in this.props.data.Users[user].roles){
							if(role.role === userRole){
								roleCount++
							}
						}
					}
				}

				roleList.push(
					<div className="dataRow" key={role.role}>
						<span style={{flex: '1 0 300px'}}>{role.name}</span>

						<span className="buttonBox" style={{flex: '0 0 280px'}}>
							{this.props.data.Roles ? <img src={roleIcon} alt="Edit Permissions"  data-role={role.role} data-roleName={role.name} onClick={this.editRole} /> : ''}
							<span  style={{marginLeft:'0px'}}>
								{this.props.data.Users ? <img src={userIcon} alt="Edit Users" data-role={role.role}  data-roleName={role.name} onClick={this.editUsers}/> : ''}
								<span style={{color:'#fff', lineHeight: '28px',marginLeft:'0px', verticalAlign: 'top'}}>{roleCount}</span>
							</span>
							<img src={del} alt="Delete Role" data-role={role.role} data-roleName={role.name}  onClick={this.deleteRole} />
						</span>
					</div>
				)
			}
			var permissions = ''
			
			if(this.state.editingRole){
				rolePermissions = this.props.data.Roles[this.state.editRole].security
				modules = []

				for(module in rolePermissions){
					rights = []
					for (right in rolePermissions[module]){
						if(this.state.permissionsChanged.hasOwnProperty(module) && this.state.permissionsChanged[module].hasOwnProperty(right)){
							rights.push(
								<div key={right}>
									<div style={{ fontSize: '16px'}}>{right}</div>
									<div className="activeBox" data-module={module} data-right={right} data-enabled={this.state.permissionsChanged[module][right]} onClick={this.togglePermission}>
										<div className={this.state.permissionsChanged[module][right] ? "enabled" : "disabled"}></div>
									</div>
								</div>
							)
						}else {
							rights.push(
								<div key={right}>
									<div style={{ fontSize: '16px'}}>{right}</div>
									<div className="activeBox" data-module={module} data-right={right} data-enabled={rolePermissions[module][right]} onClick={this.togglePermission}>
										<div className={rolePermissions[module][right] ? "enabled" : "disabled"}></div>
									</div>
								</div>
							)
						}
					}
					for(view of this.props.data.views){
						if(view.view === module){
							title = view.title
						}
					}

					modules.push(
						<div key={module} className="permissionRow" >

							<div style={{flex: '0 0 200px',textAlign:'left'}}>{title}</div>
							{rights}
						</div>
					)
				}

				permissions = (
					<div className="blackout">
						<div className="pop" style={{minWidth: '600px'}}>
							<div className="viewCloser" onClick={this.cancel}>X</div>

							<div className="error">{this.state.newUserError}</div>
							<div>Editing Permissions for "{this.props.data.Roles[this.state.editRole].name}" Role</div>
							<div>
								{modules}
							</div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditRole}>Confirm Permissions</div>}
						</div>
					</div>
				)
			}

			if(this.state.addingRole){
				rolePermissions = this.props.data.Roles.default.security
				modules = []

				for(module in rolePermissions){
					rights = []
					for (right in rolePermissions[module]){
						if(this.state.permissionsChanged.hasOwnProperty(module) && this.state.permissionsChanged[module].hasOwnProperty(right)){
							rights.push(
								<div key={right}>
									<div style={{ fontSize: '16px'}}>{right}</div>
									<div className="activeBox" data-module={module} data-right={right} data-enabled={this.state.permissionsChanged[module][right]} onClick={this.togglePermission}>
										<div className={this.state.permissionsChanged[module][right] ? "enabled" : "disabled"}></div>
									</div>
								</div>
							)
						}else {
							rights.push(
								<div key={right}>
									<div style={{ fontSize: '16px'}}>{right}</div>
									<div className="activeBox" data-module={module} data-right={right} data-enabled={rolePermissions[module][right]} onClick={this.togglePermission}>
										<div className={rolePermissions[module][right] ? "enabled" : "disabled"}></div>
									</div>
								</div>
							)
						}
					}
					for(view of this.props.data.views){
						if(view.view === module){
							title = view.title
						}
					}

					modules.push(
						<div key={module} className="permissionRow" >

							<div style={{flex: '0 0 200px',textAlign:'left'}}>{title}</div>
							{rights}
						</div>
					)
				}

				permissions = (
					<div className="blackout">
						<div className="pop" style={{minWidth: '600px'}}>
							<div className="viewCloser" onClick={this.cancel}>X</div>

							<SnazzyInput error={this.state.newUserIDError} value={this.state.editRole} onChange={this.addRoleNameChange} placeHolder={"Add New Role Name"}/>

							<div>
								{modules}
							</div>

							{this.state.roleNameError ? <div className="error">{this.state.roleNameError}</div>:''}
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmAddRole}>Create New Role</div>}
						</div>
					</div>
				)
			}

			var delRole = ""
			if (this.state.deletingRole) {
				delRole = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Removing Role</div>
							<div style={{margin: "15px"}}> {this.props.data.Roles[this.state.editRole].name} </div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmDeleteRole}>Delete This Role</div>}
						</div>
					</div>
				)
			}


			var editUsers = ""
			if (this.state.editingUsers) {
				var users = this.props.data.Users
				var userList = []
				var removeUserList = []
				var selUserList = []
				var addUserList = []

				for (var user in users ) {
					if(user === 'dirad'){
						//Skip it. Don't display the secret admin user
					} else if(this.props.data.Users[user].roles[this.state.editRole] === true && this.state.usersToRemove.indexOf(user) < 0) {
						selUserList.push(
							<div key={user} data-user={user} className="dataRow" onClick={this.removeUser}>{users[user].wholeName}</div>
						)
					}else if (this.state.usersToAdd.indexOf(user) > -1){
						addUserList.push(
							<div key={user} data-user={user} style={{fontStyle: 'italic'}} className="dataRow" onClick={this.removeUser}>{users[user].wholeName}</div>
						)
					}else if (this.state.usersToRemove.indexOf(user) > -1){
						removeUserList.push(
							<div key={user} data-user={user} style={{fontStyle: 'italic'}} className="dataRow" onClick={this.addUser}>{users[user].wholeName}</div>
						)
					}else {
						userList.push(
							<div key={user} data-user={user} className="dataRow" onClick={this.addUser}>{users[user].wholeName}</div>
						)
					}
				}
				editUsers = (
					<div className="blackout">
						<div className="pop" style={{width: "80vw", maxWidth: "1000px"}}>
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div style={{marginBottom: "25px"}}>Add or Remove "{this.props.data.Roles[this.state.editRole].name}" Role From Users</div>
							<div style={{display: "inline-block"}}>
								<div className="loginLabel" >Users Without This Role</div>
								<div className="roleBox">
									{removeUserList}
									{userList}
								</div>
							</div>
							<div style={{display: "inline-block"}}>
								<div className="loginLabel" >Users Assigned This Role</div>
								<div className="roleBox">
									{addUserList}
									{selUserList}
								</div>
							</div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditUsers}>Confirm User Role Assignments</div>}
						</div>
					</div>
				)
			}


			return (
				<div className="viewContent">
					<img src={plus}  className="plusButton" alt="Add a Role" onClick={this.addRole} />

					<div className="dataHeaders">
						<span style={{flex: '1 0 300px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="name">Role Name</span>
						<span style={{flex: '0 0 280px'}} ></span>
					</div>
					<div className="dataFilters">
						<input style={{flex: '1 0 300px'}} type="text" onChange={this.filter} value={this.state.filterBy.name} ref="username" data-filterBy="name"/>
						<span style={{flex: '0 0 280px'}} ></span>
					</div>

					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{roleList}
					</div>
					{permissions}
					{delRole}
					{editUsers}
				</div>
			);
		} else {
			return (<div></div>);
		}
	}
}

export default Roles;
