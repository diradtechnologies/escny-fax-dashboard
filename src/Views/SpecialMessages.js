import React  from 'react';
import Flatpickr from 'react-flatpickr'
var ReactSuperSelect = require('react-super-select')

class SpecialMessages extends React.PureComponent  {
	constructor(props) {
		super(props);
		this.state = {
            uploading: false,
			editingMessage: false,
			loading: false,
            file: false

		};
	}
    selectFile = (prompt) => (e) => {
        e.stopPropagation();
        this.setState({editPrompt: prompt, uploading: true, uploadLanguage: 'English'})
    }
    setFileName = (e) => {
        if(e.target.files.length > 0){
            this.setState({file: e.target.files[0]})
        } else {
            this.setState({file: false})
        }
        this.fileReader = new FileReader()
        this.fileReader.onload = (event) => {
            //new TextDecoder("utf-8").decode(uint8array)
            //var dec = new TextDecoder("utf-8")
            this.setState({binaryData: this.fileReader.result.split(';base64,').pop()})
            //this.setState({binaryData: String.fromCharCode.apply(null, new Uint16Array(this.fileReader.result))})
        }
        this.fileReader.readAsDataURL(e.target.files[0])

    }
    toggleLanguage = (option) =>  {
		if(option){
			this.setState({uploadLanguage: option.name});
		}
	}
    openPlayer = (e) => {
        this.setState({playing: true})
    }
    closePlayer = (e) => {
        this.setState({playing: false})
    }
    confirmUploadPrompt = () => {
        this.setState({loading: true});
        this.props.ws.send(JSON.stringify({route: 'uploadSpecialMessage', prompt: this.state.editPrompt, language: this.state.uploadLanguage, file: this.state.binaryData }))
    }
	editMessage = (message) => e => {
        var newState = {editingMessage: true, loading: false, editPrompt: message.Prompt, description: message.Description, error: '', start: message.Active_StartDateTime, end: message.Active_EndDateTime, active: message.is_active}
        if(message.hasOwnProperty('Barge_Allowed')){
            newState.Barge_Allowed = message.Barge_Allowed
        }
        if(message.hasOwnProperty('Action')){
            newState.Action = message.Action
        }
		this.setState(newState)
	}
	editDescription = (e) =>  {
		if (e.target.value.length < 1){
			this.setState({description: '', error: 'Please enter a description for this prompt.'});
		} else {
			this.setState({description: e.target.value, error: ''});
		}
	}
	cancelEditMessage = () => {
		this.setState({editingMessage: false, uploading:false, loading: false, editPrompt: false, start:false, end: false,active: null})
	}
	confirmEditMessage = () => {
		if (this.state.description.length < 1){
			this.setState({error: 'Please enter a description for this prompt.'});
		} else if (new Date(this.state.start) > new Date( this.state.end)) {
			this.setState({error: 'End date must not be before start date.'});
		}else {
			this.setState({ loading: true });
            var send = {route: 'editSpecialMessage', editPrompt: this.state.editPrompt,  description: this.state.description, start: this.state.start, end: this.state.end, active:this.state.active  }
            if(this.state.hasOwnProperty('Barge_Allowed')){
                send.Barge_Allowed = this.state.Barge_Allowed
            }
            if(this.state.hasOwnProperty('Action')){
                send.Action = this.state.Action
            }
			this.props.ws.send(JSON.stringify(send))
		}
	}

	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmEditMessage();
		}
	}

	toggle = (param) => (e) => {
		var newState = Object.assign({}, this.state)
		newState[param] = this.state[param] ? false : true
		this.setState(newState)
	}
    toggleAction = (option) =>  {
		if(option){
			this.setState({Action: option.name, error: ''});
		}
	}

	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.setState({ uploading: false, editingMessage: false, loading: false });
		}
	}
	render() {
		if (this.props.active) {
			var smList = []
			for (var message of this.props.data.SpecialMessages) {
				smList.push(
					<div className="dataRow" key={message.Id} onClick={this.editMessage(message)}>
						<div className="activeBox">
							<div className={message.is_active ? "enabled" : "disabled"}></div>
						</div>
						<span style={{flex: '0 0 75px'}}>{message.Prompt}</span>
						<span style={{flex: '1 0 20%'}}>{message.Description}</span>
						<span style={{flex: '1 0 30%'}}>{new Date(message.Active_StartDateTime).toLocaleString() +' - '+ new Date(message.Active_EndDateTime).toLocaleString()}</span>
                        <span className="buttonBox" style={{flex: '0 0 80px'}}>
                            <img src={this.props.img.upload} alt="Upload .wav" onClick={this.selectFile(message.Prompt)}/>
                            {false ? <img style={{height: '40px', position: 'relative', top: '-6px'}} src={this.props.img.speaker} alt="Play Message" /> : ''}

						</span>
					</div>
				)
			}

			var editMessage = ""
			if (this.state.editingMessage) {
				editMessage = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelEditMessage}>X</div>
							<div>You Are Editing Special Message {this.state.editPrompt}</div>
							<br/>
							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.description} onChange={this.editDescription} ></input>
								<span className="inputBar"></span><label className="textLabel">Prompt Description</label>
							</div>

							<div style={{display: 'inline-block'}}>
								<div>Start</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>
							<div style={{display: 'inline-block'}}>
								<div>End</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) :this.setState({end: undefined}) }} />
							</div>

							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Active</div>
									<div className="activeBox" onClick={this.toggle('active')}>
										<div className={this.state.active ? "enabled" : "disabled"}></div>
									</div>
								</div>
                                {this.state.hasOwnProperty('Barge_Allowed') ? <div >
									<div>Allow Barge</div>
									<div className="activeBox" onClick={this.toggle('Barge_Allowed')}>
										<div className={this.state.Barge_Allowed ? "enabled" : "disabled"}></div>
									</div>
								</div> : ''}
                            </div>

                            {this.state.hasOwnProperty('Action') ?
                            <div>
                                <div style={{margin: '10px'}}>Action After Playing</div>
                                <div style={{display: 'flex'}}>
            						<ReactSuperSelect style={{flex: '1 0 150px', margin: '10px', display: 'inline-block'}}  onChange={this.toggleAction} initialValue={{id: this.state.Action, name:  this.state.Action}} placeholder={"Action"} clearable={false} closeOnSelectedOptionClick={false} dataSource={[{id: 'Continue', name: 'Continue'},{id: 'Disconnect', name: 'Disconnect'},{id: 'Transfer', name: 'Transfer'}]} />
            					</div> </div> : ''}

							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>

							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditMessage}>Confirm Special Message</div>}
						</div>
					</div>
				)
			}

            var upload = ""
			if (this.state.uploading) {
				upload = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelEditMessage}>X</div>
							<div>You Are Uploading a .wav File for {this.state.editPrompt}</div>
							<br/><br/>
                            <input style={{width: '0.1px', height: '0.1px', opacity: '0', overflow: 'hidden', position: 'absolute', zIndex: '-1'}} type="file" id="wav" onChange = {this.setFileName}/>

                            {this.state.file ?
                                <div >
                                    <label style={{display: 'inline-block', border: '1px solid #379', borderTopRightRadius: '0px', borderBottomRightRadius: '0px' }} className="button" htmlFor="wav">Browse
                                    </label>

                                    <span style={{display: 'inline-block', border: '1px solid #379', padding: '15px 29px',   fontSize: '25px' }}>{this.state.file.name.split('\\')[ this.state.file.name.split('\\').length -1]}
                                    </span>

                                    <div style={{position: 'relative', display: 'inline-block', border: '1px solid #379', borderTopLeftRadius: '0px', borderBottomLeftRadius: '0px', borderTopRightRadius: '5px', borderBottomRightRadius: '5px' }} className="button"  onClick={this.openPlayer}>-
                                        <div style={{left: '25px', top: '15px', borderColor: 'rgba(0,0,0,0) rgba(0,0,0,0) rgba(0,0,0,0) #fff', borderWidth: '15px 25px', borderStyle: 'solid',     position: 'absolute'}}></div>
                                    </div>

                                </div>
                                : <label className="button" htmlFor="wav">Browse</label>
                             }

                            <br/>
                            <br/>

                            <ReactSuperSelect style={{flex: '1 0 150px', marginTop: '50px', margin: '10px', display: 'inline-block'}}  onChange={this.toggleLanguage} initialValue={{id: 'English', name:  'English'}} placeholder={"Action"} clearable={false} closeOnSelectedOptionClick={false} dataSource={[{id: 'English', name: 'English'},{id: 'Spanish', name: 'Spanish'}]} />

                            <div className="error" style={{padding: '15px'}}>{this.state.error}</div>

							{this.state.binaryData ? this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmUploadPrompt}>Confirm Upload</div> : ''}
						</div>
					</div>
				)
			}

            var audioPlayer = ""
            if (this.state.playing) {
                audioPlayer = (
                    <div className="blackout" onClick={this.closePlayer}>
                        <div className="pop">
                            <div className="viewCloser" style={{display: 'block'}} onClick={this.closePlayer}>X</div>
                            <div className="loginLabel">Play Special Message</div>
                            <div className="error">{this.state.recordingError}</div>
                            <audio id="recordingPlayer" style={{width: "500px", height: "50px", margin: "25px"}} src={URL.createObjectURL(this.state.file)} controls controlsList="nofullscreen nodownload noremoteplayback" />
                            <div  onClick={this.closePlayer} className="button">Close</div>
                        </div>
                    </div>
                )
            }

			return (
				<div width="100%">
					<div className="dataHeaders">
						<span style={{flex: '0 0 75px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="enabled">Active?</span>
						<span style={{flex: '0 0 75px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="name">Prompt</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Description</span>
						<span style={{flex: '1 0 30%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Date/Time Range</span>
                        <span style={{flex: '0 0 80px', cursor: 'pointer'}} ></span>
					</div>
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{smList}
					</div>

					{editMessage}
                    {upload}
                    {audioPlayer}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default SpecialMessages
