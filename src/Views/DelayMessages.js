import React  from 'react';
import Flatpickr from 'react-flatpickr'
import plus from '../img/plus.svg';
import del from '../img/delete.svg';
 
class DelayMessages extends React.PureComponent  {
	constructor(props) {
		super(props);
		this.state = { 	
			editingMessage: false,
			loading: false
		};
	}
	
	editMessage = (message) => e => {
		this.setState({
			editingMessage: true, 
			loading: false, 
			StopID: message.StopID, 
			RouteID: message.RouteID, 
			error: '', 
			start: message.StartDateTime, 
			end: message.EndDateTime, 
			verbiage: message.DelayMessageVerbiage, 
			nextThree: message.PlayNextThree, 
			id: message.ID
		})
	}
	editParam = (param) => (e) =>  {
		var newState = Object.assign({}, this.state)
		newState[param] = e.target.value
		this.setState(newState);
	}
	toggle = (param) => (e) => {
		var newState = Object.assign({}, this.state)
		newState[param] = this.state[param] ? false : true
		this.setState(newState)
	}
	cancel = () => {
		this.setState({editingMessage: false, addingMessage: false,  deletingMessage: false,  loading: false, StopID: false, RouteID: false, error: '', start: false, end: false, verbiage: false,  nextThree: false, id: false})
	}
	confirmEditMessage = () => {
		if (this.state.verbiage.length < 1){
			this.setState({error: 'Please enter TTS verbiage for this message.'});
		} else if (this.state.verbiage.length > 500){
			this.setState({error: 'TTS verbiage must be 500 characters or fewer.'});
		} else if (!this.state.StopID && !this.state.RouteID) {
			this.setState({error: 'You must enter either a route or stop ID, or both.'});
		} else if (!this.state.start || !this.state.end) {
			this.setState({error: 'You must indicate a start and end date.'});
		}  else if (new Date(this.state.start) > new Date( this.state.end)) {
			this.setState({error: 'End date must not be before start date.'});
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editDelayMessage', StopID: this.state.StopID, RouteID: this.state.RouteID, start: this.state.start, end: this.state.end, verbiage: this.state.verbiage, nextThree: this.state.nextThree, id: this.state.id}))		
		}	
	}
	
	addMessage = (e) => {
		this.setState({ addingMessage: true, loading: false, StopID: '', RouteID: '', error: '', start: false, end: false, verbiage: '', nextThree: false, })
	}

	confirmAddMessage = () => {
		if (this.state.verbiage.length < 1){
			this.setState({error: 'Please enter TTS verbiage for this message.'});
		} else if (this.state.verbiage.length > 500){
			this.setState({error: 'TTS verbiage must be 500 characters or fewer.'});
		} else if (!this.state.StopID && !this.state.RouteID) {
			this.setState({error: 'You must enter either a route or stop ID, or both.'});
		} else if (new Date(this.state.start) > new Date( this.state.end)) {
			this.setState({error: 'End date must not be before start date.'});
		} else if (!this.state.start || !this.state.end) {
			this.setState({error: 'You must indicate a start and end date.'});
		}  else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'addDelayMessage', StopID: this.state.StopID, RouteID: this.state.RouteID, start: this.state.start, end: this.state.end, verbiage: this.state.verbiage, nextThree: this.state.nextThree}))		
		}	
	}
	
	deleteMessage = (message) => e => {
		e.stopPropagation()
		this.setState({deletingMessage: true, id: message.ID, loading: false, StopID: message.StopID, RouteID: message.RouteID, error: ''})
	}
	confirmDeleteMessage = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'deleteDelayMessage', id: this.state.id }))		
	}
	checkEnter = (e) => {
		if(e.keyCode === 13){
			if(this.state.editingMessage){
				this.confirmEditMessage();
			} else if(this.state.addingMessage){
				this.confirmAddMessage();
			} else if(this.state.deletingMessage){
				this.confirmDeleteMessage();
			} 
		}
	}
	

	
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.setState({ editingMessage: false, addingMessage: false, deletingMessage: false, loading: false });
		}
	}
	
	render() {
		if (this.props.active) {	
			var dmList = []
			for (var message of this.props.data.DelayMessages) {
				dmList.push(
					<div className="dataRow" key={message.ID} onClick={this.editMessage(message)}>
						<div className="activeBox">
							<div className={message.PlayNextThree ? "enabled" : "disabled"}></div>
						</div>
						<span style={{flex: '0 0 60px', textAlign: 'center'}}>{message.StopID ? message.StopID : '--'}</span>
						<span style={{flex: '0 0 200px' , textAlign: 'center'}}>{message.stop_name  ? message.stop_name : '--'}</span>
						<span style={{flex: '0 0 45px' , textAlign: 'center'}}>{message.RouteID  ? message.RouteID : '--'}</span>
						<span style={{flex: '1 0 100px' , textAlign: 'center'}}>{message.route_long_name  ? message.route_long_name : '--'}</span>
						<span style={{flex: '0 0 450px'}}>{new Date(message.StartDateTime).toLocaleString() +' - '+ new Date(message.EndDateTime).toLocaleString()}</span>
						<span className="buttonBox" style={{flex: '0 0 28px',  margin: '6px 0px'}}>
							<img  style={{marginLeft: '0px'}} src={del} alt="Delete Holiday" onClick={this.deleteMessage(message)} />
						</span>
					</div>
				)
			}
			
			var editMessage = ""
			if (this.state.editingMessage) {
				editMessage = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop">
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Editing a Delay Message</div>
							<br/>
						<div style={{display: 'flex'}}>	
							<div className="snazzyInputBox">
								<input style={{textAlign: 'center'}} maxLength="4" className="textInput" type="text" required value={this.state.StopID} onChange={this.editParam('StopID')} ></input>
								<span className="inputBar"></span><label className="textLabel">Stop ID</label>
							</div>
							
							<div className="snazzyInputBox">
								<input style={{textAlign: 'center'}}  className="textInput" type="text" maxLength="3" required value={this.state.RouteID} onChange={this.editParam('RouteID')} ></input>
								<span className="inputBar"></span><label className="textLabel">Route ID</label>
							</div>
						</div>	
							<div className="snazzyInputBox">
								<textarea className="bigTextInput" required value={this.state.verbiage} onChange={this.editParam('verbiage')} ></textarea>
								<label className="textLabel">Message Verbiage</label>
								<div style={{color: this.state.verbiage.length > 500 ? '#937' : '',  textAlign: 'right',  paddingRight: '15px',  top: '-15px', fontSize: '14px', position: 'relative'}}>{this.state.verbiage.length}/500</div>
							</div>
							
							<div style={{display: 'inline-block'}}>
								<div>Start</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>
							<div style={{display: 'inline-block'}}>
								<div>End</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) :this.setState({end: undefined}) }} />
							</div>
							
							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Play Next Three Times</div>
									<div className="activeBox" onClick={this.toggle('nextThree')}>
										<div className={this.state.nextThree ? "enabled" : "disabled"}></div>
									</div>
								</div>
							</div>
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditMessage}>Confirm Delay Message</div>}
						</div>
					</div>
				)
			}
			var addMessage = ""
			if (this.state.addingMessage) {
				addMessage = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop">
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Adding a New Delay Message</div>
							<br/>
						<div style={{display: 'flex'}}>	
							<div className="snazzyInputBox">
								<input style={{textAlign: 'center'}} maxLength="4" className="textInput" type="text" required value={this.state.StopID} onChange={this.editParam('StopID')} ></input>
								<span className="inputBar"></span><label className="textLabel">Stop ID</label>
							</div>
							
							<div className="snazzyInputBox">
								<input style={{textAlign: 'center'}}  className="textInput" type="text" maxLength="4" required value={this.state.RouteID} onChange={this.editParam('RouteID')} ></input>
								<span className="inputBar"></span><label className="textLabel">Route ID</label>
							</div>
						</div>	
							<div className="snazzyInputBox">
								<textarea className="bigTextInput" required value={this.state.verbiage} onChange={this.editParam('verbiage')} ></textarea>
								<label className="textLabel">Message Verbiage</label>
								<div style={{color: this.state.verbiage.length > 500 ? '#937' : '',  textAlign: 'right',  paddingRight: '15px',  top: '-15px', fontSize: '14px', position: 'relative'}}>{this.state.verbiage.length}/500</div>
							</div>
							
							<div style={{display: 'inline-block'}}>
								<div>Start</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>
							<div style={{display: 'inline-block'}}>
								<div>End</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) :this.setState({end: undefined}) }} />
							</div>
							
							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Play Next Three Times</div>
									<div className="activeBox" onClick={this.toggle('nextThree')}>
										<div className={this.state.nextThree ? "enabled" : "disabled"}></div>
									</div>
								</div>
							</div>
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmAddMessage}>Add New Delay Message</div>}
						</div>
					</div>
				)
			}
			var deleteMessage = ""
			if (this.state.deletingMessage) {
				deleteMessage = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Removing Delay Message for</div>
							{this.state.StopID ? <div style={{margin: "15px"}}>Stop ID: {this.state.StopID} </div>	: '' }		
							{this.state.RouteID ?  <div style={{margin: "15px"}}>Route ID: {this.state.RouteID} </div>	: '' }									
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmDeleteMessage}>Delete This Message</div>}
						</div>
					</div>
				)
			}
			return (
				<div width="100%">
					<img src={plus}  className="plusButton" alt="Add a Holiday" onClick={this.addMessage} />
					<div className="dataHeaders">
						<span style={{flex: '0 0 75px', cursor: 'pointer'}} >Next3?</span>
						<span style={{flex: '0 0 60px', cursor: 'pointer'}} >StopID</span>
						<span style={{flex: '0 0 200px', cursor: 'pointer'}} >Stop Name</span>
						<span style={{flex: '0 0 45px', cursor: 'pointer'}} >RouteID</span>
						<span style={{flex: '1 0 100px', cursor: 'pointer'}} >Route Name</span>
						<span style={{flex: '0 0 450px', cursor: 'pointer'}} >Date/Time Range</span>
					</div> 
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{dmList}
					</div>
					
					{editMessage}
					{addMessage}
					{deleteMessage}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default DelayMessages