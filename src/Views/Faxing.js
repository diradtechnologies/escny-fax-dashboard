import React from 'react';
import { Chart } from 'react-google-charts';
var fax;

export default class Faxing extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
            sending: false,
            setting: false,
            viewing: false,
            loading: false,
            flipping: false,
            send: {
                toName:'',
                toNumber:'',
                toCompany:'',
                fromName: '',
                message:'',
                file: false
            },
            unreadSortBy:  'dateCreated',
            unreadSortDirection: -1,
            readSortBy:  'dateCreated',
            readSortDirection: -1,
            readFilterBy: {},
            unreadFilterBy: {}

        };
        this.urlParams = new URLSearchParams(window.location.search);
	}

    cancel = () => {
        this.setState({viewing: false, sending: false, setting: false, deleting: false, loading: false})
    }

    sending = () => {
        this.setState({sending: true, sendError: false, file: false,
            send: {
                toName:'',
                toNumber:'',
                toCompany:'',
                fromName: this.props.data.Faxing.settings.defaultFrom,
                message:'',
                file: false
            }
        })
    }

    editSendField = (field) => (e) => {
        var send = Object.assign({}, this.state.send)
        send[field] = e.currentTarget.value
        this.setState({send: send, sendError: false})
    }

    confirmSend = () => {
        if(this.state.send.toNumber.length < 10){
            this.setState({sendError: "Please enter a 10 digit fax number in the To Number field."})
        } else if (!this.state.send.file || this.state.file.name.slice(-3) != 'pdf'){
            this.setState({sendError: "Please upload a pdf file for faxing."})
        } else {
            this.setState({ loading: true });
            this.props.ws.send(JSON.stringify({route: 'sendFax', fax: this.state.send}))
        }
    }

    setting = () => {
        this.setState({setting: true});
    }

    viewing = (fax) => (e) =>{
        this.setState({viewing: fax})
    }

    view = (fax) => {
        this.setState({viewing: fax})
    }

    deleteFax = (fax) => (e) => {
        e.stopPropagation()
        this.setState({deleting: fax})
    }
    confirmDeleteFax = (e) => {
        this.setState({loading: true})
        this.props.ws.send(JSON.stringify({route: 'deleteFax', sid: this.state.deleting.sid}))
    }
    markFaxAsRead = (fax) => (e) => {
        e.stopPropagation()
        this.props.ws.send(JSON.stringify({route: 'markFaxAsRead', sid: fax.sid}))
    }
    markFaxAsUnread = (fax) => (e) => {
        e.stopPropagation()
        this.props.ws.send(JSON.stringify({route: 'markFaxAsUnread', sid: fax.sid}))
    }
    toggleFlipped = (fax) => (e) => {
        this.setState({flipping: true})
        e.stopPropagation()
        if(fax.flipped){
            this.props.ws.send(JSON.stringify({route: 'markFaxAsUnflipped', sid: fax.sid}))
        } else {
            this.props.ws.send(JSON.stringify({route: 'markFaxAsFlipped', sid: fax.sid}))
        }
    }

    readSort = (field) => (e) => {
        var dir = 1
        if(field == this.state.readSortBy){
            dir = this.state.readSortDirection * -1
        }
        this.setState({readSortBy: field, readSortDirection: dir})
    }

    unreadSort = (field) => (e) => {
        var dir = 1
        if(field == this.state.unreadSortBy){
            dir = this.state.unreadSortDirection * -1
        }
        this.setState({unreadSortBy: field, unreadSortDirection: dir})
    }

    readFilter = (field) => (e) => {
        var readFilterBy = Object.assign({},this.state.readFilterBy)
        if(e.target.value.length > 0) {
            readFilterBy[field] = e.target.value
        } else {
            delete readFilterBy[field]
        }
        this.setState({readFilterBy: readFilterBy})
    }

    unreadFilter = (field) => (e) => {
        var unreadFilterBy = Object.assign({},this.state.unreadFilterBy)
        if(e.target.value.length > 0) {
            unreadFilterBy[field] = e.target.value
        } else {
            delete unreadFilterBy[field]
        }
        this.setState({unreadFilterBy: unreadFilterBy})
    }

    filterFunc(array, filterBy){
		return array.filter(function(object) {
			for(var prop in filterBy){
				if(typeof object[prop] === "undefined") {
					return false
				}
                if(prop == 'dateCreated'){
                    if (new Date(object.dateCreated).toLocaleString().toLowerCase().indexOf(filterBy[prop].toString().toLowerCase()) < 0) {
    					return false
    				}
                }else {
                    if (object[prop].toString().toLowerCase().indexOf(filterBy[prop].toString().toLowerCase()) < 0) {
    					return false
    				}
                }
			}
			return true
		})
	}

    setFileName = (e) => {
        if(e.target.files.length > 0){
            this.setState({file: e.target.files[0]})
        } else {
            this.setState({file: false})
        }
        this.fileReader = new FileReader()
        this.fileReader.onload = (event) => {
            var send = Object.assign({}, this.state.send)
            send.file = this.fileReader.result.split(';base64,').pop()
            this.setState({send: send, sendError: false})
        }
        this.fileReader.readAsDataURL(e.target.files[0])
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.loading) {
			this.cancel();
		}
        if(this.state.flipping){
            var newViewing = Object.assign({}, this.state.viewing)
            if(newViewing.flipped){
                newViewing.flipped = false
            }else {
                newViewing.flipped = true
            }
            this.setState({viewing: newViewing, flipping: false})
        }
        console.log(this.urlParams.get('fx'))
        if(this.urlParams.get('fx')){
            for(fax of this.props.data.Faxing.faxes){
                console.log(fax.sid)
                if(fax.sid == this.urlParams.get('fx')){
                    this.view(fax)
                }
            }
        }
	}

    convertSec(sec) {
		var d, h, m, s;
		s = Math.floor(sec);
		m = Math.floor(s / 60);
		s = s % 60;
		h = Math.floor(m / 60);
		m = m % 60;
		d = Math.floor(h / 24);
		h = h % 24;

		var statusTime = ''

		d > 0 ? statusTime += d + ' days, ' : statusTime += ''
		h < 1 ? statusTime += '': h > 0 && h < 10 ? statusTime +=  '0' + h + ':' : statusTime += h + ':'
		m < 1  ? statusTime += '00:' : m < 10 ? statusTime +=  '0' + m + ':' : statusTime += m + ':'
		s < 1  ? statusTime += '00' : s < 10 ? statusTime +=  '0' + s : statusTime += s

		return statusTime;
	};

	render() {
		if (this.props.active) {

            console.log()
            var sendPane = ""
            if (this.state.sending) {
                sendPane = (
                    <div className="blackout">
                        <div className="pop">
                            <div className="viewCloser" onClick={this.cancel}>X</div>

                            <div className="snazzyInputBox">
								<input className="textInput" type="text" onChange={this.editSendField('toNumber')} value={this.state.send.toNumber} required></input>
								<span className="inputBar"></span><label className="textLabel">To Number</label>
                            </div>

                            <div className="snazzyInputBox">
								<input className="textInput" type="text" onChange={this.editSendField('toCompany')} value={this.state.send.toCompany} required></input>
								<span className="inputBar"></span><label className="textLabel">To Company</label>
                            </div>

                            <div className="snazzyInputBox">
								<input className="textInput" type="text" onChange={this.editSendField('toName')} value={this.state.send.toName} required></input>
								<span className="inputBar"></span><label className="textLabel">To Name</label>
                            </div>

                            <div className="snazzyInputBox">
								<input className="textInput" type="text" onChange={this.editSendField('fromName')} value={this.state.send.fromName} required></input>
								<span className="inputBar"></span><label className="textLabel">From Name</label>
                            </div>

                            <div className="snazzyInputBox">
								<textarea className="bigTextInput" required onChange={this.editSendField('message')} value={this.state.send.message}></textarea>
								<label className="textLabel">Message</label>
								<div style={{color: this.state.send.message.length > 500 ? '#937' : '',  textAlign: 'right',  paddingRight: '15px',  top: '-15px', fontSize: '14px', position: 'relative'}}>{this.state.send.message.length}/500</div>
							</div>

                            <input style={{width: '0.1px', height: '0.1px', opacity: '0', overflow: 'hidden', position: 'absolute', zIndex: '-1'}} type="file" id="wav" onChange = {this.setFileName}/>

                            {this.state.file ?
                                <div >
                                    <label style={{display: 'inline-block', border: '1px solid #379', borderTopRightRadius: '0px', borderBottomRightRadius: '0px' }} className="button" htmlFor="wav">Browse
                                    </label>

                                    <span style={{display: 'inline-block', border: '1px solid #379', padding: '15px 29px',   fontSize: '25px', borderTopLeftRadius: '0px', borderBottomLeftRadius: '0px', borderTopRightRadius: '5px', borderBottomRightRadius: '5px' }}>{this.state.file.name.split('\\')[ this.state.file.name.split('\\').length -1]}
                                    </span>
                                </div>

                                :   <div style={{ margin: '25px'}}>
                                        <label className="button" htmlFor="wav">Browse</label>
                                    </div>
                             }

                            <div className="error">{this.state.sendError}</div>

							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmSend}>Send Fax</div>}
                        </div>
                    </div>
                )
            }

            var deletePane = ""
			if (this.state.deleting) {
				deletePane = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>Permanently deleting {this.state.deleting.direction} fax</div>
                            <div style={{margin: "15px"}}> Date: {new Date(this.state.deleting.dateCreated).toLocaleString()} </div>
							<div style={{margin: "15px"}}> From: {this.state.deleting.from} </div>
                            <div style={{margin: "15px"}}> To: {this.state.deleting.to} </div>

							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmDeleteFax}>Delete Fax</div>}
						</div>
					</div>
				)
			}

            var settings = ""
            if (this.state.setting) {
                settings = (
                    <div className="blackout">
                        <div className="pop">
                            <div className="viewCloser" onClick={this.cancel}>X</div>
                            <div>Notify on Received Fax</div>
                            <div>Notify on Sent Fax</div>
                            <div>Default From Name</div>

                            {this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmResetUser}>Save Settings</div>}
                        </div>
                    </div>
                )
            }

            var detail = ""
            if (this.state.viewing) {
                fax = this.state.viewing
                detail = (
                    <div className="blackout">
                        <div className="pop">
                            <div className="viewCloser" onClick={this.cancel}>X</div>

                            <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                <div>Date/Time:</div>
                                <div>{new Date(fax.dateCreated).toLocaleString()}</div>
                            </div>
                            {fax.user ?
                                <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                    <div>User:</div>
                                    <div>{fax.user + ' (' + fax.userID +')'}</div>
                                </div>
                            : ''}
                            <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                <div>Direction:</div>
                                <div>{fax.direction}</div>
                            </div>

                            <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                <div>From:</div>
                                <div>{fax.from}</div>
                            </div>

                            <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                <div>To:</div>
                                <div>{fax.to}</div>
                            </div>

                            <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                <div>Status:</div>
                                <div>{fax.status}</div>
                            </div>

                            {fax.hasOwnProperty('read') ?
                                <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                    <div>Read:</div>
                                    <div>{fax.read ? 'Read' : 'Unread'}</div>
                                </div>
                            :''}

                            {fax.hasOwnProperty('numPages') ?
                                <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                    <div>Number of Pages:</div>
                                    <div>{fax.numPages}</div>
                                </div>
                            :''}

                            {fax.hasOwnProperty('quality') ?
                                <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                    <div>Quality:</div>
                                    <div>{fax.quality}</div>
                                </div>
                            :''}

                            {fax.duration > 0 ?
                                <div style={{display:'flex', justifyContent: 'space-between', margin: '10px'}}>
                                    <div>Duration:</div>
                                    <div>{this.convertSec(fax.duration)}</div>
                                </div>
                            :''}
                            {this.state.flipping ? <div className="loader"></div> : <div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Flipped?</div>
									<div className="activeBox" onClick={this.toggleFlipped(fax)}>
										<div className={fax.flipped ? "enabled" : "disabled"}></div>
									</div>
								</div>
							</div>}


                            <a href={window.location.origin.replace(':3000', ':8765') + "/fax/download/"+fax.sid}><div className="button"  >Download Fax</div></a>
                        </div>
                    </div>
                )
            }

            var unreadFaxList = []
            var unreadFaxCounter = 0
            var readFaxList = []
            var readFaxCounter = 0
            for(var sid in this.props.data.Faxing.faxes){
                if(this.props.data.Faxing.faxes[sid].read){
                    readFaxList.push(this.props.data.Faxing.faxes[sid])
                    readFaxCounter++
                }else {
                    unreadFaxList.push(this.props.data.Faxing.faxes[sid])
                    unreadFaxCounter++
                }
            }

            if(Object.keys(this.state.unreadFilterBy).length > 0) {
				unreadFaxList = this.filterFunc(unreadFaxList, this.state.unreadFilterBy)
			}

            if(Object.keys(this.state.readFilterBy).length > 0) {
				readFaxList = this.filterFunc(readFaxList, this.state.readFilterBy)
			}

            unreadFaxList.sort((a,b)=>{
                if(this.state.unreadSortBy == "dateCreated"){
                    return ((new Date(a.dateCreated).getTime() - new Date(b.dateCreated).getTime()) * this.state.unreadSortDirection)
                } else {
                    if(a[this.state.unreadSortBy] > b[this.state.unreadSortBy]){
                        return this.state.unreadSortDirection
                    } else {
                        return this.state.unreadSortDirection * -1
                    }
                }
            })

            readFaxList.sort((a,b)=>{
                if(this.state.readSortBy == "dateCreated"){
                    return ((new Date(a.dateCreated).getTime() - new Date(b.dateCreated).getTime()) * this.state.readSortDirection)
                } else {
                    if(a[this.state.readSortBy] > b[this.state.readSortBy]){
                        return this.state.readSortDirection
                    } else {
                        return this.state.readSortDirection * -1
                    }
                }
            })

            var unreadFaxes = []
            var readFaxes = []

            for(var fax of readFaxList){
                readFaxes.push(
                    <div className="dataRow" key={fax.sid} onClick={this.viewing(fax)}>
                        <div className="activeBox" title="Mark as Unread" data-active={fax.read} onClick={this.markFaxAsUnread(fax)}>
                            <div className={fax.read ? "enabled" : "disabled"}></div>
                        </div>
                        <div style={{textAlign: 'left', flex: '3 0 200px' }}>{new Date(fax.dateCreated).toLocaleString()}</div>
                        <div style={{textAlign: 'left', flex: '1 0 85px' }} >{fax.direction}</div>
                        <div style={{textAlign: 'left', flex: '2 0 135px' }} >{fax.from}</div>
                        <div style={{textAlign: 'left', flex: '2 0 135px' }} >{fax.to}</div>
                        <div style={{textAlign: 'left', flex: '1 0 100px' }}>{fax.status}</div>
                        <span className="buttonBox" style={{flex: '0 0 28px'}}>
							<img src={this.props.img.delete} title="Delete Fax" alt="Delete Fax" onClick={this.deleteFax(fax)} />
						</span>
                    </div>
                )
            }

            for(var fax of unreadFaxList){
                unreadFaxes.push(
                    <div className="dataRow" key={fax.sid} onClick={this.viewing(fax)}>
                        <div className="activeBox" title="Mark as Read" data-active={fax.read} onClick={this.markFaxAsRead(fax)}>
                            <div className={fax.read ? "enabled" : "disabled"}></div>
                        </div>
                        <div style={{textAlign: 'left', flex: '3 0 200px' }}>{new Date(fax.dateCreated).toLocaleString()}</div>
                        <div style={{textAlign: 'left', flex: '1 0 85px' }}>{fax.direction}</div>
                        <div style={{textAlign: 'left', flex: '2 0 135px' }}>{fax.from}</div>
                        <div style={{textAlign: 'left', flex: '2 0 135px' }}>{fax.to}</div>
                        <div style={{textAlign: 'left', flex: '1 0 100px' }}>{fax.status}</div>
                        <span className="buttonBox" style={{flex: '0 0 28px'}}>
							<img src={this.props.img.delete} title="Delete Fax" alt="Delete Fax" onClick={this.deleteFax(fax)} />
						</span>
                    </div>
                )
            }

			return (
				<div width="100%">
                    <img src={this.props.img.plus}  className="plusButton" title="Send a Fax" alt="Send a Fax" onClick={this.sending} />

                    <h1>Unread Faxes ({unreadFaxCounter})</h1>
                    <div className="dataHeaders">
						<span style={{flex: '0 0 80px', cursor: 'pointer'}}>Read</span>
						<span style={{flex: '3 0 200px', cursor: 'pointer'}} onClick={this.unreadSort('dateCreated')} >Date/Time</span>
						<span style={{flex: '1 0 85px', cursor: 'pointer'}} onClick={this.unreadSort('direction')} >Direction</span>
						<span style={{flex: '2 0 135px', cursor: 'pointer'}} onClick={this.unreadSort('from')} >From</span>
                        <span style={{flex: '2 0 135px', cursor: 'pointer'}} onClick={this.unreadSort('to')} >To</span>
                        <span style={{flex: '1 0 100px', cursor: 'pointer'}} onClick={this.unreadSort('status')} >Status</span>
						<span style={{flex: '0 0 80px'}} ></span>
					</div>
                    <div className="dataFilters">
                        <span style={{flex: '0 0 80px'}} ></span>
                        <input style={{flex: '3 0 200px'}} type="text" onChange={this.unreadFilter('dateCreated')} value={this.state.unreadFilterBy.dateCreated} />
                        <input style={{flex: '1 0 85px'}} type="text" onChange={this.unreadFilter('direction')} value={this.state.unreadFilterBy.direction} />
						<input style={{flex: '2 0 135px'}} type="text" onChange={this.unreadFilter('from')} value={this.state.unreadFilterBy.from} />
						<input style={{flex: '2 0 135px'}} type="text" onChange={this.unreadFilter('to')} value={this.state.unreadFilterBy.to} />
						<input style={{flex: '1 0 100px'}} type="text" onChange={this.unreadFilter('status')} value={this.state.unreadFilterBy.status} />
						<span style={{flex: '0 0 80px'}} ></span>
					</div>
                    {unreadFaxes}


                    <h1>Read Faxes  ({readFaxCounter})</h1>
                    <div className="dataHeaders">
						<span style={{flex: '0 0 80px', cursor: 'pointer'}}>Read</span>
						<span style={{flex: '3 0 200px', cursor: 'pointer'}} onClick={this.readSort('dateCreated')} >Date/Time</span>
						<span style={{flex: '1 0 85px', cursor: 'pointer'}} onClick={this.readSort('direction')} >Direction</span>
						<span style={{flex: '2 0 135px', cursor: 'pointer'}} onClick={this.readSort('from')} >From</span>
                        <span style={{flex: '2 0 135px', cursor: 'pointer'}} onClick={this.readSort('to')} >To</span>
                        <span style={{flex: '1 0 100px', cursor: 'pointer'}} onClick={this.readSort('status')} >Status</span>
						<span style={{flex: '0 0 80px'}} ></span>
					</div>
                    <div className="dataFilters">
                        <span style={{flex: '0 0 80px'}} ></span>
                        <input style={{flex: '3 0 210px'}} type="text" onChange={this.readFilter('dateCreated')} value={this.state.readFilterBy.dateCreated} />
                        <input style={{flex: '1 0 85px'}} type="text" onChange={this.readFilter('direction')} value={this.state.readFilterBy.direction} />
						<input style={{flex: '2 0 135px'}} type="text" onChange={this.readFilter('from')} value={this.state.readFilterBy.from} />
						<input style={{flex: '2 0 135px'}} type="text" onChange={this.readFilter('to')} value={this.state.readFilterBy.to} />
						<input style={{flex: '1 0 100px'}} type="text" onChange={this.readFilter('status')} value={this.state.readFilterBy.status} />
						<span style={{flex: '0 0 80px'}} ></span>
					</div>
                    {readFaxes}

                    {sendPane}
                    {deletePane}
                    {detail}

				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}
