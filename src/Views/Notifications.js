import React, { Component }  from 'react';
import del from '../img/delete.svg';
import plus from '../img/plus.svg';
var ReactSuperSelect = require('react-super-select')
var ReactSlider  = require('react-slider')

export default class Notifications extends Component {
	constructor(props) {
		super(props);
		this.state = { filterBy: {}, sortBy:'', sortOrder: 1,
			loading: false,
			activeNotification: false,
			activeAction: false
		};
	}

	addNotification = (e) => {
		this.setState({activeNotification:{templateName: '', file: ''}, loading: false})
	}

	editNotification = (e) => {
		for (var file of this.props.data.Notifications.dataFiles){
			if (file.fileInfoId === e.currentTarget.getAttribute('data-fileinfoid')){
				var selectedFile = file
			}
		}
		this.setState({activeNotification:{templateName: e.currentTarget.getAttribute('data-templatename'), file: selectedFile}, loading: false})
	}

	toggleNotification = (e) => {
		var notifications = this.state.notifications
		for (var not of notifications){
			console.log(not.id)
			console.log( e.currentTarget.getAttribute('data-notification'))
			if (not.id === e.currentTarget.getAttribute('data-notification')){
				if(not.enabled) {
					not.enabled = false
				} else {
					not.enabled = true
				}
				console.log(not)
			}
		}
		console.log(notifications)
		this.setState({notifications: notifications})
	}

	deleteNotification = (e) => {
		e.stopPropagation()
		this.setState({deletingNotification: true, delNotification: {templateID: e.target.getAttribute('data-templateid'), templateName: e.target.getAttribute('data-templatename') } })
	}

	cancelNotification = (e) => {
		this.setState({activeNotification: false,  deletingNotification: false, loading: false})
	}

	notificationChange = (e) => {
		var activeNotification = this.state.activeNotification
		activeNotification[e.currentTarget.getAttribute('data-type')] = e.currentTarget.value
		console.log(activeNotification)
		this.setState({activeNotification: activeNotification})
	}

	notificationSelectFile = (sel) =>{
		console.log(sel)
		var activeNotification = this.state.activeNotification
		activeNotification.file = sel ? sel : undefined
		this.setState({activeNotification: activeNotification})
	}

	confirmNotification  = (e) => {
		if( !this.state.activeNotification.templateName || this.state.activeNotification.templateName.length < 1) {
			this.setState({newNotificationError: 'Please Provide a Notification Name.'})
		}else if (!this.state.activeNotification.file) {
			this.setState({newNotificationError: 'Please Select a Data File for This Notification.'})
		}else if (!this.state.activeNotification.actions || this.state.activeNotification.actions.length < 1 ) {
			this.setState({newNotificationError: 'Please Add at Least One Action for This Notification.'})
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'addNotification', activeNotification: this.state.activeNotification  }))
		}
	}

	confirmDeleteNotification  = (e) => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'deleteNotification', delNotification: this.state.delNotification  }))
	}


	addAction = (e) => {
		if (!this.state.activeNotification.file) {
			this.setState({newNotificationError: 'Please Select a Data File for This Notification.'})
		} else {
			this.setState({activeAction: {sendWindows:[0, 86400]},  loading: false})
		}
	}

	cancelAction  = (e) => {
		this.setState({activeAction: false,  loading: false})
	}
	newActionChange = (e) => {
		var newNotification = this.state.newNotification
		newNotification[e.currentTarget.getAttribute('data-type')] = e.currentTarget.value
		this.setState({newNotification: newNotification})
	}
	confirmAddAction = (e) => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'addNotification', newNotification: this.state.newNotification  }))
	}

	deleteAction = (e) => {
		this.setState({deletingNotification: true, delNotification: e.target.getAttribute('data-value')})
	}
	cancelDeleteAction  = (e) => {
		this.setState({deletingNotification: false})
	}
	confirmDeleteAction  = (e) => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'deleteNotification', delNotification: this.state.delNotification  }))
	}

	toggleAcknowledgement = (e) =>  {
		var notifications = this.state.notifications
		for (var not of notifications){
			console.log(not.id)
			console.log( e.currentTarget.getAttribute('data-notification'))
			if (not.id === e.currentTarget.getAttribute('data-notification')){
				if(not.hasAcknowledgment) {
					not.hasAcknowledgment = false
				} else {
					not.hasAcknowledgment = true
				}
				console.log(not)
			}
		}
		console.log(notifications)
		this.setState({notifications: notifications})
	}
	toggleMessage = (e) =>  {
		var notifications = this.state.notifications
		for (var not of notifications){
			console.log(not.id)
			console.log( e.currentTarget.getAttribute('data-notification'))
			if (not.id === e.currentTarget.getAttribute('data-notification')){
				if(not.enabled) {
					not.enabled = false
				} else {
					not.enabled = true
				}
				console.log(not)
			}
		}
		console.log(notifications)
		this.setState({notifications: notifications})
	}
	togglePreconfirmation = (e) =>  {
		var notifications = this.state.notifications
		for (var not of notifications){
			console.log(not.id)
			console.log( e.currentTarget.getAttribute('data-notification'))
			if (not.id === e.currentTarget.getAttribute('data-notification')){
				if(not.hasPreMessageConfirm) {
					not.hasPreMessageConfirm = false
				} else {
					not.hasPreMessageConfirm = true
				}
				console.log(not)
			}
		}
		console.log(notifications)
		this.setState({notifications: notifications})
	}
	toggleGreeting = (e) => {
		var notifications = this.state.notifications
		for (var not of notifications){
			console.log(not.id)
			console.log( e.currentTarget.getAttribute('data-notification'))
			if (not.id === e.currentTarget.getAttribute('data-notification')){
				if(not.hasGreeting) {
					not.hasGreeting = false
				} else {
					not.hasGreeting = true
				}
				console.log(not)
			}
		}
		console.log(notifications)
		this.setState({notifications: notifications})
	}

	sort = (e) => {
		var sortOrder
		if(this.state.sortOrder === -1){
			sortOrder = 1;
		}else{
			sortOrder = -1;
		}
		this.setState({sortBy: e.target.getAttribute("data-sortby"), sortOrder: sortOrder})
	}

	filter = (e) => {
		var filterBy = this.state.filterBy
		if(e.target.value.length > 0) {
			filterBy[e.target.getAttribute("data-filterby")] = e.target.value
		} else {
			delete filterBy[e.target.getAttribute("data-filterby")]
		}
		this.setState({filterBy: filterBy})
		console.log(this.state.filterBy)
	}

	sortFunc(sortBy, sortOrder){
		return function(a,b) {
			if(sortBy in a && sortBy in b){
				if(a[sortBy] === b[sortBy]){
					return a.name > b.name ? 1 * sortOrder : -1 * sortOrder
				}
				return a[sortBy] > b[sortBy] ? 1 * sortOrder : -1 * sortOrder
			}
			else if (!(sortBy in a) && !(sortBy in b)){
				return a.name > b.name ? 1 * sortOrder : -1 * sortOrder
			}else {
				return sortBy in a ? -1  : 1
			}
		}
	}

	filterFunc(array, filterBy){
		console.log(filterBy)
		return array.filter(function(object) {
			for(var prop in filterBy){
				if(typeof object[prop] === "undefined") {
					return false
				}
				if (object[prop].toString().toLowerCase().indexOf(filterBy[prop].toString().toLowerCase()) < 0) {
					return false
				}
			}
			return true
		})
	}

	drag = (e) => {
		e.dataTransfer.setData("text", " {" + e.target.innerHTML + "} ");
	}

	actionMethodSelect = (sel) => {
		var newAction = this.state.newAction;
		newAction.method = sel.name
		this.setState({newAction: newAction})
	}

	updateSliderLabels = (e) => {
		var newAction = this.state.newAction;

		newAction.sendWindows = e
		this.setState({newAction: newAction})
	}
	convertTime(time){
		var result = new Date()
		result.setHours(0)
		result.setMinutes(0)
		result.setSeconds(time)

		return result.toLocaleTimeString().replace(":00 ", " ")
	}

	render() {
		var header
		if (this.props.active) {
			//Filtering Notification List
			var notifications
			if(Object.keys(this.state.filterBy).length > 0) {
				notifications = this.filterFunc(this.props.data.Notifications.templates.slice(), this.state.filterBy)
			}else{
				notifications = this.props.data.Notifications.templates.slice()
			}

			//Sorting filtered Notification List
			if(this.state.sortBy !== '')  {
				notifications.sort(this.sortFunc(this.state.sortBy, this.state.sortOrder))
			}


			//Listing out templates, grouped into "Notifications" based on templateName
			var notificationList = []
			var templateNames = []
			if(this.props.data.Notifications.templates.length > 0){
				for (var [i, notification] of this.props.data.Notifications.templates.entries()  ) {
					if(templateNames.indexOf(notification.templateName) < 0){
						templateNames.push(notification.templateName)
						notificationList.push(
							<div className="dataRow" tabIndex={1000 + i} key={notification.uniqueId} data-templatename={notification.templateName} data-fileinfoid={notification.fileInfoIds[0]} onClick={this.editNotification}>
								<div className="activeBox" data-active={notification.isEnabled} data-notification={notification.uniqueId} onClick={this.toggleNotification}>
									<div className={notification.isEnabled ? "enabled" : "disabled"}></div>
								</div>
								<span style={{flex: '10 0 100px'}}>{notification.templateName}</span>
								<span style={{flex: '1 0 200px'}}>{new Date(notification.modified).toLocaleString()}</span>
								<span className="buttonBox">
									<img src={del} alt="delete" data-templateid={notification.uniqueId} data-templatename={notification.templateName} onClick={this.deleteNotification}/>
								</span>
							</div>
						)
					}
				}
			} else {
				notificationList.push(
					<div  onClick={this.addNotification}>
						No notifications have been created yet. Click to add one now.
					</div>
				)
			}


			//Adding or Editing a notification, within a pop-up
			var activeNotification = ""
			if (this.state.activeNotification) {
				var templates = []
				for(var template of this.props.data.Notifications.templates ) {
					if( this.state.activeNotification.templateName === template.templateName ){
						var absOffset = Math.abs(template.sendOffsetSeconds)
						var offset = ''
						var offsetUnits = ''

						if(absOffset < 86400){
							offset = Math.round(absOffset / 3600)
							offsetUnits = 'Hours'
						} else if (absOffset < 604800){
							offset = Math.round(absOffset / 86400)
							offsetUnits = 'Days'
						} else {
							offset = Math.round(absOffset / 604800)
							offsetUnits = 'Weeks'
						}

						if(this.state.activeNotification.file){
							for(header of this.state.activeNotification.file.headers){
								if(header.uniqueId === template.sendVariable){
									var fileVariable = header.value
								}
							}
						}

						templates.push(
							<div key={template.uniqueId} style={{display: 'flex', justifyContent: 'space-between'}}>
								<span>{template.endpointTypes[0]}</span>
								<span>{offset}</span>
								<span>{offsetUnits}</span>
								<span>{template.sendOffsetSeconds <= 0 ? "Before" : "After"}</span>
								<span>{fileVariable}</span>
								<span>-</span>
							</div>
						)
					}
				}


				var selectProps = {placeholder:"Select a Data File", optionLabelKey:"originalFileName", optionValueKey:"fileInfoId" }
				if(this.state.activeNotification.file){
					selectProps.initialValue = this.state.activeNotification.file
					selectProps.disabled = true
					selectProps.clearable = false
				}
				activeNotification = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelNotification}>X</div>

							<div className="snazzyInputBox" style={{margin: '0px auto'}}>
								<input className="textInput" type="text" required value={this.state.activeNotification.templateName} data-type="templateName" onChange={this.notificationChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Notification Name</label>
							</div>


							<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px'}} {...selectProps}  dataSource={this.props.data.Notifications.dataFiles}   onChange={this.notificationSelectFile}/>

							<div style={{position: 'relative', borderBottom: '2px solid #3377994d', margin: '30px auto 0px auto', textAlign: 'center'}} >
								Actions
								<span style={{position: 'absolute', right: '0px', lineHeight: '10px', fontSize: '36px', cursor: 'pointer'}} onClick={this.addAction}>+</span>
							</div>

							{templates}

							<div className="error"  style={{margin: '30px auto 0px auto'}}>{this.state.newNotificationError}</div>

							{this.state.loading ? <div className="loader" style={{margin: '30px auto 0px auto'}}></div> : <div className="button"  style={{margin: '30px auto 0px auto'}} onClick={this.confirmAddNotification}>Create New Notification</div>}
						</div>
					</div>
				)
			}



			var delNotification = ""
			if (this.state.deletingNotification) {
				delNotification = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelNotification}>X</div>
							<div>You Are Removing Notification</div>
							<div style={{margin: "15px"}}> {this.state.delNotification.templateName} </div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmDeleteNotification}>Delete This Notification</div>}
						</div>
					</div>
				)
			}


			var activeAction = ""
			if (this.state.activeAction) {
				var headers = []
				var headerList = []
				for (header of this.state.activeNotification.file.headers){
					if(header.isVariable || true) {
						headers.push({id: header.uniqueId, name: header.value})
						headerList.push(
							<div draggable="true" style={{minWidth: '100px', padding: '5px 10px', border: '1px solid #379', display: 'inline'}} onDragStart={this.drag} key={header.uniqueId}>{header.value}</div>
						)
					};
				}
				var fields = ""
				if (this.state.newAction.method === "Call"){
					fields = (<div style={{display: 'flex', flexFlow:'column nowrap',  justifyContent: 'space-between'}}>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleGreeting}>
											<div className={notification.hasGreeting ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Greeting</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Verbiage to play at the beginning of the call."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.togglePreconfirmation}>
											<div className={notification.hasPreMessageConfirm ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Preconfirmation</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Prompt for confirmation before playing a message."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleMessage}>
											<div className={notification.enabled ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Message Body</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Main message content."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleAcknowledgement}>
											<div className={notification.hasAcknowledgment ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Acknowledgement</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Request confirmation of message receipt."/>
									</div>
								</div>
							)
				}else if (this.state.newAction.method === "Text"){
					fields = (<div style={{display: 'flex', flexFlow:'column nowrap',  justifyContent: 'space-between'}}>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleAcknowledgement}>
											<div className={notification.hasAcknowledgment ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Condition</div>
										<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px'}}  placeholder="File Variable" dataSource={headers} />
										<div>Equals</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Request confirmation of message receipt."/>
									</div>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleGreeting}>
											<div className={notification.hasGreeting ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Greeting</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Text to include before the main message."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleMessage}>
											<div className={notification.enabled ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Message Body</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Text message body."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleAcknowledgement}>
											<div className={notification.hasAcknowledgment ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Acknowledgement</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Request confirmation of message receipt."/>
									</div>
								</div>
							)
				}else if (this.state.newAction.method === "Email"){
					fields = (<div style={{display: 'flex', flexFlow:'column nowrap', justifyContent: 'space-between'}}>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleGreeting}>
											<div className={notification.hasGreeting ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Email Subject</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Subject line for the email."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleMessage}>
											<div className={notification.enabled ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Message Body</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Email message body."/>
									</div>

									<br/>
									<div style={{display: 'flex', height: '75px', width: '700px', justifyContent: 'space-between', alignItems: 'center'}} >
										<div className="activeBox" style={{margin: '0px 53px'}} data-active={notification.enabled} data-notification={notification.id} onClick={this.toggleAcknowledgement}>
											<div className={notification.hasAcknowledgment ? "enabled" : "disabled"}></div>
										</div>
										<div style={{flex: '1 1 25%'}}>Acknowledgement</div>
										<textarea style={{flex: '1 1 50%', border: '1px solid #888'}} placeholder="Request confirmation of message receipt."/>
									</div>
								</div>
							)
				}
				var sliders = []
				for(i =0; i< this.state.newAction.sendWindows.length /2 ; i++) {
					sliders.push(<div id="actionStartHandle">{ this.convertTime(this.state.newAction.sendWindows[i])}</div>)
					sliders.push(<div id="actionEndHandle">{  this.convertTime(this.state.newAction.sendWindows[i + 1])}</div>)
				}


				activeAction = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelAddAction}>X</div>


							<div style={{display: 'flex', width: '1000px', marginBottom: '15px', justifyContent: 'space-between'}}>
								<div>Notification Name: {this.state.newNotification.name} </div>
								<div>Referenced File: {this.state.newNotification.file.originalFileName} </div>
							</div>

							<div style={{display: 'flex', width: '1000px'}} >
								<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px'}}   placeholder="Method" dataSource={[{id: 1, name: "Call"},{id:2, name: "Text"},{id: 3, name: "Email"}]} onChange={this.actionMethodSelect}/>
								<input type="number" style={{flex: '1 0 150px', margin: '0px', position: 'relative', display: 'block', lineHeight: '40px', height: '40px', padding: '0px', border: '1px solid #bbb', textDecoration: 'none',  color: '#333', cursor: 'pointer', outline: 'none', boxSizing: 'border-box', textAlign: 'center'}}   placeholder="Offset"  />
								<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px'}}   placeholder="Unit" dataSource={[{id: 1, name: "Days"},{id:2, name: "Weeks"},{id: 3, name: "Hours"}]} />
								<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px'}}  placeholder="Timing" dataSource={[{id: 1, name: "Before"},{id:2, name: "After"}]} />
								<ReactSuperSelect style={{flex: '1 0 150px', margin: '0px'}}  placeholder="File Variable" dataSource={headers} />
							</div>

							<br/>
							<div style={{display: 'flex', width: '1000px', justifyContent: 'space-between'}}>
								<ReactSlider className="vertical-slider" value={this.state.newAction.sendWindows} pearling={true} onChange={this.updateSliderLabels} step={900} max={86400} defaultValue={[0, 84600]} orientation="vertical" withBars>
									{sliders}
								</ReactSlider >

								{fields}

								<div style={{display: 'flex', flexFlow: 'column nowrap', justifyContent: 'space-between' }}>
									{headerList}
								</div>
							</div>

							<div className="error">{this.state.newNotificationError}</div>


							{this.state.loading ? <div className="loader" style={{margin: '30px auto 0px auto'}}></div> : <div className="button"  style={{margin: '30px auto 0px auto'}} onClick={this.confirmAddNotification}>Add Action</div>}
						</div>
					</div>
				)
			}


			return (
				<div className="viewContent">
					<img src={plus}  className="plusButton" alt="Add a Notification" onClick={this.addNotification} />

					<div className="dataHeaders">
						<span style={{flex: '0 0 140px', cursor: 'pointer'}} onClick={this.sort} data-sortby="enabled">Active</span>
						<span style={{flex: '10 5 100px', cursor: 'pointer'}} onClick={this.sort} data-sortby="name">Notification</span>
						<span style={{flex: '1 1 200px', cursor: 'pointer'}} onClick={this.sort} data-sortby="modified">Modified</span>
						<span style={{flex: '0 0 55px'}} ></span>
					</div>

					<div className="dataFilters">
						<ReactSuperSelect style={{flex: '0 0 140px', margin: '0px'}}  placeholder="   " dataSource={[{id: 1, name: 'Active'}, {id: 0, name: 'Not Active'}]} onChange={this.filter}/>
						<input style={{flex: '10 5 100px'}} type="text" onChange={this.filter} value={this.state.filterBy.name} ref="name" data-filterby="name"/>
						<input style={{flex:  '1 1 200px'}} type="text" onChange={this.filter} value={this.state.filterBy.modified} ref="modified"  data-filterby="modified"/>
						<span style={{flex: '0 0 55px'}} ></span>
					</div>

					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{notificationList}
					</div>

					{activeNotification}
					{delNotification}
					{activeAction}
				</div>
			);
		} else {
			return (
				<div>

				</div>
			);
		}
	}
}
