import React, { Component }  from 'react';

export default class Templates extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			activeTemplate: false,
            delTemplate: false
		};
	}

    cancel = (e) => {
        this.setState({activeTemplate: false,  delTemplate: false, loading: false})
    }

	editTemplate = (e) => {
        for(var template of this.props.data.Templates){
            if(e.currentTarget.getAttribute('data-templatename') == template.templateName){
                var activeTemplate = Object.assign({}, template)
            }
        }
		this.setState({activeTemplate: activeTemplate, loading: false})
	}

	templateChange = (e) => {
		var activeTemplate = Object.assign({}, this.state.activeTemplate)
        var type = 0
        if(e.currentTarget.getAttribute('data-type') == 'Spanish'){
            type = 1
        }
		activeTemplate.textMessageBody[type] = e.currentTarget.value
		console.log(activeTemplate)
		this.setState({activeTemplate: activeTemplate})
	}

	confirmTemplate  = (e) => {
		if( this.state.activeTemplate.textMessageBody[0].length < 1 ||  this.state.activeTemplate.textMessageBody[1].length < 1) {
			this.setState({templateError: 'Please Provide a Notification Name.'})
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editTemplate', activeTemplate: this.state.activeTemplate  }))
		}
	}

	render() {
		var header
		if (this.props.active) {
			var notifications = this.props.data.Templates.slice()

			//Listing out templates, grouped into "Notifications" based on templateName
			var notificationList = []
			if(this.props.data.Templates.length > 0){
				for (var [i, notification] of this.props.data.Templates.entries()  ) {
						notificationList.push(
							<div className="dataRow" tabIndex={1000 + i} key={notification.uniqueId} data-templatename={notification.templateName} data-fileinfoid={notification.fileInfoIds[0]} onClick={this.editTemplate}>
								<div className="activeBox" data-active={notification.isEnabled} data-notification={notification.uniqueId}>
									<div className={notification.isEnabled ? "enabled" : "disabled"}></div>
								</div>
								<span style={{flex: '10 0 100px'}}>{notification.templateName}</span>
								<span style={{flex: '1 0 200px'}}>Modified: {new Date(notification.modified).toLocaleString()}</span>
							</div>
						)
				}
			} else {
				notificationList.push(
					<div>
						No notifications have been created yet.
					</div>
				)
			}


			//Adding or Editing a notification, within a pop-up
			var activeTemplate = ""
			if (this.state.activeTemplate) {
                activeTemplate = (
                    <div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancel}>X</div>
                        <div style={{flex: '1 1 25%'}}>English Verbiage</div>
                        <textarea style={{margin: '10px', width: '400px', height: '100px', flex: '1 1 100%', border: '1px solid #888'}} data-type="English" onChange={this.templateChange} value={this.state.activeTemplate.textMessageBody[0]} placeholder="Text message body."/>
                        <br/>
                        <div style={{flex: '1 1 25%'}}>Spanish Verbiage</div>
                        <textarea style={{margin: '10px', width: '400px', height: '100px', flex: '1 1 100%', border: '1px solid #888'}} data-type="Spanish" onChange={this.templateChange} value={this.state.activeTemplate.textMessageBody[1]} placeholder="Text message body."/>

                        <div className="error">{this.state.templateError}</div>

                        {this.state.loading ? <div className="loader" style={{margin: '30px auto 0px auto'}}></div> : <div className="button"  style={{margin: '30px auto 0px auto'}} onClick={this.confirmTemplate}>Confirm Template</div>}
                        </div>
					</div>

                )
			}

			return (
				<div className="viewContent">
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{notificationList}
					</div>

					{activeTemplate}
				</div>
			);
		} else {
			return (
				<div>

				</div>
			);
		}
	}
}
