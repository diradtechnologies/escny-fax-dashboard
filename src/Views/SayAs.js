import React, { Component }  from 'react';
import del from '../img/delete.svg';
import plus from '../img/plus.svg';


class SayAS extends Component {constructor(props) {
		super(props);
		this.state = { 
			active: false, 
			editing: false,
			loading: false,
			setting: false,
			rep:{},
			replacements:[
				{heard:"slomins",
				submit: "slow-mins"},
				{heard:"DiRAD",
				submit: "die rad"}
			]
		};
	}
	add = () => {
		this.setState({editing: true, loading: false, rep:{}})
	}
	
	edit = (rep) => (e) => {
		this.setState({editing: true, loading: false, rep:{rep}})
	}
	editHeard =(e)=> {
		this.setState({editing: true, loading: false, rep:{heard: e.currentTarget.value , submit: this.state.rep.submit }})
	}
	
	editSubmit=(e)=> {
		this.setState({editing: true, loading: false, rep:{heard:this.state.rep.heard, submit: e.currentTarget.value }})
	}
	conf =()=>{
		var replacements = this.state.replacements.slice()
		replacements.push(this.state.rep)
		this.setState({editing: false, loading: false, replacements:replacements})
	}
	deleteExclusion = (rep) => (e) => {
		var replacements = this.state.replacements.slice()
		for(var i=0;i<replacements.length; i++){
			if(replacements[i].heard === rep.heard){
				replacements.splice(i,1)
			}
		}
		this.setState({editing: false, loading: false, replacements:replacements})
	}
	
	cancel = () => {
		this.setState({editing: false, loading: false, setting: false})
	}
	
	
	
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel();
		}
	}
	render() {
		if (this.props.active) {	
			var exclusions = []
				for (var rep of this.state.replacements) {
					exclusions.push(
						<div className="dataRow" key={rep.heard}>
							<span style={{flex: '0 0 300px'}} onClick={this.edit(rep)}>{rep.heard}</span>
							<span style={{flex: '1 0 20%', textAlign: 'center'}} onClick={this.edit(rep)} >{rep.submit}</span>
							<span className="buttonBox" style={{flex: '0 0 280px'}}>
								<img src={del} alt="Delete Exclusion" onClick={this.deleteExclusion(rep)} />
							</span>
							
						</div>
					)
				}
			var edit = ""
			if (this.state.editing) {
				edit = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							
							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.rep.heard} onChange={this.editHeard} ></input>
								<span className="inputBar"></span><label className="textLabel">Word/Phrase</label>
							</div>
							
							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.rep.submit} onChange={this.editSubmit} ></input>
								<span className="inputBar"></span><label className="textLabel">Say As...</label>
							</div>
							
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.conf}>Confirm Exclusion</div>}
						</div>
					</div>
				)
			}	
			return (
				<div className="viewContent" id="reportView" >
					<img src={plus}  className="plusButton" alt="Add" onClick={this.add} />
					<div className="dataHeaders">
						<span style={{flex: '0 0 300px', cursor: 'pointer'}} onClick={this.sort} data-sortby="name">Word/Phrase</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortby="modified">Say As...</span>
						<span style={{flex: '0 0 330px'}}></span>
					</div> 
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{exclusions}
						{edit}
					</div>
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default SayAS