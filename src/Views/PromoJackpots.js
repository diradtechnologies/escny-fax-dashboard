import React, { Component }  from 'react';
import Flatpickr from 'react-flatpickr'

class PromoJackpots extends Component {
	constructor(props) {
		super(props);
		this.state = { 	
			editingMessage: false,
			loading: false,
		};

	}
	
	editMessage = (message) => e => {
		this.setState({editingMessage: true, loading: false, game: message.Game, jackpot: parseInt(message.Jackpot.replace('$','').replace(new RegExp(',', 'g'), ''),10), error: '', start: message.Start_DateTime, end: message.End_DateTime})
	}
	editJackpot = (e) =>  {
		if (e.target.value < 1){
			this.setState({jackpot: 0, error: 'Please enter a jackpot value.'});
		} else {
			this.setState({jackpot: e.target.value, error: ''});
		}
	}
	cancelEditMessage = () => {
		this.setState({editingMessage: false, loading: false, game: false, start:false, end: false})
	}
	confirmEditMessage = () => {
		if (this.state.jackpot < 1){
			this.setState({error: 'Please enter a jackpot amount. (ex: enter 50000000 for fifty million)'});
		} else if (new Date(this.state.start) > new Date( this.state.end)) {
			this.setState({error: 'End date must not be before start date.'});
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editPromoJackpot', game: this.state.game,  jackpot: this.state.jackpot, start: this.state.start, end: this.state.end  }))		
		}	
	}
	
	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmEditMessage();
		}
	}
	
	toggleMessage = (e) => {
		e.stopPropagation()
		var newActive = e.currentTarget.getAttribute('data-active') === "true" ? false : true
		this.props.ws.send(JSON.stringify({route: 'togglePromoJackpot', game: e.currentTarget.getAttribute('data-game'), active: newActive }))	
	}
	
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.setState({ editingMessage: false, loading: false });
		}
	}
	
	render() {
		if (this.props.active) {	
			var pjList = []
			for (var promo of this.props.data.PromoJackpots) {
				pjList.push(
					<div className="dataRow" key={promo.Game} onClick={this.editMessage(promo)}>
						<div className="activeBox" data-active={promo.Active} data-game={promo.Game} onClick={this.toggleMessage}>
							<div className={promo.Active ? "enabled" : "disabled"}></div>
						</div>
						<span style={{flex: '0 0 150px'}} >{promo.Game.replace('_', ' ')}</span>
						<span style={{flex: '1 0 20%'}} onClick={this.changeDescription} >{'$' + promo.Jackpot}</span>
						<span style={{flex: '3 0 30%'}} onClick={this.changeStatus} >{new Date(promo.Start_DateTime).toLocaleString() +' - '+ new Date(promo.End_DateTime).toLocaleString()}</span>
					</div>
				)
			}
			
			
			var editMessage = ""
			if (this.state.editingMessage) {
				editMessage = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancelEditMessage}>X</div>
							<div>You Are Editing Promo Jackpot Message for {this.state.game.replace('_', ' ')}</div>
							<br/>
							<div className="snazzyInputBox">
								<input className="textInput" type="number" min="0" required value={this.state.jackpot} onChange={this.editJackpot} ></input>
								<span className="inputBar"></span><label className="textLabel">Jackpot Value</label>
							</div>
							
							<div style={{display: 'inline-block'}}>
								<div>Start</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>
							<div style={{display: 'inline-block'}}>
								<div>End</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d h:i K"}} data-enable-time value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) :this.setState({end: undefined}) }} />
							</div>
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditMessage}>Confirm Promo Jackpot</div>}
						</div>
					</div>
				)
			}
			
		
			return (
				<div width="100%">
					{pjList}
					{editMessage}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default PromoJackpots