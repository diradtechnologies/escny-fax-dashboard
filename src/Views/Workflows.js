import React, { Component }  from 'react';

class Workflows extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			active: false, 
			editing: false,
			loading: false,
		};
	}
	
	
	cancel = () => {
		this.setState({editing: false, loading: false, setting: false})
	}
	
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel();
		}
	}
	render() {
		if (this.props.active) {	
			return (
				<div className="viewContent" id="reportView" >
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
				
					</div>
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default Workflows