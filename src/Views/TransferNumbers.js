import React, { Component }  from 'react';

class TransferNumbers extends Component {
	constructor(props) {
		super(props);
		this.state = { 	
			editing: false,
			loading: false
		};
	}
	
	cancel = () => {
		this.setState({editing: false, loading: false, Display: false, Description: false, Language: false, phoneNumber:false, active: null})
	}
	editValue = (e) =>  {
		if (e.target.value.length < 1){
			this.setState({phoneNumber: '', error: 'Please enter a transfer number.'});
		} else {
			this.setState({phoneNumber: e.target.value.replace(/[^0-9]/gm, ''), error: ''});
		}
	}
	
	editNumber = (num) => e => {
		this.setState({editing: true, loading: false, Display: num.DisplayName, Description: num.Description, Language:num.Language, phoneNumber:  num.Phone_Number, active: num.is_enabled, error: ''})
	}
	confirmEditNumber = () => {
		if (this.state.phoneNumber.length < 10){
			this.setState({error: 'Please enter a ten digit transfer number.'});
		} else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editTransferNumber', Description: this.state.Description,  Language: this.state.Language, phoneNumber: this.state.phoneNumber, active: this.state.active }))		
		}	
	}
	
	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmEditNumber();
		}
	}
	toggle = (param) => (e) => {
		var newState = Object.assign({}, this.state)
		newState[param] = this.state[param] ? false : true
		this.setState(newState)
	}
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel()
		}
	}
	
	render() {
		if (this.props.active) {	
			var numList = []
			for (var num of this.props.data.TransferNumbers) {
				numList.push(
					<div className="dataRow" key={num.RecNum} onClick={this.editNumber(num)}>
						<div className="activeBox">
							<div className={num.is_enabled ? "enabled" : "disabled"}></div>
						</div>
						<span style={{flex: '1 0 300px'}} >{num.DisplayName ? num.DisplayName : num.Description}</span>
						<span style={{flex: '0 0 200px', textAlign:'center'}} >{num.Language}</span>
						<span style={{flex: '0 0 300px', textAlign: 'center'}} >{num.Phone_Number}</span>
					</div>
				)
			}
			
			
			var editNum = ""
			if (this.state.editing) {
				editNum = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Editing the {this.state.Language} Transfer Number for</div>
							<br/>
							<div>{this.state.Display ? this.state.Display :  this.state.Description}</div>
							<br/>
							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.phoneNumber} onChange={this.editValue} ></input>
								<span className="inputBar"></span><label className="textLabel">Telephone Number</label>
							</div>
							
							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Active</div>
									<div className="activeBox" onClick={this.toggle('active')}>
										<div className={this.state.active ? "enabled" : "disabled"}></div>
									</div>
								</div>
							</div>
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditNumber}>Confirm Transfer Number</div>}
						</div>
					</div>
				)
			}
			
		
			return (
				<div width="100%">
					<div className="dataHeaders">
						<span style={{flex: '0 0 83px', cursor: 'pointer', textAlign: 'center'}}>Active?</span>
						<span style={{flex: '1 0 300px', cursor: 'pointer', textAlign: 'left'}}>Description</span>
						<span style={{flex: '0 0 200px', cursor: 'pointer'}}>Language</span>
						<span style={{flex: '0 0 300px', cursor: 'pointer'}}>Telephone Number</span>
					</div> 
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{numList}
					</div>
					
					{editNum}

				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default TransferNumbers