import React  from 'react';
import Flatpickr from 'react-flatpickr'
import { Chart } from 'react-google-charts';
import pdf from '../img/pdf.svg';
import excel from '../img/excel.svg';
import print from '../img/print.svg';

class Reports extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			active: false,
			activeReport: false,
			activeCall: false,
			playingRecording: false,
			recLoading: false
		};
	}

	play = (call) =>  {
		this.setState({playingRecording: true, activeCall: call}, function(){
			this.props.ws.send(JSON.stringify({route: 'getTwilioRecording', call: this.state.activeCall}))
		})
	}

	cancelRecording = () => {
		this.setState({playingRecording: false, activeCall: false})
	}

	selectReport = (e) => {
		this.setState({activeReport: JSON.parse(e.currentTarget.getAttribute('data-report')),  parameters: {}, reportData: false})
	}

	cancelReport = (e) => {
		this.setState({activeReport: false})
		this.props.ws.send(JSON.stringify({route: 'newReport'}))
		this.forceUpdate()
	}

	updateReportParameter = (param, e) => {
		var newParameters = Object.assign({},this.state.parameters)

		newParameters[param] = e.currentTarget.value
		this.setState({parameters: newParameters})
	}

	updateDateParameter = (e, value) => {
		var newParameters = Object.assign({}, this.state.parameters)
		var date
		if(value[0]){
			date = {datetime: value[0], offset: value[0].getTimezoneOffset() * 60000}

		}else {
			date = undefined
		}

		newParameters[e] = date
		this.setState({parameters: newParameters})
	}

	updateDateTimeParameter = (e, value) => {
		var newParameters = Object.assign({}, this.state.parameters)
		if(value[0]){
			newParameters[e] = {datetime: value[0], offset: value[0].getTimezoneOffset() * 60000}
		}else{
			newParameters[e] = undefined
		}
		//newParameters.offset = value[0].getTimezoneOffset() * 60000
		this.setState({parameters: newParameters})
	}

	runReport = (exp, e) => {
		var reportParams = Object.assign({},this.state.parameters)
		reportParams.route = this.state.activeReport.route
		reportParams.report = this.state.activeReport
		reportParams.exp = exp

		this.props.ws.send(JSON.stringify(reportParams))
	}

	base64ToBlob = (base64Data, contentType) => {

		contentType = contentType || '';
		var sliceSize = 1024;
		var byteCharacters = atob(base64Data);
		var bytesLength = byteCharacters.length;
		var slicesCount = Math.ceil(bytesLength / sliceSize);
		var byteArrays = new Array(slicesCount);

		for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
			var begin = sliceIndex * sliceSize;
			var end = Math.min(begin + sliceSize, bytesLength);

			var bytes = new Array(end - begin);
			for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
				bytes[i] = byteCharacters[offset].charCodeAt(0);
			}
			byteArrays[sliceIndex] = new Uint8Array(bytes);
		}
		return new Blob(byteArrays, { type: contentType });
	}
	/*shouldComponentUpdate(nextProps, nextState){
		console.log('considering changes')
		return true
		if(this.props.active !== nextProps.active){
			return true
		}else if(JSON.stringify(this.state.activeReport)  !== JSON.stringify(nextState.activeReport)){
			return true
		}else if(JSON.stringify(this.state.parameters)  !== JSON.stringify(nextState.parameters)){
			return true
		}else if(JSON.stringify(this.props.data.download) !== JSON.stringify(nextProps.data.download)){
			return true
		}else if(JSON.stringify(this.props.data.dynamicParams) !== JSON.stringify(nextProps.data.dynamicParams)){
			console.log('params changed')
			return true
		}else if(JSON.stringify(this.props.data.reportData) !== JSON.stringify(nextProps.data.reportData)){
			return true
		} else if(this.props.data.reportLoading){
			return true
		} else {
			return false
		}
	}*/
	componentDidUpdate(prevProps, prevState){
		if(this.props.data.download && this.props.data.download.data && (!prevProps.data.download  || this.props.data.download.data !== prevProps.data.download.data)){
			var url = window.URL.createObjectURL(this.base64ToBlob(this.props.data.download.data, "application/zip"));
			var a = document.createElement("a");
			a.style = "display: none";
			a.href = url;
			a.download = this.state.activeReport.name + ' -- ' +  new Date().toISOString() + '.' + this.props.data.download.type;

			document.body.appendChild(a);
			a.click();

			document.body.removeChild(a);
		}
	}
	convertSec(sec) {
		sec = Math.round(sec)
		var d, h, m, s;
		s = sec
		m = Math.floor(s / 60);
		s = s % 60;
		h = Math.floor(m / 60);
		m = m % 60;
		d = Math.floor(h / 24);
		h = h % 24;

		var time = ''

		d > 0 ? time += d + ' days, ' : time += ''
		h < 1 ? time += '': h > 0 && h < 10 ? time +=  '0' + h + ':' : time += h + ':'
		m < 1  ? time += '00:' : m < 10 ? time +=  '0' + m + ':' : time += m + ':'
		s < 1  ? time += '00' : s < 10 ? time +=  '0' + s : time += s

		return time;
	};
	formatPhoneNumber(num){
		if(!num){return false};
		if (num.length === 0) {
			return "No Number Available"
		}else if (num.length < 7) {
			return num;
		}else if (num.length === 7){
			return (num.substring(0, 3)+'-'+num.substring(3, 7))
		}else if (num.length === 10) {
			return ('('+num.substring(0, 3)+') '+num.substring(3, 6)+'-'+num.substring(6, 10))
		}else if (num.length === 11) {
			return ('('+num.substring(1, 4)+') '+num.substring(4, 7)+'-'+num.substring(7, 11))
		}else if (num.length === 12) {
			return ('('+num.substring(2, 5)+') '+num.substring(5, 8)+'-'+num.substring(8, 12))
		}else {
			return num;
		}
	}

	render() {
		if (this.props.active) {
			var reports = []
			for(var rep of this.props.data.Reports.reportList){
				reports.push(
					<div className="dataRow" key={rep.route} data-report={JSON.stringify(rep)} onClick={this.selectReport} >{rep.name}</div>
				)
			}

			var activeReport = ""
			var row
			if(this.state.activeReport && this.state.activeReport.name){
				var paramHeaders = []
				var reportParams = []
				var choice
				var parameterOptions
				for (var parameter of this.state.activeReport.parameters){
					paramHeaders.push(
						<div key={parameter.label}>{parameter.label}</div>
					)
					if(parameter.type === 'datetime'){
						reportParams.push(
							<Flatpickr  key={parameter.label} data-enable-time options={{dateFormat:"Y-m-d h:i K", defaultHour:0, defaultMinute:0}} value={this.state.parameters[parameter.parameter] ? this.state.parameters[parameter.parameter].datetime : null} onChange={this.updateDateTimeParameter.bind(this, parameter.parameter) } />
						)
					} else if (parameter.type === 'date'){
						reportParams.push(
							<Flatpickr key={parameter.label}   options={{dateFormat:"Y-m-d h:i K", defaultHour:0, defaultMinute:0}} value={!this.state.parameters[parameter.parameter] || this.state.parameters[parameter.parameter].datetime} onChange={this.updateDateParameter.bind(this, parameter.parameter)} />
						)
					} else if (parameter.type === 'text'){
						reportParams.push(
							<input key={parameter.label}  type="text" value={this.state.parameters[parameter.parameter]} onChange={this.updateReportParameter.bind(this, parameter.parameter)} />
						)
					} else if (parameter.type === 'dropdown'){
						 parameterOptions = [<option value={null} key="blankOption"></option>]
						for (choice of parameter.options){
							parameterOptions.push(
								<option value={choice} key={choice}>{choice}</option>
							)
						}

						reportParams.push(
							<select key={parameter.label}  onChange={this.updateReportParameter.bind(this, parameter.parameter)} value={this.state.parameters[parameter.parameter]}>
								{parameterOptions}
							</select>
						)
					}else if (parameter.type === 'dynamic'){
						if(this.props.data.dynamicParams && this.props.data.dynamicParams[parameter.parameter]){
							parameterOptions = [<option value={null} key="blankOption"></option>]
							for (choice of this.props.data.dynamicParams[parameter.parameter]){
								parameterOptions.push(
									<option value={choice} key={choice}>{choice}</option>
								)
							}
							reportParams.push(
								<select key={parameter.label}  onChange={this.updateReportParameter.bind(this, parameter.parameter)} value={this.state.parameters[parameter.parameter]}>
									{parameterOptions}
								</select>
							)
						}else {
							this.props.ws.send(JSON.stringify({route: this.state.activeReport.route + '_' + parameter.parameter }))
							reportParams.push(
								<input key={parameter.label}  type="text" value={this.state.parameters[parameter.parameter]} onChange={this.updateReportParameter.bind(this, parameter.parameter)} />
							)
						}
					}
				}
				paramHeaders.push(
						<div key="paramHeader"></div>
					)

				var report = []
				var rows, i, options, columns

				if(this.props.data.reportData){
					if(this.props.data.reportData.graph){
						if(this.props.data.reportData.graph.type === 'pie'){
							report.push(
								<div>
									<h2 style={{textAlign: 'center', color: '#2A6AA9'}}>{this.props.data.reportData.graph.title }</h2>
									<div className="mini-chart-container"  style={{display:'inline-block', width: '100%', height: '40vh'}}>

										<Chart
											chartType="PieChart"
											columns={this.props.data.reportData.graph.columns}
											rows={this.props.data.reportData.graph.rows}
											options={
												{pieHole: 0,
													legend: 'labeled',
                                                    sliceVisibilityThreshold: 0,
                                                    pieStartAngle: 90,
												pieSliceTextStyle: {color: '#fff', fontSize: '1vw'  },
												chartArea: { width: "90%",	height: "90%" }
												}
											}
											graph_id="pieChart"
											height="100%"
											width="100%"
										/>
									</div>
								</div>
							)
						} else if(this.props.data.reportData.graph.type === 'bar'){
								rows = this.props.data.reportData.graph.rows.slice()

								for(i = 0; i < this.props.data.reportData.graph.columns.length; i++){
									if(this.props.data.reportData.graph.columns[i].type === 'datetime'){
										for(row of rows){
											if(row[i]){row[i] = new Date(row[i])}
										}
									} else if(this.props.data.reportData.graph.columns[i].type === 'number'){
										for(row of rows){
											if(row[i]){row[i] = parseInt(row[i],10)}
										}
									}  else if(this.props.data.reportData.graph.columns[i].role === 'tooltip'){
										for(row of rows){
											if(row[i]){
												var day = new Date(row[0]).getDay()
												switch(day) {
													case 0:
														day = 'Sunday'
														break;
													case 1:
														day = 'Monday'
														break;
													case 2:
														day = 'Tuesday'
														break;
													case 3:
														day = 'Wednesday'
														break;
													case 4:
														day = 'Thursday'
														break;
													case 5:
														day = 'Friday'
														break;
													case 6:
														day = 'Saturday'
														break;
													default:
														day = null
												}

												var time = new Date(row[0]).toLocaleTimeString()
												row[i] = day + ' ' + time + '<br/> ' + this.props.data.reportData.graph.columns[1].label + ': ' + row[1]
											}
										}
									}
								}
								options = {
									vAxis: {format: 0, gridlines: {count: -1}, title: this.props.data.reportData.graph.columns[1].label, titleTextStyle: {italic: false, fontSize: 18, color: '#2A6AA9'}},
									chartArea: {  width: "90%", height: "70%" },
									legend: { position: 'none' },
									animation: {duration: 500},
									colors:['#2A6AA9'], bar: {groupWidth: 15}, tooltip: { isHtml: true }}
								columns = this.props.data.reportData.graph.columns

								//[{type: 'datetime', label: 'Time'},{ type: 'number', label: 'Number of Calls'},{type: 'string', role: 'tooltip', p: {html: true}}]

								options.hAxis = {
									format: 'EEEE',
									gridlines: {
										count: -1,
										units: {days: {format:'EEEE'}}
									}
								}

								report.push(
									<div className="chart-container" style={{display:'inline-block', width: '100%'}}>
										<h2 style={{textAlign: 'center', color: '#2A6AA9',marginBottom: '0px'}}>{module.title}</h2>
										<Chart
											chartType="ColumnChart"
											columns={columns}
											rows={rows}
											options={options}
											graph_id="historyChart"
											width="100%"
										/>
									</div>
								)
							} else if(this.props.data.reportData.graph.type === 'area'){
    							rows = this.props.data.reportData.graph.rows.slice()

    							for(i = 0; i < this.props.data.reportData.graph.columns.length; i++){
    								if(this.props.data.reportData.graph.columns[i].type === 'datetime' || this.props.data.reportData.graph.columns[i].type === 'date'){
    									for(row of rows){
    										if(row[i]){row[i] = new Date(row[i])}
    									}
    								} else if(this.props.data.reportData.graph.columns[i].type === 'number'){
    									for(row of rows){
    										//if(row[i]){row[i] = parseInt(row[i],10)}
    									}
    								}
    							}
    							options = {
                                    vAxis: {gridlines: {count: 0}},
                                    series: {
                                        0:{
                                            type:"Area",
                                            areaOpacity: .3
                                        },
                                        1: {
                                            type:"Area",
                                            areaOpacity: 1
                                        }
                                    },
                                    areaOpacity: 1,
                                    title: this.props.data.reportData.graph.columns[1].label,
                                    titleTextStyle: {italic: false, fontSize: 18, color: '#2A6AA9'},
    								chartArea: {  width: "90%", height: "70%" },
    								legend: { position: 'none' },
                                    isStacked: false,
    								animation: {duration: 500},
    								colors:['#2A6AA9', '#937'], bar: {groupWidth: 15}, tooltip: { isHtml: true }
                                }

                                columns = this.props.data.reportData.graph.columns


    							report.push(
    								<div className="chart-container" style={{display:'inline-block', width: '100%'}}>
    									<h2 style={{textAlign: 'center', color: '#2A6AA9',marginBottom: '0px'}}>{module.title}</h2>
    									<Chart
    										chartType="SteppedAreaChart"
    										columns={columns}
    										rows={rows}
    										options={options}
    										graph_id="historyChart"
    										width="100%"
    									/>
    								</div>
    							)
    						} else if(this.props.data.reportData.graph.type === 'timeline'){
    							rows = this.props.data.reportData.graph.rows.slice()

    							for(i = 0; i < this.props.data.reportData.graph.columns.length; i++){
    								if(this.props.data.reportData.graph.columns[i].type === 'datetime' || this.props.data.reportData.graph.columns[i].type === 'date'){
    									for(row of rows){
    										if(row[i]){row[i] = new Date(row[i])}
    									}
    								} else if(this.props.data.reportData.graph.columns[i].type === 'number'){
    									for(row of rows){
    										//if(row[i]){row[i] = parseInt(row[i],10)}
    									}
    								}
    							}
    							options = {
                                    height:100,
                                    vAxis: {gridlines: {count: 0}},
                                    timeline: {
                                        showBarLabels: false
                                    },
                                    title: this.props.data.reportData.graph.columns[1].label,
                                    titleTextStyle: {italic: false, fontSize: 18, color: '#2A6AA9'},
    								chartArea: {  width: "90%", height: "70%" },
    								legend: { position: 'none' },
    								colors: this.props.data.reportData.graph.colors,
                                    bar: {groupWidth: 15},
                                    tooltip: { isHtml: true }
                                }

                                columns = this.props.data.reportData.graph.columns


    							report.push(
    								<div className="chart-container" style={{display:'inline-block', width: '100%'}}>
    									<h2 style={{textAlign: 'center', color: '#2A6AA9',marginBottom: '0px'}}>{module.title}</h2>
    									<Chart
    										chartType="Timeline"
    										columns={columns}
    										rows={rows}
    										options={options}
    										graph_id="historyChart"
    										width="100%"
                                            height="100px"
    									/>
    								</div>
    							)
    						}
					}

					if(this.props.data.reportData.list){
						for (var data in this.props.data.reportData.list){
							if(this.props.data.reportData.list[data] === null){
								report.push(<div key={data} className="reportRow"></div>)
							}else{
								report.push(<div key={data} className="reportRow">{data + ': ' + this.props.data.reportData.list[data]}</div>)
							}

						}
					}

					if(this.props.data.reportData.table){
						for(i = 0; i < this.props.data.reportData.table.columns.length; i++){
							if(this.props.data.reportData.table.columns[i].type === 'datetime'){
								for(row of this.props.data.reportData.table.rows){
									if(row[i]){row[i] = new Date(row[i])}
								}
							} else if(this.props.data.reportData.table.columns[i].type === 'number'){
								for(row of this.props.data.reportData.table.rows){
									if(row[i]){row[i] = parseInt(row[i],10)}
								}
							} else if(this.props.data.reportData.table.columns[i].type === 'phone'){
								for(row of this.props.data.reportData.table.rows){
									this.props.data.reportData.table.columns[i].type = 'string'
									if(row[i]){row[i] = this.formatPhoneNumber(row[i],10)}
								}
							} else if(this.props.data.reportData.table.columns[i].type === 'recording') {
								for(row of this.props.data.reportData.table.rows){
									this.props.data.reportData.table.columns[i].type = 'string'
									if(row[i]){row[i] = '<div style="left: Calc(50% - 7.5px); cursor: pointer; position: relative;border-radius: 15px; height: 15px; width: 15px; background-color: #933;" onclick="window.ReportsFunctions.play(\''+row[i]+'\');"></div>'}
								}
							}
						}

						if(this.props.data.reportData.table.rows.length > 0){
							report.push(<div key="recordCount">{this.props.data.reportData.table.rows.length === 100000 ? 'First 100,000 Records Displayed' : 'Total Records Returned: ' + this.props.data.reportData.table.rows.length }</div>)
							report.push(
								<Chart
									key="chart"
									chartPackages= {['corechart', 'table']}
									chartType="Table"
									options={{
										allowHtml: true,
										page: 'enable',
										pageSize: 100,
										pagingButtons: 'auto',
										width: '100%'

									}}
									columns={this.props.data.reportData.table.columns}
									rows={this.props.data.reportData.table.rows}
									graph_id={"reportTable"}
									width='85vw'
									height='auto'
								/>
							)
						}else {
							report.push(<div key="noresults">No Results Returned</div>)
						}
					}
				}

				activeReport = (
					<div className="blackout">
						<div className="pop">
							<div style={{fontSize: '24px', padding: '15px'}}>{this.state.activeReport.name}</div>

							{this.props.data.reportData ? <img alt="Excel Icon" src={excel} onClick={this.runReport.bind(this, "excel")} style={{cursor: "pointer", position: "absolute", top: "50px", right: "72px"}} /> : ''}

							{false && this.props.data.reportData && (!this.props.data.reportData.table || this.props.data.reportData.table.rows.length < 5001) ? <img alt="PDF Icon" src={pdf}  onClick={this.runReport.bind(this, "pdf")} style={{cursor: "pointer", position: "absolute", top: "50px", right: "132px"}}/> : ''}

							{false && this.props.data.reportData && (!this.props.data.reportData.table || this.props.data.reportData.table.rows.length < 5001) ? <img alt="Print Icon" src={print}  onClick={window.print} style={{cursor: "pointer", position: "absolute", height: "30px", top: "50px", right: "192px"}}/> : ''}

							<div className="viewCloser" style={{display: 'block'}} onClick={this.cancelReport}>X</div>

							<div style={{margin: '5px 0px 5px 0px'}} className="reportHeaders">
								{paramHeaders}
							</div>

							<div style={{margin: '5px 0px 15px 0px'}} className="reportFilters">
								{reportParams}
								<div className="button" onClick={this.runReport.bind(this, false)} >Start</div>
							</div>

							{this.props.data.reportLoading ? <div style={{position: "static", height: "100px"}}><div style={{position: "absolute", left: "50%", transform: "translateX(-50%)"}}><div style={{margin: "100px"}} className="loader"></div></div></div> : report}

						</div>
					</div>
				)
			}

			var recordingPlayer = ""
			if (this.state.playingRecording) {
				recordingPlayer = (
					<div className="blackout" onClick={this.stopClick}>
						<div className="pop">
							<div className="viewCloser" style={{display: 'block'}} onClick={this.cancelRecording}>X</div>
							<div className="loginLabel">Listen to or Download Recording</div>
							<div className="error">{this.state.recordingError}</div>
							{!this.props.data.reportData.recording ? '' :<audio id="recordingPlayer" style={{width: "500px", height: "50px", margin: "25px"}} src={"https://api.twilio.com" + this.props.data.reportData.recording.replace('.json', '')} download={this.state.activeCall + '.wav'} controls controlsList="nofullscreen nodownload noremoteplayback" />}
							{!this.props.data.reportData.recording ? <div className="loader"></div> : <a style={{textDecoration: "none"}} href={window.location.origin + '/recording' + this.props.data.reportData.recording.replace('.json', '')} download={this.state.activeCall + '.wav'} > <div className="button" >Download Recording</div></a>}
						</div>
					</div>
				)
			}

			return (
				<div className="viewContent" id="reportView" >
					<div style={{overflow: 'auto', height: 'calc(100% - 5px)'}}>
						{reports}
					</div>

						{activeReport}
						{recordingPlayer}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default Reports
