import React, {Component} from 'react';
import plus from '../img/plus.svg';
import redLock from '../img/redLock.svg';
import reset from '../img/unlock.svg';
import del from '../img/delete.svg';
import group from '../img/group.svg';
import roleIcon from '../img/role.svg';

class Users extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			time: new Date(),
			filterBy: {}, sortBy:'lastName', sortOrder: 1,

			addingUser: false,
			newUserError: '',
			newUserID: '', newUserIDError: '',
			newUserFirst: '', newUserFirstError: '',
			newUserLast: '', newUserLastError: '',
			newUserPIN: '', newUserPINError: '',
            newUserRec: '', newUserRecNameError: '',
			newUserRecPin: '', newUserRecPinError: '',

			resettingUser: false,
			resetUser: '',
			resetUserName: '',
			resetUserPIN: '',
			resetUserPINError: '',

			editingRoles: false,
			roleUser:'',
			roleUserName: '',
			rolesToAdd: [],
			rolesToRemove: [],

			changingName: false,
			changingNameUser: '', changingNameFirstError: '',
			changingNameFirst: '', changingNameLastError: '',
			changingNameLast: '',

			changingStatus: false,
			statusUser: '',
			statusUserName: ''
		};
	}

	componentDidMount = () => {
		this.timerTick = setInterval(this.tick, 1000);
	}

	componentWillUnmount = () => {
		clearInterval(this.timerTick)
	}

	tick = () => {
		if(this.props.active){
			this.setState({time: new Date()})
		}
	}

	changeName = (e) => {
		this.setState({changingName: true, newUserRec: e.target.getAttribute('data-userrec'), newUserRecPin: e.target.getAttribute('data-userrecpin'), changingNameUser: e.target.getAttribute('data-user'), changingNameFirst: e.target.getAttribute('data-userNameFirst'), changingNameLast: e.target.getAttribute('data-userNameLast')})
	}
	cancelChangeName = () =>  {
		this.setState({changingName: false,  loading: false})
	}
	changeNameFirstChange = (e) =>  {
		if (e.target.value.length < 1){
			this.setState({changingNameFirst: '', changingNameFirstError: 'Please enter the user\'s first name.'})
		} else {
			this.setState({changingNameFirst: e.target.value, changingNameFirstError: ''});
		}
	}
	changeNameLastChange = (e) =>  {
		if (e.target.value.length < 1){
			this.setState({changingNameLast: '', changingNameLastError: 'Please enter the user\'s last name.'})
		} else {
			this.setState({changingNameLast: e.target.value, changingNameLastError: ''});
		}
	}
	confirmChangeName = () =>  {
		 if (this.state.changingNameFirst.toString().length < 1) {
			this.setState({changingNameFirstError: 'Please enter the user\'s first name.'});
		} else if (this.state.changingNameLast.toString().length < 1) {
			this.setState({changingNameLastError: 'Please enter the user\'s last name.'});
		} else if (this.state.newUserRec && (this.state.newUserRec.toString().length < 4 || this.state.newUserRec.toString().length > 10)) {
			this.setState({newUserRecNameError: 'UserID must be between 4-10 digits.'});
		} else if (this.state.newUserRecPin && (this.state.newUserRecPin.toString().length < 4 || this.state.newUserRecPin.toString().length > 10)) {
			this.setState({newUserRecPinError: 'Access Code must be at between 4-10 digits.'});
		} else if (this.state.newUserRec && !this.state.newUserRecPin) {
			this.setState({newUserRecPinError: 'Enter a User ID, or remove ID.'});
		} else if (!this.state.newUserRec && this.state.newUserRecPin) {
			this.setState({newUserRecNameError: 'Enter an Access Code, or remove User ID.'});
		} else if (this.state.newUserRec && this.state.newUserRecPin && this.state.newUserRec == this.state.newUserRecPin ) {
			this.setState({newUserRecNameError: 'User ID and Access Code must be different.'});
		}	else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'changeName',  recPassword: this.state.newUserRecPin, recUserID: this.state.newUserRec, user: this.state.changingNameUser, first: this.state.changingNameFirst, last: this.state.changingNameLast,  }))
		}
	}

	unlockUser = (e) => {
		this.setState({unlockingUser: true, unlockUser: e.currentTarget.getAttribute('data-user'), unlockUserName:  e.currentTarget.getAttribute('data-userName')})
	}
	cancelUnlockUser = (e) => {
		this.setState({unlockingUser: false, unlockUser:'', unlockUserName:''})
	}
	confirmUnlockUser = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'unlockUser', unlockUser: this.state.unlockUser }))
	}

	changeStatus = (e) => {
		this.setState({changingStatus: true, statusUser: e.currentTarget.getAttribute('data-user'), statusUserName:  e.currentTarget.getAttribute('data-userName')})
	}
	cancelChangeStatus = () => {
		this.setState({changingStatus: false})
	}
	confirmChangeStatus = (e) => {
		this.setState({ loading: true });
		var online = e.currentTarget.className === "onStatusOption"
		this.props.ws.send(JSON.stringify({route: 'statusUpdate', user: this.state.statusUser, status: e.currentTarget.innerHTML, online: online  }))
	}
	confirmUserLogout = (e) => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'logOutUser', user: this.state.statusUser }))
	}

	resetUser = (e) => {
		this.setState({resettingUser: true, resetUser: e.currentTarget.getAttribute('data-user'), resetUserName:  e.currentTarget.getAttribute('data-userName'), resetUserPIN: '', resetUserPINError:''})
	}
	resetUserPINChange = (e) =>  {
		if (e.target.value.length < 1){
			this.setState({resetUserPIN: '', resetUserPINError: 'Please enter a new password.'});
		} else {
			this.setState({resetUserPIN: e.target.value, resetUserPINError: ''});
		}
	}
	cancelResetUser = () => {
		this.setState({resettingUser: false, resetUser:'', resetUserName:'', resetUserPIN: '', resetUserPINError: ''})
	}
	confirmResetUser = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'resetUser', resetUser: this.state.resetUser,  resetUserPIN: this.state.resetUserPIN }))
	}

	changeRoles = (e) => {
		this.setState({editingRoles: true,  roleUser: e.currentTarget.getAttribute('data-user'), roleUserName: e.currentTarget.getAttribute('data-userName'), rolesToAdd:[], rolesToRemove: []});
	}
	cancelRolesChanges = () => {
		this.setState({editingRoles: false, roleUser:'', roleUserName: '', rolesToAdd:[], rolesToRemove: []})
	}
	addRole = (e) => {
		var rolesToAdd = this.state.rolesToAdd.slice()
		var rolesToRemove = this.state.rolesToRemove.slice()
		rolesToAdd.push(e.target.getAttribute('data-role'))
		if(rolesToRemove.indexOf(e.target.getAttribute('data-role')) > -1 ) {
			rolesToRemove.splice(rolesToRemove.indexOf(e.target.getAttribute('data-role')), 1)
		}
		this.setState({rolesToAdd: rolesToAdd, rolesToRemove: rolesToRemove})
	}
	removeRole = (e) => {
		var rolesToAdd = this.state.rolesToAdd.slice()
		var rolesToRemove = this.state.rolesToRemove.slice()
		rolesToRemove.push(e.target.getAttribute('data-role'))
		if(rolesToAdd.indexOf(e.target.getAttribute('data-role')) > -1 ) {
			rolesToAdd.splice(rolesToAdd.indexOf(e.target.getAttribute('data-role')), 1)
		}
		this.setState({rolesToAdd: rolesToAdd, rolesToRemove: rolesToRemove})
	}
	confirmRoleChanges = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'updateUserRoles', roleUser: this.state.roleUser,  rolesToAdd:  this.state.rolesToAdd,  rolesToRemove: this.state.rolesToRemove}))
	}


	addUser = () => {
		this.setState({addingUser: true, newUserID: '', newUserIDError: '', newUserFirst: '', newUserFirstError: '', newUserLast: '', newUserLastError: '', newUserPIN: '', newUserRec: '', newUserRecNameError:'',	newUserRecPin: '', newUserRecPinError:'',newUserPINError: ''})
	}
	cancelAddUser = () => {
		this.setState({addingUser: false,  loading: false})
	}
	newUserIDChange = (e) => {
		if (e.target.value.length < 1){
			this.setState({newUserID: '', newUserIDError: 'Please enter a Username.'})
		} else {
			this.setState({newUserID: e.target.value.split(' ')[0], newUserIDError: ''});
		}
	}
	newUserPINChange = (e) => {
		if (e.target.value.length < 1){
			this.setState({newUserPIN: '', newUserIDError: 'Please enter a password.'})
		} else {
			this.setState({newUserPIN: e.target.value, newUserIDError: ''});
		}
	}
	newUserFirstChange = (e) => {
		if (e.target.value.length < 1){
			this.setState({newUserFirst: '', newUserIDError: 'Please enter the user\'s first name.'})
		} else {
			this.setState({newUserFirst: e.target.value, newUserIDError: ''});
		}
	}
	newUserLastChange = (e) => {
		if (e.target.value.length < 1){
			this.setState({newUserLast: '', newUserIDError: 'Please enter the user\'s last name.'})
		} else {
			this.setState({newUserLast: e.target.value, newUserIDError: ''});
		}
	}
    newUserRecChange = (e) => {
			var newRec = e.target.value.replace(/\D/gm, '')
			if(newRec.length != e.target.value.length){
				this.setState({newUserRec: newRec, newUserRecNameError: 'Digits only, for use in the IVR.'});
			} else{
				this.setState({newUserRec: newRec, newUserRecNameError: ''});
			}
	}
	newUserRecPinChange = (e) => {
		var newRecPin = e.target.value.replace(/\D/gm, '')
		if(newRecPin.length != e.target.value.length){
			this.setState({newUserRecPin: newRecPin, newUserRecPinError: 'Digits only, for use in the IVR.'});
		} else{
			this.setState({newUserRecPin: newRecPin, newUserRecPinError: ''});
		}
	}
	confirmAddUser = () => {
		if (this.state.newUserID.toString().length < 5) {
			this.setState({newUserError: 'Please enter a Username. (Minimum 5 Characters)'});
		} else if (this.state.newUserPIN && this.state.newUserPIN.toString().length < 4) {
			this.setState({newUserError: 'Optional Password must be at least 4 Characters'});
		} else if (this.state.newUserFirst.toString().length < 1) {
			this.setState({newUserError: 'Please enter the user\'s first name.'});
		} else if (this.state.newUserLast.toString().length < 1) {
			this.setState({newUserError: 'Please enter the user\'s last name.'});
		} else if (this.state.newUserRec && (this.state.newUserRec.toString().length < 4 || this.state.newUserRec.toString().length > 10)) {
			this.setState({newUserRecNameError: 'Optional User ID must be between 4-10 digits.'});
		} else if (this.state.newUserRecPin && (this.state.newUserRecPin.toString().length < 4 || this.state.newUserRecPin.toString().length > 10)) {
			this.setState({newUserError: 'Optional Access Code must be at least 4 digits.'});
		} else if (this.state.newUserRec && !this.state.newUserRecPin) {
			this.setState({newUserError: 'Please enter an Access Code, or remove the User ID.'});
		} else if (!this.state.newUserRec && this.state.newUserRecPin) {
			this.setState({newUserError: 'Please enter a User ID, or remove the Access Code.'});
		} else if (this.state.newUserRec && this.state.newUserRecPin && this.state.newUserRec == this.state.newUserRecPin ) {
			this.setState({newUserError: 'The User ID and Access Code must be different.'});
		}	else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'addNewUser', recPassword: this.state.newUserRecPin, recUserID: this.state.newUserRec, newUserID: this.state.newUserID, newUserPIN: this.state.newUserPIN, newUserFirst: this.state.newUserFirst, newUserLast: this.state.newUserLast,  }))
		}
	}


	deleteUser = (e) => {
		this.setState({deletingUser: true, delUser: e.target.getAttribute('data-user'), delUserName: e.target.getAttribute('data-userName') })
	}
	cancelDeleteUser = () => {
		this.setState({deletingUser: false, delUser:'', delUserName: ''})
	}
	confirmDeleteUser = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'deleteUser', delUser: this.state.delUser  }))
	}

	componentWillReceiveProps(nextProps) {
        if(nextProps.data.resetUserPINError){
            this.setState({resettingUser: true, resetUserPINError: nextProps.data.resetUserPINError, editingRoles: false, changingName: false, addingUser: false, deletingUser: false,  unlockingUser: false,  changingStatus: false,  loading: false, editingQueues: false });
        } else if(this.state.loading) {
			this.setState({editingRoles: false, changingName: false, addingUser: false, deletingUser: false,  unlockingUser: false, resettingUser: false, changingStatus: false,  loading: false, editingQueues: false });
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return (this.props !== nextProps || this.state !== nextState);
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps.active === false && this.props.active === true) {        // If module is becoming active...
			for (var prop in this.state.filterBy) {											// Loop through the existing filter criteria
				this.refs[prop].value = this.state.filterBy[prop]						// And fill the inputs back in
			}
		}
	}
	sort = (e) => {
		var sortOrder
		if(this.state.sortOrder === -1){
			sortOrder = 1;
		}else{
			sortOrder = -1;
		}
		this.setState({sortBy: e.target.getAttribute("data-sortBy"), sortOrder: sortOrder})
	}

	filter = (e) => {
		var filterBy = this.state.filterBy
		if(e.target.value.length > 0) {
			filterBy[e.target.getAttribute("data-filterBy")] = e.target.value
		} else {
			delete filterBy[e.target.getAttribute("data-filterBy")]
		}
		this.setState({filterBy: filterBy})
	}

	sortFunc(sortBy, sortOrder){
		return function(a,b) {
			if(sortBy === "status" || sortBy ==="ActivityDateTime") {
				if('status' in a && 'status' in b){
					if(!a.loggedIn && !b.loggedIn){
						return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
					}else if(!a.loggedIn) {
						return 1
					}else if(!b.loggedIn) {
						return -1
					}
					if(a[sortBy] === b[sortBy]){
						return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
					}
					return a[sortBy] > b[sortBy] ? 1 * sortOrder : -1 * sortOrder
				}else {
					if('status' in a){
						return -1
					}
					return 1
				}
			}

			if(sortBy in a && sortBy in b){
				if(a[sortBy] === b[sortBy]){
					return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
				}
				return a[sortBy] > b[sortBy] ? 1 * sortOrder : -1 * sortOrder
			}else if (!(sortBy in a) && !(sortBy in b)){
				return a.username > b.username ? 1 * sortOrder : -1 * sortOrder
			}else {
				return sortBy in a ? -1 : 1
			}
		}
	}

	filterFunc(array, filterBy){
		return array.filter(function(object) {
			for(var prop in filterBy){
				if(typeof object[prop] === "undefined") {
					return false
				}
				if (object[prop].toString().toLowerCase().indexOf(filterBy[prop].toString().toLowerCase()) < 0) {
					return false
				}
			}
			return true
		})
	}

	convertMS(ms) {
		var d, h, m, s;
		s = Math.floor(ms / 1000);
		m = Math.floor(s / 60);
		s = s % 60;
		h = Math.floor(m / 60);
		m = m % 60;
		d = Math.floor(h / 24);
		h = h % 24;

		var statusTime = ''

		d > 0 ? statusTime += d + ' days, ' : statusTime += ''
		h < 1 ? statusTime += '': h > 0 && h < 10 ? statusTime +=  '0' + h + ':' : statusTime += h + ':'
		m < 1  ? statusTime += '00:' : m < 10 ? statusTime +=  '0' + m + ':' : statusTime += m + ':'
		s < 1  ? statusTime += '00' : s < 10 ? statusTime +=  '0' + s : statusTime += s

		return statusTime;
	};

	render() {
		if (this.props.active) {
			var users = [];

			for (var user in this.props.data.Users) {
				users.push(this.props.data.Users[user]);
			}

			//Filtering User List
			if(Object.keys(this.state.filterBy).length > 0) {
				users = this.filterFunc(users, this.state.filterBy)
			}

			//Sorting filtered User List
			if(this.state.sortBy !== '')  {
				users.sort(this.sortFunc(this.state.sortBy, this.state.sortOrder))
			}

			var userList = []
			for (user of users) {
				if(user.username === 'dirad'){
					//Do nothing. Hide the default dirad user account.
				} else {
					userList.push(
						<div className="dataRow" key={user.username}>
							<span style={{flex: '0 0 200px', textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }} className="userID">{user.username + '@' + this.props.data.domain}</span>
							<span style={{flex: '1 0 20%'}} onClick={this.changeName} data-userrec={user.userID} data-userrecpin={user.accessCode} className="userName" data-user={user.username}  data-userNameFirst={user.firstName} data-userNameLast={user.lastName}>{user.firstName + ' ' + user.lastName}</span>
							<span style={{flex: '1 1 30%'}} onClick={this.changeStatus} data-user={user.username}  data-userName={user.firstName + ' ' + user.lastName}>
								{user.loggedIn ? <span>{user.status}</span> : ''}
								{user.loggedIn ? <span style={{float: 'right'}}>{this.convertMS(this.state.time - new Date(user.ActivityDateTime)) }</span> : ''}
							</span>

							<span className="buttonBox" style={{flex: '0 0 250px'}}>
								{this.props.data.Roles ? <img src={roleIcon} alt="Change Roles"  data-user={user.username} data-userName={user.firstName + ' ' + user.lastName} onClick={this.changeRoles} /> : ''}

								{user.failedAttempts > 3 ? <img src={redLock} alt="unlock" data-user={user.username}  data-userName={user.firstName + ' ' + user.lastName} onClick={this.unlockUser}/> : <img src={reset} alt="reset" data-user={user.username} data-userName={user.firstName + ' ' + user.lastName} onClick={this.resetUser}/>}



								{this.props.features.callCenter ? <img src={group} alt="Change Workgroups"  data-user={user.username} data-userName={user.firstName + ' ' + user.lastName} onClick={this.changeWorkgroups} /> : ''}

								<img src={del} data-user={user.username} data-userName={user.firstName + ' ' + user.lastName} alt="Delete User" onClick={this.deleteUser} />
							</span>



						</div>
					)
				}
			}

			var addUser = ""
			if (this.state.addingUser) {
				addUser = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelAddUser}>X</div>

							<div className="error">{this.state.newUserError}</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.newUserID} onChange={this.newUserIDChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Username</label>
                                <div className="error">{this.state.newUserIDError}</div>
							</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="password" required value={this.state.newUserPIN} onChange={this.newUserPINChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Password</label>
                                <div className="error">{this.state.newUserPINError}</div>
							</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.newUserFirst} onChange={this.newUserFirstChange} ></input>
								<span className="inputBar"></span><label className="textLabel">First Name</label>
                                <div className="error">{this.state.newUserFirstError}</div>
							</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.newUserLast} onChange={this.newUserLastChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Last Name</label>
                                <div className="error">{this.state.newUserLastError}</div>
							</div>

                            {this.props.features.tui ? <div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.newUserRec} onChange={this.newUserRecChange} ></input>
								<span className="inputBar"></span><label className="textLabel">User ID (Optional)</label>
                                <div className="error">{this.state.newUserRecNameError}</div>
							</div> : ''}

							{this.props.features.tui ? <div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.newUserRecPin} onChange={this.newUserRecPinChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Access Code (Optional)</label>
                                <div className="error">{this.state.newUserRecPinError}</div>
							</div> :''}

							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmAddUser}>Create New User</div>}
						</div>
					</div>
				)
			}

			var changeName = ""
			if (this.state.changingName) {
				changeName = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelChangeName}>X</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.changingNameFirst} onChange={this.changeNameFirstChange} ></input>
								<span className="inputBar"></span><label className="textLabel">First Name</label>
                                <div className="error">{this.state.changingNameFirstError}</div>
							</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.changingNameLast} onChange={this.changeNameLastChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Last Name</label>
                                <div className="error">{this.state.changingNameLastError}</div>
							</div>

                            {this.props.features.tui ? <div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.newUserRec} onChange={this.newUserRecChange} ></input>
								<span className="inputBar"></span><label className="textLabel">User ID (Optional)</label>
                                <div className="error">{this.state.newUserRecNameError}</div>
							</div>  :''}

							{this.props.features.tui ? <div className="snazzyInputBox">
								<input className="textInput" type="password" required value={this.state.newUserRecPin} onChange={this.newUserRecPinChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Access Code (Optional)</label>
                                <div className="error">{this.state.newUserRecPinError}</div>
							</div>  :''}

							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmChangeName}>Confirm User Change</div>}
						</div>
					</div>
				)
			}


			var delUser = ""
			if (this.state.deletingUser) {
				delUser = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelDeleteUser}>X</div>
							<div>You Are Removing User</div>
							<div style={{margin: "15px"}}> {this.state.delUserName} </div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmDeleteUser}>Delete This User</div>}
						</div>
					</div>
				)
			}

			var editRoles = ""
			if (this.state.editingRoles) {
				var roles = this.props.data.Roles
				var roleList = []
				var removeRoleList = []
				var selRoleList = []
				var addRoleList = []

				for (var role in roles ) {
					if(this.props.data.Users[this.state.roleUser].roles[role] === true && this.state.rolesToRemove.indexOf(role) < 0) {
						selRoleList.push(
							<div key={role} data-role={role} className="dataRow" onClick={this.removeRole}>{roles[role].name}</div>
						)
					}else if (this.state.rolesToAdd.indexOf(role) > -1){
						addRoleList.push(
							<div key={role} data-role={role} style={{fontStyle: 'italic'}} className="dataRow" onClick={this.removeRole}>{roles[role].name}</div>
						)
					}else if (this.state.rolesToRemove.indexOf(role) > -1){
						removeRoleList.push(
							<div key={role} data-role={role} style={{fontStyle: 'italic'}} className="dataRow" onClick={this.addRole}>{roles[role].name}</div>
						)
					}else {
						roleList.push(
							<div key={role} data-role={role} className="dataRow" onClick={this.addRole}>{roles[role].name}</div>
						)
					}
				}
				editRoles = (
					<div className="blackout">
						<div className="pop" style={{width: "80vw", maxWidth: "1000px"}}>
							<div className="viewCloser" onClick={this.cancelRolesChanges}>X</div>
							<div style={{marginBottom: "25px"}}>Edit Roles for {this.state.roleUserName}</div>
							<div style={{display: "inline-block"}}>
								<div className="loginLabel" >Available Roles</div>
								<div id="availableRoles"  className="roleBox">
									{removeRoleList}
									{roleList}
								</div>
							</div>
							<div style={{display: "inline-block"}}>
								<div className="loginLabel" >Selected Roles</div>
								<div id="selectedRoles" className="roleBox">
									{addRoleList}
									{selRoleList}
								</div>
							</div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmRoleChanges}>Confirm Roles</div>}
						</div>
					</div>
				)
			}

			var resetUser = ""
			if (this.state.resettingUser) {
				resetUser = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelResetUser}>X</div>
							<div className="loginLabel">Reset the Password for {this.state.resetUserName}</div>

							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.resetUserPIN} onChange={this.resetUserPINChange} ></input>
								<span className="inputBar"></span><label className="textLabel">Enter a New Password</label>
							</div>
                            <div className="error">{this.state.resetUserPINError}</div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmResetUser}>Reset Password</div>}
						</div>
					</div>
				)
			}
			var unlockUser = ""
			if (this.state.unlockingUser) {
				unlockUser = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelUnlockUser}>X</div>
							<div className="loginLabel" style={{marginBottom: '15px'}}>Unlock Account for {this.state.unlockUserName}</div>
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmUnlockUser}>Unlock Account</div>}
						</div>
					</div>
				)
			}
			var changeStatus = ""
			if (this.state.changingStatus) {
				var statusList =  (
					<div id="statusList">
						<h3 onClick={this.confirmUserLogout} id="logOut" >- Log Out -</h3>
					</div>
				)

				changeStatus = (
					<div className="blackout">
						<div className="pop">
							<div className="viewCloser" onClick={this.cancelChangeStatus}>X</div>
							<div className="loginLabel" style={{marginBottom: '15px'}}>Change Status for {this.state.statusUserName}</div>
							{statusList}
							{this.state.loading ? <div className="loader"></div> : ''}
						</div>
					</div>
				)
			}

			return (
				<div className="viewContent">
					<img src={plus}  className="plusButton" alt="Add a User" onClick={this.addUser} />

					<div className="dataHeaders">
						<span style={{flex: '0 0 200px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="username">Username</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="wholeName">Name</span>
						<span style={{flex: '1 1 15%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="status">Status</span>
						<span style={{flex: '1 1 15%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="ActivityDateTime">Time</span>
						<span style={{flex: '0 0 280px'}} ></span>
					</div>
					<div className="dataFilters">
						<input style={{flex: '0 0 210px'}} type="text" onChange={this.filter} value={this.state.filterBy.username} ref="username" data-filterBy="username"/>
						<input style={{flex: '1 0 20%'}} type="text" onChange={this.filter} value={this.state.filterBy.wholeName} ref="wholeName"  data-filterBy="wholeName"/>
						<input style={{flex: '1 1 30%'}} type="text" onChange={this.filter} value={this.state.filterBy.status} ref="Status"  data-filterBy="status"/>
						<span style={{flex: '0 0 280px'}} ></span>
					</div>

					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{userList}
					</div>

					{addUser}
					{unlockUser}
					{resetUser}
					{delUser}
					{editRoles}
					{changeStatus}
					{changeName}
				</div>
			);
		} else {
			return (
				<div></div>
			);
		}
	}
}

export default Users;
