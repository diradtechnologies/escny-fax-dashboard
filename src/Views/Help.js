import React from 'react';

class Help extends React.PureComponent  {
	render() {
		if (this.props.active) {
			return (
				<div className="viewContent">
					<div style={{border: "1px solid #888", padding: "15px", marginRight: "8px"}}>
						<h3 style={{color: "#5f6062"}}>Contact Technical Support</h3>
						
						<strong style={{color: "#5f6062"}}>Phone:</strong> (518) 438-6000, option 4<br/>
						<strong style={{color: "#5f6062"}}>Toll Free:</strong> 1-800-778-2927, option 4<br/>
						<strong style={{color: "#5f6062"}}>Email:</strong> <a target="_blank" rel="noopener noreferrer" href="mailto:techsupport@dirad.com">techsupport@dirad.com</a>
						
						<br/>
					</div> 
					
					<br/>
					
					<div id="helpBox">
						<a target="_blank"  rel="noopener noreferrer" href="Contacting DiRAD's Technical Support Department.docx"><div className="helpRow">Download Technical Support Documentation</div></a>
					</div>

				</div>
			);
		} else {
			return (
				<div width="100%">
					
				</div>
			)
		}
	}
}

export default Help