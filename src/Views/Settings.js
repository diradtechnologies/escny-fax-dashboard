import React, { Component }  from 'react';

class Settings extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			active: false, 
			editingSetting: false,
			loading: false,
		};
	}
	
	cancel = () => {
		this.setState({editingSetting: false, loading: false, setting: false})
	}
	
	editSetting = (setting) => e => {
		this.setState({editingSetting: true, loading: false, setting: Object.assign({},setting)})
	}
	adjustSetting = (e) => {
		var setting = this.state.setting
		setting.value = e.currentTarget.value
		this.setState({setting:setting})
	}
	confirmSetting = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: this.state.setting.route, setting: this.state.setting }))		
	}
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel();
		}
	}
	render() {
		if (this.props.active) {	
			var settings = []
			for(var setting in this.props.data.Settings){
				settings.push(
					<div className="dataRow" key={setting} onClick={this.editSetting(this.props.data.Settings[setting])}>
						<span>{this.props.data.Settings[setting].name}</span>  
						<span  style={{flex: '0 0 50%', textAlign: 'center'}}>{this.props.data.Settings[setting].value}</span>
					</div>
				
				)
			}
			var editSetting = ""
			if (this.state.editingSetting) {
				var control = ''
				if(this.state.setting.type === 'number'){
					control = (
						<input style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} type="number" min={this.state.setting.min} max={this.state.setting.max} value={this.state.setting.value} onChange={this.adjustSetting} />
					)
				}
				
				
				editSetting = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>Adjust the setting for <br/> {this.state.setting.name}</div>
							<br/>
							
							{control}
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmSetting}>Confirm</div>}
						</div>
					</div>
				)
			}
			
			return (
				<div className="viewContent" id="reportView" >
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{settings}
					</div>
					{editSetting}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default Settings