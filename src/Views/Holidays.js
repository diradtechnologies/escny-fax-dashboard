import React, { Component }  from 'react';
import Flatpickr from 'react-flatpickr'
import del from '../img/delete.svg';
import plus from '../img/plus.svg';

class Holidays extends Component {
	constructor(props) {
		super(props);
		this.state = { 	
			addingHoliday: false,
			editingHoliday: false,
			deletingHoliday: false,
			loading: false,
			filterBy: {}, 
			sortBy:'role', 
			sortOrder: 1
		};

	}
	cancel = () => {
		this.setState({addingHoliday: false, editingHoliday: false, deletingHoliday: false, loading: false, name: false, date:false, active: null, partial: null})
	}
	editHolidayName = (e) =>  {
		if (e.target.value < 1){
			this.setState({name: '', error: 'Please enter a holiday name.'});
		} else {
			this.setState({name: e.target.value, error: ''});
		}
	}
	editHoliday = (holiday) => e => {
		this.setState({editingHoliday: true, loading: false, id: holiday.Id, name: holiday.Holiday_Name, date:  holiday.Holiday_Date, active: holiday.is_enabled, partial: holiday.Partial,   error: ''})
	}
	confirmEditHoliday = () => {
		if (this.state.name.length < 1){
			this.setState({error: 'Please enter a holiday name.'});
		} else if (!this.state.date) {
			this.setState({error: 'Please select a date for this holiday.'});
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editHoliday', name: this.state.name,  date: this.state.date, id: this.state.id,  partial: this.state.partial,  active: this.state.active  }))		
		}	
	}
	
	addHoliday = (e) => {
		this.setState({addingHoliday: true, loading: false, name: '', date:  '', error: '', active: false, partial: false})
	}
	confirmAddHoliday = () => {
		if (this.state.name.length < 1){
			this.setState({error: 'Please enter a holiday name.'});
		} else if (!this.state.date) {
			this.setState({error: 'Please select a date for this holiday.'});
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'addHoliday', name: this.state.name,  date: this.state.date, partial: this.state.partial,  active: this.state.active  }))		
		}	
	}
	
	deleteHoliday = (holiday) => e => {
		e.stopPropagation()
		this.setState({deletingHoliday: true, loading: false, name: holiday.Holiday_Name, error: ''})
	}
	confirmDeleteHoliday = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'deleteHoliday', name: this.state.name }))		
	}
	
	checkEnter = (e) => {
		if(e.keyCode === 13){
			if(this.state.addingHoliday){
				this.confirmAddHoliday();
			}else if (this.state.editingHoliday){
				this.confirmEditHoliday();
			}else if (this.state.deletingHoliday){
				this.confirmDeleteHoliday();
			}	
		}
	}
	
	toggle = (param) => (e) => {
		var newState = Object.assign({}, this.state)
		newState[param] = this.state[param] ? false : true
		this.setState(newState)
	}
	
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel()
		}
	}
	
	render() {
		if (this.props.active) {	
			var holidayList = []
			for (var holiday of this.props.data.Holidays) {
				holidayList.push(
					<div className="dataRow"  onClick={this.editHoliday(holiday)} key={holiday.Id}>
						<div className="activeBox" data-active={holiday.is_enabled} data-name={holiday.Holiday_Name} >
							<div className={holiday.is_enabled ? "enabled" : "disabled"}></div>
						</div>
						
						<span style={{flex: '0 0 300px'}} >{holiday.Holiday_Name}</span>
						
						{holiday.hasOwnProperty('Partial') ? 
							<div className="activeBox">
								<div className={holiday.Partial ? "enabled" : "disabled"}></div>
							</div> : ''
						}
						
						<span style={{flex: '1 0 20%', textAlign: 'center'}}>{new Date(holiday.Holiday_Date).toDateString()}</span>
						
						<span className="buttonBox" style={{flex: '0 0 280px'}}>
							<img src={del} alt="Delete Holiday" onClick={this.deleteHoliday(holiday)} />
						</span>
					</div>
				)
			}
			
			
			var editHoliday = ""
			if (this.state.editingHoliday) {
				editHoliday = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							
							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.name} onChange={this.editHolidayName} ></input>
								<span className="inputBar"></span><label className="textLabel">Holiday Name</label>
							</div>
							
							<div style={{display: 'inline-block'}}>
								<div>Holiday Date</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d"}} value={this.state.date} onChange={date => { date[0] ? this.setState({date: date[0]}) : this.setState({date: undefined}) }} />
							</div>
							
							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Active</div>
									<div className="activeBox" onClick={this.toggle('active')}>
										<div className={this.state.active ? "enabled" : "disabled"}></div>
									</div>
								</div>
								<div >
									<div>Partial</div>
									{holiday.hasOwnProperty('Partial') ? 
										<div className="activeBox" onClick={this.toggle('partial')}>
											<div className={this.state.partial ? "enabled" : "disabled"}></div>
										</div>
										: ''}
									</div>
							</div>	
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmEditHoliday}>Confirm Holiday</div>}
						</div>
					</div>
				)
			}
			
			var addHoliday = ""
			if (this.state.addingHoliday) {
				addHoliday = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							
							<div className="snazzyInputBox">
								<input className="textInput" type="text" required value={this.state.name} onChange={this.editHolidayName} ></input>
								<span className="inputBar"></span><label className="textLabel">Holiday Name</label>
							</div>
							
							<div style={{display: 'inline-block'}}>
								<div>Holiday Date</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d"}} value={this.state.date} onChange={date => { date[0] ? this.setState({date: date[0]}) : this.setState({date: undefined}) }} />
							</div>
							
							<div style={{ paddingTop: '25px',display: 'flex', justifyContent: 'space-around'}}>
								<div >
									<div>Active</div>
									<div className="activeBox" onClick={this.toggle('active')}>
										<div className={this.state.active ? "enabled" : "disabled"}></div>
									</div>
								</div>
								<div >
									<div>Partial</div>
									{holiday.hasOwnProperty('Partial') ? 
										<div className="activeBox" onClick={this.toggle('partial')}>
											<div className={this.state.partial ? "enabled" : "disabled"}></div>
										</div>
										: ''}
									</div>
							</div>
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmAddHoliday}>Confirm New Holiday</div>}
						</div>
					</div>
				)
			}
			
			var deleteHoliday = ""
			if (this.state.deletingHoliday) {
				deleteHoliday = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Removing Holiday</div>
							<div style={{margin: "15px"}}> {this.state.name} </div>							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmDeleteHoliday}>Delete This Holiday</div>}
						</div>
					</div>
				)
			}
		
			return (
				<div width="100%">
					<img src={plus}  className="plusButton" alt="Add a Holiday" onClick={this.addHoliday} />
					<div className="dataHeaders">
						<span style={{flex: '0 0 83px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="enabled">Active?</span>
						<span style={{flex: '0 0 300px', cursor: 'pointer', textAlign: 'left'	}} onClick={this.sort} data-sortBy="name">Description</span>
						{holiday.hasOwnProperty('Partial') ? 
							<span style={{flex: '0 0 76px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="enabled">Partial?</span>
						: '' }
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Date</span>
						<span style={{flex: '0 0 330px'}}></span>
					</div> 
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{holidayList}
					</div>
					
					{editHoliday}
					{addHoliday}
					{deleteHoliday}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default Holidays