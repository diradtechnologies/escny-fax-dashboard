import React, { Component }  from 'react';
import Flatpickr from 'react-flatpickr'

class CallingHours extends Component {
	constructor(props) {
		super(props);
		this.state = { 	
			editingHours: false,
			loading: false,
		};
	}
	
	cancel = () => {
		this.setState({editingHours: false, loading: false, game: false, start:false, end: false})
	}
	
	editHours = (day) => e => {
		this.setState({editingHours: true, loading: false, id: day.RecNum, day: day.Day_Of_Week, error: '', start: day.Earliest_Time_To_Call, end: day.Latest_Time_To_Call})
	}
	confirmHours = () => {
		if (new Date(this.state.start) > new Date( this.state.end)) {
			this.setState({error: 'End time must not be before Start time.'});
		}else {
			this.setState({ loading: true });
			this.props.ws.send(JSON.stringify({route: 'editCallingHours', id: this.state.id, start: this.state.start, end: this.state.end  }))		
		}	
	}
	
	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmHours();
		}
	}
	
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel();
		}
	}
	
	render() {
		if (this.props.active) {	
			var chList = []
			for (var day of this.props.data.CallingHours) {
				chList.push(
					<div className="dataRow" key={day.RecNum} onClick={this.editHours(day)}>
						<span style={{flex: '0 0 150px'}} >{day.Day_Of_Week}</span>
						<span style={{flex: '1 0 20%', textAlign:'center'}}>{new Date(day.Earliest_Time_To_Call).toLocaleTimeString().substr(0,4) + new Date(day.Earliest_Time_To_Call).toLocaleTimeString().substr(7,9)}</span>
						<span style={{flex: '1 0 20%', textAlign:'center'}}>{new Date(day.Latest_Time_To_Call).toLocaleTimeString().substr(0,4) + new Date(day.Latest_Time_To_Call).toLocaleTimeString().substr(7,9)}</span>
					</div>
				)
			}
			
			
			var editHours = ""
			if (this.state.editingHours) {
				editHours = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop" >
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>You Are Editing Hours for {this.state.day}</div>
							<br/>
							
							<div style={{display: 'inline-block'}}>
								<div>Call Start Time</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{enableTime: true, noCalendar:true, dateFormat:"h:i K"}} value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>
							
							<div style={{display: 'inline-block'}}>
								<div>Call End Time</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{enableTime: true, noCalendar:true, dateFormat:"h:i K"}} value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) : this.setState({end: undefined}) }} />
							</div>
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmHours}>Confirm Hours</div>}
						</div>
					</div>
				)
			}
			
		
			return (
				<div width="100%">
					<div className="dataHeaders">
						<span style={{flex: '0 0 150px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="name">Day</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Call Start Time</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Call End Time</span>
					</div> 
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{chList}
					</div>
					
					<div style={{textAlign: 'center', color: '#888', fontStyle: 'italic'}}>Manage the hours during which outbound calls are allowed.</div>
					{editHours}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default CallingHours