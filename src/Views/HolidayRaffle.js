import React, { Component }  from 'react';
import Flatpickr from 'react-flatpickr'

class HolidayRaffle extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			active: false, 
			editingRange: false,
			loading: false,
		};
	}
	cancel = () => {
		this.setState({editingRange: false, loading: false, start: false, end: false})
	}
	editRaffleRange = (e) => {
		this.setState({editingRange: true, loading: false, start: this.props.data.HolidayRaffle.start, end: this.props.data.HolidayRaffle.end})
	}
	confirmRange = () => {
		this.setState({ loading: true });
		this.props.ws.send(JSON.stringify({route: 'ksl_SetHolidayRaffleDates', start: this.state.start, end: this.state.end }))		
	}
	checkEnter = (e) => {
		if(e.keyCode === 13){
			this.confirmRange();
		}
	}
	toggleRaffle = (e) => {
		e.stopPropagation()
		var newActive = e.currentTarget.getAttribute('data-active') === "true" ? false : true
		this.props.ws.send(JSON.stringify({route: 'ksl_toggleHolidayRaffle', prompt: e.currentTarget.getAttribute('data-prompt'), active: newActive }))	
	}
	componentWillReceiveProps(nextProps) {
		if(this.state.loading) {
			this.cancel();
		}
	}
	render() {
		if (this.props.active) {	
			var settings = []
			
				settings.push(
					<div className="dataRow" key={'holidayRaffle'} onClick={this.editRaffleRange}>
						<div className="activeBox" data-active={this.props.data.HolidayRaffle.active} onClick={this.toggleRaffle}>
							<div className={this.props.data.HolidayRaffle.active ? "enabled" : "disabled"}></div>
						</div>
						<span>Holiday Millionaire Raffle</span>  
						<span  style={{flex: '0 0 50%', textAlign: 'center'}}>{new Date(this.props.data.HolidayRaffle.start).toLocaleDateString() + ' - ' + new Date(this.props.data.HolidayRaffle.end).toLocaleDateString()}</span>
					</div>
				
				)
			
			var editRange = ""
			if (this.state.editingRange) {
				editRange = (
					<div className="blackout" tabIndex="1" autoFocus onKeyUp={this.checkEnter}>
						<div className="pop">
							<div className="viewCloser" onClick={this.cancel}>X</div>
							<div>Adjusting the Effective Date Range for the Holiday Millionaire Raffle</div>
							<br/>
							
							<div style={{display: 'inline-block'}}>
								<div>Start</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d"}} value={this.state.start} onChange={date => { date[0] ? this.setState({start: date[0]}) : this.setState({start: undefined}) }} />
							</div>
							<div style={{display: 'inline-block'}}>
								<div>End</div>
								<Flatpickr style={{border: '1px solid #2A6AA9', padding: '15px', width: '130px', margin: '5px'}} options={{dateFormat:"Y-m-d"}} value={this.state.end} onChange={date => { date[0] ? this.setState({end: date[0]}) :this.setState({end: undefined}) }} />
							</div>
							
							<div className="error" style={{padding: '15px'}}>{this.state.error}</div>
							
							{this.state.loading ? <div className="loader"></div> : <div className="button"  onClick={this.confirmRange}>Confirm Date Range</div>}
						</div>
					</div>
				)
			}
			
			return (
				<div className="viewContent" id="reportView" >
					<div className="dataHeaders">
						<span style={{flex: '0 0 75px', cursor: 'pointer'}} onClick={this.sort} data-sortBy="enabled">Active?</span>
						<span style={{flex: '1 0 20%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Description</span>
						<span style={{flex: '1 0 30%', cursor: 'pointer'}} onClick={this.sort} data-sortBy="modified">Date Range</span>
					</div> 
					<div style={{overflow: 'auto', height: 'calc(100% - 85px)'}}>
						{settings}
					</div>
					{editRange}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default HolidayRaffle