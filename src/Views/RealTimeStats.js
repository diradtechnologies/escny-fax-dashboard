import React, { Component }  from 'react';
import { Chart } from 'react-google-charts';

class RealTimeStats extends Component {
	constructor(props) {
		super(props);
		this.state = { 		};

	}

	render() {
		var modules = []
		if(Object.keys(this.props.data.RealTimeStats).length > 0){
			for (var module of this.props.data.RealTimeStats){
				var rows
				
				if(module.type === 'pie'){
					if (module.data.length > 0){
						rows = []
						for(var option of module.data){
							rows.push([option.option + ': ' + option.count, option.count])
						}
						
						modules.push(
							<div key={module.title} className="mini-chart-container"  style={{display:'inline-block', width: '50%', height: '35vh'}}>
								<h2 style={{textAlign: 'center', color: '#2A6AA9'}}>{module.title}</h2>
								<Chart
									chartType="PieChart"
									columns={[{type: 'string', label: 'Menu Option'},{ type: 'number', label: 'Number of Times Chosen'}]}
									rows={rows}
									options={
										{pieHole: 0.4, 
											legend: {position: 'right', alignment: 'center'},
										pieSliceTextStyle: {color: '#000', fontSize: 10  },
										chartArea: {top: -50, width: "100%",	height: "100%" }, 
										}
									}
									graph_id="pieChart"
									height="80%"
									width="100%"
								/>
							</div>
						)
					} else {
						modules.push(
							<div key={module.title} className="mini-chart-container"  style={{display:'inline-block', width: '50%', height: '35vh'}}>
								<h2 style={{textAlign: 'center', color: '#2A6AA9'}}>{module.title}</h2>
								<div style={{textAlign: 'center'}}>There is no data available.</div>
							</div>
						)
					}	
				} else if(module.type === 'bar'){
					rows = [[new Date(   (  new Date()  ).getTime() + 3600000), 0, '']]
					var options = { 
						chartArea: {  left: 50, width: "100%", height: "70%" },
						legend: { position: 'none' }, 
						animation: {duration: 500}, 
						colors:['#2A6AA9'], bar: {groupWidth: 15}, tooltip: { isHtml: true }}
					var columns = [{type: 'datetime', label: 'Time'},{ type: 'number', label: 'Number of Calls'},{type: 'string', role: 'tooltip', p: {html: true}}]
					
					for (var time of module.data) {
						rows.push([new Date(Date.UTC(time.y, time.m, time.d, time.h)), time.calls, '<div style="padding: 5px 5px 0px 5px;"><b>'+(new Date(Date.UTC(time.y, time.m, time.d, time.h))).toLocaleString()+'</b></div><div style="padding: 5px;">Calls: <b>'+time.calls+'</b></div>' ])
					}
					options.hAxis = {
						viewWindow: {
							min: new Date((new Date()).getTime() - 86400000),
							max: new Date()
						},
						gridlines: {
							count: 6,
							units: {
							  hours: {format: ['h']},
							}
						}
					}

					modules.push(
						<div key={module.title}  className="chart-container" style={{display:'inline-block', paddingLeft: '2%', width: '48%',    verticalAlign: 'top'}}>
							<h2 style={{textAlign: 'center', color: '#2A6AA9',marginBottom: '0px'}}>{module.title}</h2>
							<Chart
								chartType="ColumnChart"
								columns={columns}
								rows={rows}
								options={options}
								graph_id="historyChart"
								width="90%"
							/>
						</div>
					)	
				}else if(module.type === 'table'){
					for(var i = 0; i < module.data.columns.length; i++){
						if(module.data.columns[i].type === 'datetime'){
							for(var row of module.data.rows){
								var date = new Date(row[i])
								if(row[i]){row[i] = date}
								//if(row[i]){row[i] = new Date(Date.UTC(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds() ))}
							}
						}
					}
					if(module.data.rows.length > 0){
						modules.push(<h2  key="Total Active Calls" style={{textAlign: 'center', color: '#2A6AA9'}}>Total Active Calls: {module.data.rows.length }</h2>)
						modules.push(
							<Chart key={module.title} 
								chartPackages= {['corechart', 'table']}
								chartType="Table"
								options={{
									title: module.title, 
									page: 'enable',
									pageSize: 100,
									pagingButtons: 'auto',
									width:'100%'
								}}
								
								columns={module.data.columns}
								rows={module.data.rows}
								graph_id={"reportTable"}	
								width='100%'
								height='2000px'
							/>
						)
					}else {
						modules.push(<h2 key={module.title} style={{textAlign: 'center', color: '#2A6AA9'}}>No Active Calls</h2>)
					}
				}
			
			
			
			}
		}
		
		if (this.props.active) {	
			return (
				<div width="100%">
					{modules}
				</div>
			);
		} else {
			return (
				<div width="100%">

				</div>
			);
		}
	}
}

export default RealTimeStats