import React from 'react';
//Syntax to use this component
//import SnazzyInput from 'snazzyInput.js'
//<SnazzyInput error={this.state.newUserIDError} value={this.state.editRole} onChange={this.addRoleNameChange} placeHolder={"Add New Role Name"}/>

export default class SnazzyInput extends React.PureComponent {
	render() {
		return (
			<div className="snazzyInputBox">
				<div className="error">{this.props.error}
				</div>
				
				<input className="textInput" type="text" required value={this.props.value} onChange={this.props.onChange} >
				</input>
				
				<span className="inputBar">
				</span>
				
				<label className="textLabel">
					{this.props.placeHolder}
				</label>
			</div>
		)	
	}
}


