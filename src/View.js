import React from 'react'
var Views = {}
var comps = require.context('./Views', false, /.*/)
comps.keys().map((item) => { Views[item.replace('./', '')] = comps(item).default})

export class View extends React.PureComponent{
	constructor(props) {super(props)}

    render() {
	    var  ViewType = Views[this.props.view]
        return (
    		<div id={this.props.view} className={this.props.active ? 'active view' : 'view'} >
    			<div className="viewHeader">
                    <h1>{this.props.title}</h1>
    			</div>
    			<ViewType ref={(component) => {window[this.props.view + 'Functions'] = component}} features={this.props.features} ws={this.props.ws} data={this.props.data} active={this.props.active}/>
    		</div>
        );
    }
}
